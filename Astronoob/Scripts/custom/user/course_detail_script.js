﻿$(function () {
    $("#quizbutton").click(function () {
        $(this).attr('disabled', 'disabled');
        $("#start_quiz").attr('disabled', 'disabled');
        $.ajax({
            url: "/Home/QuizSession",
            type: 'POST',
            data: { courseid: model_courseid },
            success: function (response) {
                if (response == 0) {
                    window.open('/Quiz/' + vb_encrypt + '/1', 'Quiz', 'menubar=no, addressbar=no, toolbar=no, location=no')
                }
                else if (response == 1) {
                    window.location.href = '/Result/' + vb_encrypt;
                }
                else if (response == 2) {
                    window.location.href = '/Error';
                }

            }
        });
    });
});
