﻿
if (localStorage.getItem("total_seconds")) {
    var interval = 1 * localStorage.getItem("total_seconds");
} else {
    var interval = 1 * 60000;
}

    // Set the date we're counting down to
var now = new Date().getTime();
var countDownDate = new Date(now + interval);

var x = setInterval(function () {
    var now = new Date().getTime();
    var distance = countDownDate - now;

    var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
    var seconds = Math.floor((distance % (1000 * 60)) / 1000);

    document.getElementById("timer_area").innerHTML = minutes + "m " + seconds + "s ";

    if (distance <= 0) {
    clearInterval(x);
        document.getElementById("timer_area").innerHTML = "EXPIRED";
        var number = model_num + 1;
        window.location.href = '/Quiz/' + vb_encrypt +'/' + number;
        getPoint();
    } else {
    localStorage.setItem("total_seconds", distance);
    }
}, 1000);

$(function () {
    $("#submitquiz").click(function () {
        getPoint();
    });
});

function getPoint() {
        var encrypt_id = vb_encrypt;
        var answer = $("input[name='option']:checked", "#form_quiz").val();
        $.ajax({
    url: "/Home/TotalPoints",
            type: 'POST',
            data: {course_encrypt_id: encrypt_id, number: model_num, answer: answer },
            success: function (response) {
                if (response == 1) {
    localStorage.clear();
                    var number = model_num + 1;
                    window.location.href = '/Quiz/' + vb_encrypt+'/' + number;
                }
                else if (response == 0) {
                    window.opener.location.href = '/Result/' + vb_encrypt;
                    window.close();
                }
                else if (response == 2) {
    window.location.href = '/Error';
                }
            }
        });
}

this.history.forward(-1);
