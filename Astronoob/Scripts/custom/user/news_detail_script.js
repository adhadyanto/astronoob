﻿    $(function getAllNewsComments() {
        $.ajax({
            url: "/User/ShowNewsComment",
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            method: 'GET',
            data: { newsid: model_newsid },
            beforeSend: function () {
        $('#news_comment_area_body').html('<div class="text-center"><p>Loading Data</p></div>');
            },
            success: function (response) {
        $('#news_comment_area_body').html('');
                if (response == '[]') {
        $('#news_comment_area_body').html('<div class="text-center"><p>No Replies</p></div>');
                } else {
                    var data = JSON.parse(response);
                    $.each(data, function (key) {
                        var post =
                            '<tr>' +
                            '<td>' +
                            '<ol>' +
                            '<li class="single_comment_area">' +
                            '<div class="comment-content d-flex">' +
                            '<div class="comment-author">' +
                            '<img style="width:70px;height:100%" src="../../img/pp-img/' + data[key].photo + '" alt="author">' +
                            '</div>' +
                            '<div class="comment-meta">' +

                            '<h6>' + data[key].username + '</h6>' +
                            '<p>' + data[key].comment + '</p>' +
                            '<p class="comment-date">' + data[key].date + '</p>';

                            if (identity) {
                                var uname = identity_nm;
                                if (uname != data[key].username) {
                                    post += '<button type = "button" class="btn btn-secondary btn-sm" style="width:30px" title="Report" data-commentnews="' + data[key].id + '" id="report_comment_modal" onclick ="reportCommentNews(this)" ><i class="fa fa-flag"></i></button >';
                                } else {
                                    post += '<button type="button" class="btn btn-info btn-sm" style="width:30px" title="Edit" data-commentid="' + data[key].id + '" data-comment="' + data[key].comment + '" id="edit_comment_modal" onclick="editCommentNews(this)"><i class="fa fa-edit"></i></button> ';
                                    post += '<button type="button" class="btn btn-danger btn-sm" style="width:30px" title="Delete" data-commentnews="' + data[key].id + '" id="delete_comment_modal" onclick="deleteCommentNews(this)"><i class="fa fa-trash"></i></button>';
                                }
                            }
                            else {
                                    post +=
                                    '<button type="button" class="btn btn-info btn-sm" title="Please login first to report this comment" disabled>' +
                                    '<i class="fa fa-flag"></i>' +
                                    '</button>';
                            }  
                        post +=
                            '</div>' +
                            '</div>' +
                            '</div>' +
                            '</li>' +
                            '</ol>' +
                            '</td>' +
                            '</tr>' +
                            '<hr />';
                        $("#news_comment_area_body").append(post);
                    });

                    $.noConflict();

                    $("#news_comment_area").DataTable({
        "pageLength": 5,
                        "lengthChange": false,
                        "searching": false,
                        "info": false,
                        "sorting": false,
                        "responsive": true,
                        "scrollX": true
                    });
                }
            }
        });
    });

    $(function () {
        $("#submitNewsComment").click(function () {

            if ($("#news_post_comment").valid()) {
                var comment = $("#newscomment").val();
                $.ajax({
                    url: "/User/SaveNewsComment",
                    type: 'POST',
                    data: { newsid: model_newsid, comment_text: comment },
                    success: function (data) {
        getAllNewsComments();
                    }
                });
            }
        });
    });

        $(function () {
        $("#submitCommentReport").click(function () {
            var commentid = $("#reportcommentid").val();
            var option = $("input[name='option_comment']:checked", "#form_news_comment_report").val();;
            $.ajax({
                url: "/User/ReportCommentNews",
                type: 'POST',
                data: { reportcommentid: commentid, option: option },
                success: function (data) {
                    $('#news_report_comment_modal').modal('hide');
                    $('#news_success_report_modal').modal('show');
                }
            });
        });
        });


        function reportCommentNews(id) {
        $('#news_report_comment_modal').modal('show');
            $("#reportcommentid").val(id.getAttribute("data-commentnews"))
        }

        $(function () {
        $("#deleteCommentNews").click(function () {
            var commentid = $("#deletecommentid").val();
            $.ajax({
                url: "/User/DeleteCommentNews",
                type: 'POST',
                data: { commentNewsid: commentid },
                success: function (data) {
                    $('#news_delete_comment_modal').modal('hide');
                    window.location.reload(true);
                }
            });
        });
        });

        function deleteCommentNews(id) {
        $('#news_delete_comment_modal').modal('show');
            $("#deletecommentid").val(id.getAttribute("data-commentnews"))
        }

        $(function () {
        $("#submitCommentEdit").click(function () {
            if ($("#form_news_comment_edit").valid()) {
                var commentid = $("#editnewscommentid").val();
                var comment = $("#commentdesc").val();
                $.ajax({
                    url: "/User/EditCommentNews",
                    type: 'POST',
                    data: { commentNewsid: commentid, comment: comment }
                });
            }
        });
        });

        function editCommentNews(id) {
        $('#news_edit_comment_modal').modal('show');
            $("#editnewscommentid").val(id.getAttribute("data-commentid"))
            $("#commentdesc").val(id.getAttribute("data-comment"))
        }
