﻿$(function getAllCourses() {
    $.ajax({
        url: "/User/ShowCourses",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        method: 'GET',
        beforeSend: function () {
            $('course_loading').html('<div class="text-center"><p>Loading Data</p></div>');
        },
        success: function (response) {
            $('#course_loading').html('');
            if (response == '[]') {
                $('#course_loading').html('<div class="text-center"><p>No Course Available</p></div>');
            } else {
                var data = JSON.parse(response);
                $.each(data, function (key) {
                    var post =
                        '<div class="single-catagory-post d-flex flex-wrap">' +
                        '<div class="post-content">' +
                        '<a href = "/Courses/' + data[key].course_encrypt + '/' + data[key].materi_encrypt + '">' + data[key].name + '</a>' +
                        '<p>' + data[key].description + '</p>' +
                        '</div>' +
                        '</div >';
                    $("#course_area").append(post);
                });
            }
        }
    });
});