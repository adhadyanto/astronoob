﻿$(document).ready(function () {
    $.noConflict();
});

$(function getAllResult() {
    $.ajax({
        url: "/Admin/UserCourseResult",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        method: 'GET',
        data: { username: model_username },
        success: function (response) {
            var data = JSON.parse(response);
            $.each(data, function (key) {
                var post =
                    '<tr>' +
                    '<td>' +
                    data[key].name +
                    '</td>' +
                    '<td>' +
                    data[key].grade +
                    '</td>' +
                    '<td>' +
                    data[key].date +
                    '</td>' +
                    '</tr>';
                $("#post_course_body").append(post);
            });

            $("#post_course").DataTable({
                "pageLength": 3,
                "lengthChange": false,
                "searching": false
            });
        }
    });
});

$(function getAllPost() {
    $.ajax({
        url: "/Admin/UserDiscPost",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        method: 'GET',
        data: { username: model_username },
        success: function (response) {
            var data = JSON.parse(response);
            $.each(data, function (key) {
                var post =
                    '<tr>' +
                    '<td>' +
                    data[key].title +
                    '</td>' +
                    '<td>' +
                    '<a href="/Discussions/' + data[key].encrypt_id + '">link</a>' +
                    '</td>' +
                    '<td>' +
                    data[key].date +
                    '</td>' +
                    '</tr>';
                $("#post_discussion_body").append(post);
            });

            $("#post_discussion").DataTable({
                "pageLength": 3,
                "lengthChange": false,
                "searching": false
            });
        }
    });
});

$(function getAllComment() {
    $.ajax({
        url: "/Admin/UserDiscComment",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        method: 'GET',
        data: { username: model_username },
        success: function (response) {
            var data = JSON.parse(response);
            $.each(data, function (key) {
                var post =
                    '<tr>' +
                    '<td style="width:300px;">' +
                    data[key].comment +
                    '</td>' +
                    '<td>' +
                    '<a href="/Discussions/' + data[key].encrypt_id + '">link</a>' +
                    '</td>' +
                    '<td>' +
                    data[key].date +
                    '</td>' +
                    '</tr>';
                $("#comment_discussion_body").append(post);
            });

            $("#comment_discussion").DataTable({
                "pageLength": 3,
                "lengthChange": false,
                "searching": false
            });

        }
    });
});
