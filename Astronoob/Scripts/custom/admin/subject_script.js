﻿
$(document).ready(function () {
    $.noConflict();
});

$(function () {
    $("#submitSubject").click(function () {
        if ($("#form_subject").valid()) {

            var courseid = course_id;
            var number = $("#subject_number").val();
            var video = $("#subject_video").val();
            var vid_source = $("#subject_vid_source").val();
            var title = $("#subject_title").val();
            var description = $("#subject_description").val();
            var sub_source = $("#subject_source").val();
            $.ajax({
                url: "/Admin/SaveSubject",
                type: 'POST',
                data: { courseid: courseid, number: number, video: video, vid_source: vid_source, title: title, description: description, sub_source: sub_source },
                success: function (data) {
                    if (data == 1) {
                        alert("Success");
                    }
                    else if (data == 0) {
                        alert("Please check the required field");
                    }
                }
            });
        }
    });
});

$(function () {
    $("#submitQuiz").click(function () {

        if ($("#form_quiz").valid()) {

            var courseid = course_id;
            var question = $("#quiz_question").val();
            var opt_a = $("#quiz_a").val();
            var opt_b = $("#quiz_b").val();
            var opt_c = $("#quiz_c").val();
            var opt_d = $("#quiz_d").val();
            var answer = $("#quiz_answer").val();
            var number = $("#quiz_number").val();
            $.ajax({
                url: "/Admin/SaveQuiz",
                type: 'POST',
                data: { courseid: courseid, question: question, opt_a: opt_a, opt_b: opt_b, opt_c: opt_c, opt_d: opt_d, answer: answer, number: number },
                success: function (data) {
                    if (data == 1) {
                        alert("Success");
                    }
                    else if (data == 0) {
                        alert("Please check the required field");
                    }
                }

            });
        }
    });
});

$(function getAllSubject() {
    $.ajax({
        url: "/Admin/ShowSubject",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        method: 'GET',
        data: { courseid: course_id },
        beforeSend: function () {
            $('#subject_loading').html('<div class="text-center"><p>Loading Data</p></div>');
        },
        success: function (response) {
            $('#subject_loading').html('');
            var data = JSON.parse(response);
            $.each(data, function (key) {
                var post =
                    '<tr>' +
                    '<td>' +
                    data[key].number +
                    '</td>' +
                    '<td>' +
                    data[key].title +
                    '</td>' +
                    '<td>' +
                    data[key].video +
                    '</td>' +
                    '<td>' +
                    data[key].video_source +
                    '</td>' +
                    '<td>' +
                    '<p style="width:200px; white-space:nowrap; overflow:hidden; text-overflow:ellipsis;">' + data[key].description + '</p>' +
                    '</td>' +
                    '<td>' +
                    data[key].subject_source +
                    '</td>' +
                    '<td>' +
                    data[key].date +
                    '</td>';

                    if (enabled == 'True') {
                        if (data[key].number == subject_number) {
                            post +=
                                '<td>' +
                                '<button class="btn btn-sm btn-primary" style="width:30px" disabled title="Disabled quiz status first"><i class="fa fa-edit"></i></button> ' +
                                '<button class="btn btn-sm btn-danger" style="width:30px; margin-top:5px" disabled title="Disabled quiz status first"><i class="fa fa-trash"></i></button>' +
                                '</td>';
                        }
                        else {
                            post +=
                                '<td>' +
                                '<button class="btn btn-sm btn-primary" style="width:30px" disabled title="Disabled quiz status first"><i class="fa fa-edit"></i></button> ' +
                                '</td>';
                        }
                    }

                    else {
                        if (data[key].number == subject_number) {
                            post +=
                                '<td>' +
                                '<a href="/Admin/Subject/Edit/' + data[key].encrypt_id + '" class="btn btn-primary btn-sm" style="width:30px" title="Edit"><i class="fa fa-edit"></i></a> ' +
                                '<button class="btn btn-sm btn-danger" style="width:30px; margin-top:5px" title="Delete" data-subject="' + data[key].encrypt_id + '" onclick="deleteSubject(this)"><i class="fa fa-trash"></i></button>' +
                                '</td>';
                        }
                        else {
                            post +=
                                '<td>' +
                                '<a href="/Admin/Subject/Edit/' + data[key].encrypt_id + '" class="btn btn-primary btn-sm" style="width:30px" title="Edit"><i class="fa fa-edit"></i></a> ' +
                                '</td>';
                        }
                    }
                    
                    post +=
                    '</tr>';
                $("#post_subject_body").append(post);
            });

            $("#post_subject").DataTable({
                 responsive: true,
                "scrollX": true
            });
        }
    });
});

$(function getAllQuiz() {
    $.ajax({
        url: "/Admin/ShowQuiz",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        method: 'GET',
        data: { courseid: course_id },
        beforeSend: function () {
            $('#quiz_loading').html('<div class="text-center"><p>Loading Data</p></div>');
        },
        success: function (response) {
            $('#quiz_loading').html('');
            var data = JSON.parse(response);
            $.each(data, function (key) {
                var post =
                    '<tr>' +
                    '<td>' +
                    data[key].number +
                    '</td>' +
                    '<td>' +
                    '<p style="width:150px; white-space:nowrap; overflow:hidden; text-overflow:ellipsis;">' + data[key].question + '</p>' +
                    '</td>' +
                    '<td>' +
                    data[key].opt_a +
                    '</td>' +
                    '<td>' +
                    data[key].opt_b +
                    '</td>' +
                    '<td>' +
                    data[key].opt_c +
                    '</td>' +
                    '<td>' +
                    data[key].opt_d +
                    '</td>' +
                    '<td>' +
                    data[key].answer +
                    '</td>';

                    if (enabled == 'True') {

                        if (data[key].number == subject_number) {
                            post +=
                                '<td>' +
                                '<button class="btn btn-sm btn-primary" style="width:30px; margin-top:5px" disabled title="Disabled quiz status first"><i class="fa fa-edit"></i></button> ' +
                                '<button class="btn btn-sm btn-danger" style="width:30px; margin-top:5px" disabled title="Disabled quiz status first"><i class="fa fa-trash"></i></button>' +
                                '</td>';
                        }
                        else {
                            post +=
                                '<td>' +
                                '<button class="btn btn-sm btn-primary" style="width:30px" disabled title="Disabled quiz status first"><i class="fa fa-edit"></i></button> ' +
                                '</td>';
                        }

                    }

                    else {

                        if (data[key].number == quiz_number) {
                            post +=
                                '<td>' +
                                '<a href="/Admin/Quiz/Edit/' + data[key].encrypt_id + '" class="btn btn-primary btn-sm" style="width:30px; margin-top:5px" title="Edit"><i class="fa fa-edit"></i></a> ' +
                                '<button class="btn btn-sm btn-danger" style="width:30px; margin-top:5px" title="Delete" data-quiz="' + data[key].encrypt_id + '" onclick="deleteQuiz(this)"><i class="fa fa-trash"></i></button>' +
                                '</td>';
                        }
                        else {
                            post +=
                                '<td>' +
                                '<a href="/Admin/Quiz/Edit/' + data[key].encrypt_id + '" class="btn btn-primary btn-sm" style="width:30px" title="Edit"><i class="fa fa-edit"></i></a> ' +
                                '</td>';
                        }
                    }

                    post +=
                    '</tr>';
                $("#post_quiz_body").append(post);
            });

            $("#post_quiz").DataTable({
                 responsive: true,
                "scrollX": true
            });

        }
    });
});

function deleteSubject(subject) {
    var encrypt_id = subject.getAttribute("data-subject");
    var r = confirm("Are you sure want to delete this subject?");
    if (r == true) {
    $.ajax({
        url: "/Admin/DeleteSubject",
        type: 'POST',
        data: { materi_encrypt_id: encrypt_id },
        success: function (response) {
            if (response == 1) {
                window.location.reload();
            } else {
                alert("failed to delete subject");
            }
        }
    });
    }
}

function deleteQuiz(subject) {
    var encrypt_id = subject.getAttribute("data-quiz");
    var r = confirm("Are you sure want to delete this quiz?");
    if (r == true) {
    $.ajax({
        url: "/Admin/DeleteQuiz",
        type: 'POST',
        data: { quiz_encrypt_id: encrypt_id },
        success: function (response) {
            if (response == 1) {
                window.location.reload();
            } else {
                alert("failed to delete quiz");
            }
        }
    });
    }
}