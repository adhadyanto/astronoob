﻿$(function () {
    $("#submitAds").click(function () {

        var location = $("#ads_location").val();
        var link = $("#ads_link").val();
        var customer = $("#ads_customer").val();
        var picture = $("#ads_picture")[0].files[0];

        var formData = new FormData();

        formData.append("location", location);
        formData.append("link", link);
        formData.append("customer", customer);
        formData.append("picture", picture);

        $.ajax({
            url: "/Admin/SaveAds",
            type: 'POST',
            data: formData,
            contentType: false,
            processData: false,
            success: function (data) {
                //window.location.reload();
            }
        });
    });
});

$(function getAllAds() {
    $.ajax({
        url: "/Admin/ShowAds",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        method: 'GET',
        success: function (response) {
            var data = JSON.parse(response);
            $.each(data, function (key) {
                var post =
                    '<tr>' +
                    '<td style="width:200px">' +
                    data[key].location +
                    '</td>' +
                    '<td>' +
                    '<img src="../../img/ads-img/' + data[key].picture + '" style="width:75px"/>' +
                    '</td>' +
                    '<td>' +
                    '<a href="' + data[key].link + '">' + data[key].link + '</a>' +
                    '</td>' +
                    '<td>' +
                    data[key].customer +
                    '</td>' +
                    '<td>' +
                    data[key].duration + ' days' +
                    '</td>' +
                    '<td>' +
                    '<a href="/Admin/Ads/Edit/' + data[key].encrypt_id + '" class="btn btn-primary btn-sm" title="Edit" style="width:30px" ><i class="fa fa-edit"></i></a> ' +
                    '</tr>';
                $("#post_ads_body").append(post);
            });
            $.noConflict();
            $("#post_ads").DataTable({
                responsive: true,
                "scrollX": true
            });

        }
    });
});
