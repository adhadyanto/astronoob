﻿$(function () {
    $("#submitCourse").click(function () {

        if ($("form").valid()) {

            var name = $("#course_name").val();
            var description = $("#course_desc").val();
            var badgeid = $("#course_badge").val();
            var badgeafter = $("#course_badge_after").val();
            var quizstat = $("#course_quiz_status").val();

            $.ajax({
                url: "/Admin/SaveCourse",
                type: 'POST',
                data: { name: name, description: description, badgeid: badgeid, badgeafter: badgeafter, quizstat: quizstat },
                success: function (data) {
                    if (data == 0) {
                        alert("Failed to save course");
                    }
                }

            });
        }
    });
});

$(function getAllCourse() {
    $.ajax({
        url: "/Admin/ShowCourse",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        method: 'GET',
        beforeSend: function () {
            $('#course_loading').html('<div class="text-center"><p>Loading Data</p></div>');
        },
        success: function (response) {
            $('#course_loading').html('');
            var data = JSON.parse(response);
            $.each(data, function (key) {
                var post =
                    '<tr>' +
                    '<td>' +
                    data[key].name +
                    '</td>' +
                    '<td>' +
                    data[key].description +
                    '</td>' +
                    '<td>' +
                    data[key].badgeid +
                    '</td>' +
                    '<td>' +
                    data[key].quiz_status +
                    '</td>' +
                    '<td>' +
                    '<a href="/Admin/Course/Edit/' + data[key].encrypt_id + '" class="btn btn-primary btn-sm" style="width:30px; margin-top:5px" title="Edit"><i class="fa fa-edit"></i></a> ' +
                    '<a href="/Admin/Course/' + data[key].encrypt_id + '" class="btn btn-primary btn-sm" style="margin-top:5px" title="Details" >Details</a>' +
                        '</td>' +
                        '</tr>';
                $("#post_course_body").append(post);
            });

            $.noConflict();

            $("#post_course").DataTable({
                responsive: true,
                "scrollX": true
            });
        }
    });
});
