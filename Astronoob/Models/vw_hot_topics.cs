﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TestAstro.Models
{
    public class vw_hot_topics
    {
        public int id { get; set; }
        public string title { get; set; }
        public string description { get; set; }
        public string encrypt_id { get; set; }
        public Nullable<System.DateTime> date { get; set; }
        public int count { get; set; }
    }

}