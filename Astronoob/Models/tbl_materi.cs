//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace TestAstro.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class tbl_materi
    {
        public int materiid { get; set; }
        public Nullable<int> courseid { get; set; }
        public string title { get; set; }
        public Nullable<System.DateTime> date { get; set; }
        public string description { get; set; }
        public Nullable<int> number { get; set; }
        public string encrypt_id { get; set; }
        public string video { get; set; }
        public string video_source { get; set; }
        public string subject_source { get; set; }
    
        public virtual tbl_course tbl_course { get; set; }
    }
}
