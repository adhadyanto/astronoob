﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TestAstro.Models
{
    [MetadataType(typeof(tbl_materi_Metadata))]

    public partial class tbl_materi
    {
    }

    public class tbl_materi_Metadata
    {
        [Required(ErrorMessage = "Title cannot be empty")]
        public string title { get; set; }

        [Required(ErrorMessage = "Description cannot be empty")]
        public string description { get; set; }

        [Required(ErrorMessage = "Number cannot be empty")]
        public Nullable<int> number { get; set; }
    }
}