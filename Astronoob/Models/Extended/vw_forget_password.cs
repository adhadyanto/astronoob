﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TestAstro.Models
{
    [MetadataType(typeof(vw_forget_password_Metadata))]

    public partial class vw_forget_password
    {
    }

    public class vw_forget_password_Metadata
    {
        [EmailAddress(ErrorMessage = "Invalid email format")]
        [Required(ErrorMessage = "Email cannot be empty")]
        //[Remote("EmailDoesntExist", "Login", AdditionalFields = "Id",
        //        ErrorMessage = "Email is not registered")]
        public string email { get; set; }
    }
}