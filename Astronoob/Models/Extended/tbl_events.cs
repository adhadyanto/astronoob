﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace TestAstro.Models
{
    [MetadataType(typeof(tbl_events_Metadata))]
    public partial class tbl_events
    {
        [NotMapped]
        public int count { get; set; }

    }

    public class tbl_events_Metadata
    {
    }

}