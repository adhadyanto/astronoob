﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TestAstro.Models
{
    [MetadataType(typeof(vw_login_Metadata))]

    public partial class vw_login
    {
    }

    public class vw_login_Metadata
    {
        [Required(ErrorMessage = "Username cannot be empty")]
        public string username { get; set; }

        [Required(ErrorMessage = "Password cannot be empty")]
        public string password { get; set; }
    }

}