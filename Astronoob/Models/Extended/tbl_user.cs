﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CompareAttribute = System.ComponentModel.DataAnnotations.CompareAttribute;

namespace TestAstro.Models
{
    [MetadataType(typeof(tbl_user_Metadata))]
    public partial class tbl_user
    {
        [Compare("password", ErrorMessage = "Password doesn't match")]
        [Required(ErrorMessage = "Confirm password cannot be empty")]
        [NotMapped]
        public string confirm_password { get; set; }
    }

    public class tbl_user_Metadata
    {
        [Required(ErrorMessage = "Username cannot be empty")]
        [RegularExpression(@"\w*$", ErrorMessage = "Must be alphanumerical or underscore")]
        [Remote("IsUsernameExist", "Login", AdditionalFields = "Id",
        ErrorMessage = "Username is already used")]
        [StringLength(20, MinimumLength = 5, ErrorMessage = "Username must be 5-20 characters")]
        public string username { get; set; }

        [Required(ErrorMessage = "Password cannot be empty")]
        [StringLength(20, MinimumLength = 8, ErrorMessage = "Password must be 8-20 characters")]
        public string password { get; set; }

        [Required(ErrorMessage = "Name cannot be empty")]
        public string name { get; set; }

        [Required(ErrorMessage = "Email cannot be empty")]
        [EmailAddress(ErrorMessage = "Invalid email format")]
        [Remote("IsEmailExist", "Login", AdditionalFields = "Id",
                ErrorMessage = "Email is already used")]
        public string email { get; set; }
    }
}