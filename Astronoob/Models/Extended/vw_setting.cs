﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace TestAstro.Models
{
    [MetadataType(typeof(vw_setting_Metadata))]

    public partial class vw_setting
    {
        [Required(ErrorMessage = "New password cannot be empty")]
        [NotMapped]
        public string new_password { get; set; }

        [Compare("new_password", ErrorMessage = "Password doesn't match")]
        [Required(ErrorMessage = "Confirm password cannot be empty")]
        [NotMapped]
        public string confirm_password { get; set; }
    }

    public class vw_setting_Metadata
    {
        [Required(ErrorMessage = "Old password cannot be empty")]
        public string password { get; set; }
    }
}