﻿using System;

namespace TestAstro.Models
{
    public class errLog
    {
        public static void ErrorLog(string username, string message, string location)
        {
            db_project db_error = new db_project();
            tbl_error_log_history error = new tbl_error_log_history();
            error.username = username;
            error.err_msg = message;
            error.err_loc = location;
            error.err_date = DateTime.Now;
            db_error.tbl_error_log_history.Add(error);
            db_error.SaveChanges();
        }

    }
}