﻿using Microsoft.Reporting.WebForms;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.ModelBinding;
using System.Web.Mvc;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using TestAstro.Models;

namespace TestAstro.Controllers
{
    [Authorize(Roles="admin")]
    public class AdminController : Controller
    {
        db_project db = new db_project();

        [Route("Admin/User")]
        public ActionResult User()
        {
            return View(db.tbl_user.ToList());
        }

        [Route("Admin/User/{username}")]
        public ActionResult UserDetail()
        {
            var username = RouteData.Values["username"].ToString();
            var user = db.tbl_user.Where(m => m.username == username).FirstOrDefault();
            var badge_name = db.tbl_badge.Where(m => m.badgeid == user.badgeid).Select(m => m.name).FirstOrDefault();
            var prem_days = db.tbl_premium_user.Where(m => m.userid == user.id).Select(m => m.days).FirstOrDefault();

            ViewBag.badge_name = badge_name;
            ViewBag.prem_days = prem_days;

            if (user != null) {
                return View(user);
            }
            else
            {
                TempData["Error"] = "user Not Found :'(";
                return RedirectToAction("Error", "User");
            }
        }

        [Authorize]
        [Route("Admin/Card/{username}")]
        public ActionResult UserCard()
        {
            string ssrsUrl = ConfigurationManager.AppSettings["SSRSReportsURL"].ToString();
            ReportViewer report = new ReportViewer();

            report.ProcessingMode = ProcessingMode.Remote;
            report.SizeToReportContent = true;
            report.Width = Unit.Percentage(100);
            report.Height = Unit.Percentage(100);
            report.ZoomMode = ZoomMode.FullPage;
            report.ServerReport.ReportServerUrl = new Uri(ssrsUrl);
            ReportParameter[] reportParameters = new ReportParameter[1];
            reportParameters[0] = new ReportParameter("username", RouteData.Values["username"].ToString());
            report.ServerReport.ReportPath = "/UserReport";
            report.ServerReport.SetParameters(reportParameters);
            report.ServerReport.Refresh();
            ViewBag.ReportViewer = report;
            return View();
        }

        [Authorize]
        public JsonResult UserCourseResult(string username)
        {
            db.Configuration.ProxyCreationEnabled = false;
            var userid = db.tbl_user.Where(m => m.username == username).Select(m => m.id).FirstOrDefault();

            var query = db.tbl_course_history.Join(db.tbl_course, d => d.courseid, u => u.courseid, (d, u) => new
            {
                d.userid,
                d.grade,
                d.date,
                u.name
            }).Where(d => d.userid == userid).OrderBy(d => d.date).ToList();

            var list = JsonConvert.SerializeObject(query, Formatting.Indented, new JsonSerializerSettings
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            });

            var jsonResult = Json(list, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;

            return jsonResult;
        }

        [Authorize]
        public JsonResult UserDiscPost(string username)
        {
            db.Configuration.ProxyCreationEnabled = false;
            var userid = db.tbl_user.Where(m => m.username == username).Select(m => m.id).FirstOrDefault();

            var query = db.tbl_discussion.Where(d => d.userid == userid).OrderBy(d => d.date).ToList();

            var list = JsonConvert.SerializeObject(query, Formatting.Indented, new JsonSerializerSettings
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            });

            var jsonResult = Json(list, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;

            return jsonResult;
        }

        [Authorize]
        public JsonResult UserDiscComment(string username)
        {
            //db.Configuration.ProxyCreationEnabled = false;
            var userid = db.tbl_user.Where(m => m.username == username).Select(m => m.id).FirstOrDefault();

            var query = db.tbl_discussion_comment.Join(db.tbl_discussion, d => d.discussionid, u => u.id, (d, u) => new
            {
                d.comment,
                d.userid,
                d.date,
                u.encrypt_id
            }).Where(d => d.userid == userid).OrderBy(d => d.date).ToList();

            var list = JsonConvert.SerializeObject(query, Formatting.Indented, new JsonSerializerSettings
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            });

            var jsonResult = Json(list, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;

            return jsonResult;
        }

        [Route("Admin/Log")]
        public ActionResult Log()
        {
            return View(db.tbl_error_log_history.ToList());
        }

        [Route("Admin/Course")]
        // GET: Admin
        public ActionResult Index()
        {
            ViewBag.badge = db.tbl_badge.OrderByDescending(m => m.badgeid).Take(1).ToList();
            return View();
        }

        [Route("Admin/Course/{encrypt_id}")]
        public ActionResult Subject(string encrypt_id)
        {
            var course = db.tbl_course.Where(u => u.encrypt_id == encrypt_id).FirstOrDefault();
            if (course != null)
            {
                ViewBag.courseid = course.courseid;
                //ViewBag.subject = db.tbl_materi.Where(u => u.courseid == course.courseid).ToList();
                //ViewBag.quiz = db.tbl_quiz.Where(u => u.courseid == course.courseid).ToList();

                var subject_number = db.tbl_materi.Where(u => u.courseid == course.courseid).Count();
                var quiz_number = db.tbl_quiz.Where(u => u.courseid == course.courseid).Count();

                ViewBag.subject_number = subject_number + 1;
                ViewBag.quiz_number = quiz_number + 1;

                ViewBag.subject_num_del = subject_number;
                ViewBag.quiz_num_del = quiz_number;

                ViewBag.enabled = course.quiz_status;

                return View();
            }
            else {
                TempData["Error"] = "Course Not Found :'(";
                return RedirectToAction("Error", "User");
            }
        }

        [HttpPost]
        public int SaveCourse(string name, string description, int badgeid, string badgeafter, bool quizstat)
        {
            using (var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    tbl_course course = new tbl_course();
                    course.name = name;
                    course.description = description;
                    course.badgeid = badgeid;
                    course.quiz_status = quizstat;
                    course.encrypt_id = Guid.NewGuid().ToString();
                    db.tbl_course.Add(course);

                    tbl_badge badge = new tbl_badge();
                    badge.name = badgeafter;
                    db.tbl_badge.Add(badge);

                    db.SaveChanges();

                    transaction.Commit();

                    return 1;
                }
                
                catch(Exception error){
                    transaction.Rollback();
                    errLog.ErrorLog("Admin", error.Message, "Add Course");
                    return 0;
                }
            }   
        }

        public JsonResult ShowCourse()
        {
            db.Configuration.ProxyCreationEnabled = false;
            var query = db.tbl_course.ToList();

            var list = JsonConvert.SerializeObject(query, Formatting.Indented, new JsonSerializerSettings
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            });

            var jsonResult = Json(list, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;

            return jsonResult;
        }


        [HttpPost]
        public int SaveSubject(int courseid, int number, string title, string video, string vid_source, string description, string sub_source)
        {
            using (var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    tbl_materi materi = new tbl_materi();
                    materi.courseid = courseid;
                    materi.title = title;
                    materi.video = video;
                    materi.video_source = vid_source;
                    materi.number = number;
                    materi.description = description;
                    materi.subject_source = sub_source;
                    materi.date = DateTime.Now;
                    materi.encrypt_id = Guid.NewGuid().ToString();
                    db.tbl_materi.Add(materi);
                    db.SaveChanges();

                    transaction.Commit();

                    return 1;
                }

                catch (Exception error)
                {
                    transaction.Rollback();
                    errLog.ErrorLog("Admin", error.Message, "Add Subject");
                    return 0;
                }
            }
        }

        public JsonResult ShowSubject(int courseid)
        {
            db.Configuration.ProxyCreationEnabled = false;
            var query = db.tbl_materi.Where(m => m.courseid == courseid).OrderBy(m => m.number).ToList();

            var list = JsonConvert.SerializeObject(query, Formatting.Indented, new JsonSerializerSettings
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            });

            var jsonResult = Json(list, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;

            return jsonResult;
        }

        [HttpPost]
        public int SaveQuiz(int courseid, string question, string opt_a, string opt_b, string opt_c, string opt_d, string answer, int number)
        {
            using (var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    tbl_quiz quiz = new tbl_quiz();
                    quiz.courseid = courseid;
                    quiz.question = question;
                    quiz.opt_a = opt_a;
                    quiz.opt_b = opt_b;
                    quiz.opt_c = opt_c;
                    quiz.opt_d = opt_d;
                    quiz.answer = answer;
                    quiz.number = number;
                    quiz.encrypt_id = Guid.NewGuid().ToString();
                    db.tbl_quiz.Add(quiz);
                    db.SaveChanges();

                    transaction.Commit();

                    return 1;
                }

                catch (Exception error)
                {
                    transaction.Rollback();
                    errLog.ErrorLog("Admin", error.Message, "Add Quiz");
                    return 0;
                }
            }
        }

        public JsonResult ShowQuiz(int courseid)
        {
            db.Configuration.ProxyCreationEnabled = false;
            var query = db.tbl_quiz.Where(m => m.courseid == courseid).OrderBy(m=>m.number).ToList();

            var list = JsonConvert.SerializeObject(query, Formatting.Indented, new JsonSerializerSettings
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            });

            var jsonResult = Json(list, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;

            return jsonResult;
        }

        [Route("Admin/Course/Edit/{encrypt_id}")]
        public ActionResult EditCourse(string encrypt_id)
        {
            ViewBag.badge = db.tbl_badge.ToList();
            return View(db.tbl_course.Where(u => u.encrypt_id == encrypt_id).FirstOrDefault());
        }

        [Route("Admin/Course/Edit/{encrypt_id}")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditCourse(tbl_course param_course)
        {
            using (var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    tbl_course course = db.tbl_course.Find(param_course.courseid);
                    course.name = param_course.name;
                    course.description = param_course.description;
                    course.quiz_status = param_course.quiz_status;
                    db.SaveChanges();

                    transaction.Commit();

                    return RedirectToAction("Index");
                }
                catch (Exception error)
                {
                    transaction.Rollback();
                    errLog.ErrorLog("Admin", error.Message, "Edit Course");
                    TempData["Error"] = "Failed to edit course :'(";
                    return RedirectToAction("Error", "User");
                }
            }
        }

        [Route("Admin/Subject/Edit/{encrypt_id}")]
        public ActionResult EditSubject(string encrypt_id)
        {
            var course_id = db.tbl_materi.Where(m => m.encrypt_id == encrypt_id).Select(m => m.courseid).FirstOrDefault();
            var course_encrypt_id = db.tbl_course.Where(m => m.courseid == course_id).Select(m => m.encrypt_id).FirstOrDefault();
            ViewBag.encrypt_id = course_encrypt_id;
            return View(db.tbl_materi.Where(u => u.encrypt_id == encrypt_id).FirstOrDefault());
        }

        [Route("Admin/Subject/Edit/{encrypt_id}")]
        [HttpPost]
        public ActionResult EditSubject(tbl_materi param_materi)
        {
            using (var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    tbl_materi materi = db.tbl_materi.Find(param_materi.materiid);
                    materi.title = param_materi.title;
                    materi.video = param_materi.video;
                    materi.video_source = param_materi.video_source;
                    materi.description = param_materi.description;
                    materi.subject_source = param_materi.subject_source;
                    db.SaveChanges();

                    transaction.Commit();

                    var encrypt_id = db.tbl_course.Where(m => m.courseid == materi.courseid).Select(m => m.encrypt_id).FirstOrDefault();

                    return RedirectToAction("Subject", new { encrypt_id = encrypt_id});
                }
                catch (Exception error)
                {
                    transaction.Rollback();
                    errLog.ErrorLog("Admin", error.Message, "Edit Subject");
                    TempData["Error"] = "Failed to edit subject :'(";
                    return RedirectToAction("Error", "User");
                }
            }
        }

        [HttpPost]
        public int DeleteSubject(string materi_encrypt_id)
        {
            using (var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    tbl_materi tbl_materi = db.tbl_materi.Where(m => m.encrypt_id == materi_encrypt_id).FirstOrDefault();
                    var encrypt_id = db.tbl_course.Where(m => m.courseid == tbl_materi.courseid).Select(m => m.encrypt_id).FirstOrDefault();
                    db.tbl_materi.Remove(tbl_materi);
                    db.SaveChanges();
                    transaction.Commit();
                    return 1;
                }
                catch (Exception error) {
                    transaction.Rollback();
                    errLog.ErrorLog("Admin", error.Message, "Delete Subject");
                    return 0;
                }
            }
        }

        [Route("Admin/Quiz/Edit/{encrypt_id}")]
        public ActionResult EditQuiz(string encrypt_id)
        {
            var course_id = db.tbl_quiz.Where(m => m.encrypt_id == encrypt_id).Select(m => m.courseid).FirstOrDefault();
            var course_encrypt_id = db.tbl_course.Where(m => m.courseid == course_id).Select(m => m.encrypt_id).FirstOrDefault();
            ViewBag.encrypt_id = course_encrypt_id;
            return View(db.tbl_quiz.Where(u => u.encrypt_id == encrypt_id).FirstOrDefault());
        }

        [Route("Admin/Quiz/Edit/{encrypt_id}")]
        [HttpPost]
        public ActionResult EditQuiz(tbl_quiz param_quiz)
        {
            using (var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    tbl_quiz quiz = db.tbl_quiz.Find(param_quiz.quizid);
                    quiz.question = param_quiz.question;
                    quiz.opt_a = param_quiz.opt_a;
                    quiz.opt_b = param_quiz.opt_b;
                    quiz.opt_c = param_quiz.opt_c;
                    quiz.opt_d = param_quiz.opt_d;
                    quiz.answer = param_quiz.answer;

                    db.SaveChanges();

                    transaction.Commit();

                    var encrypt_id = db.tbl_course.Where(m => m.courseid == quiz.courseid).Select(m => m.encrypt_id).FirstOrDefault();

                    return RedirectToAction("Subject", new { encrypt_id = encrypt_id });
                }
                catch (Exception error)
                {
                    transaction.Rollback();
                    errLog.ErrorLog("Admin", error.Message, "Edit Quiz");
                    TempData["Error"] = "Failed to edit quiz :'(";
                    return RedirectToAction("Error", "User");
                }
            }
        }

        [HttpPost]
        public int DeleteQuiz(string quiz_encrypt_id)
        {
            using (var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    tbl_quiz tbl_quiz = db.tbl_quiz.Where(m => m.encrypt_id == quiz_encrypt_id).FirstOrDefault();

                    var encrypt_id = db.tbl_course.Where(m => m.courseid == tbl_quiz.courseid).Select(m => m.encrypt_id).FirstOrDefault();

                    db.tbl_quiz.Remove(tbl_quiz);
                    db.SaveChanges();

                    transaction.Commit();

                    return 1;
                }
                catch (Exception error)
                {
                    transaction.Rollback();
                    errLog.ErrorLog("Admin", error.Message, "Delete Quiz");
                    return 0;
                }
            }
        }

        public ActionResult Event()
        {
            return View();
        }

        [HttpPost]
        public int SaveEvent(string title, string description, string location, string contact, string date, string time, HttpPostedFileBase photo)
        {
            using (var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    if(photo != null)
                    {
                        string editedFileName = Guid.NewGuid().ToString() + "-" + photo.FileName;
                        string filePath = "~/img/event-img/" + editedFileName;
                        photo.SaveAs(Server.MapPath(filePath));

                        tbl_events events = new tbl_events();
                        events.title = title;
                        events.description = description;
                        events.location = location;
                        events.contact_person = contact;
                        events.date = DateTime.Parse(date);
                        events.date_posted = DateTime.Now;
                        events.time = TimeSpan.Parse(time);
                        events.photo = editedFileName;
                        events.encrypt_id = Guid.NewGuid().ToString();
                        db.tbl_events.Add(events);
                        db.SaveChanges();
                        transaction.Commit();
                        return 1;
                    }

                    return 0;
                    
                }
                catch (Exception error)
                {
                    transaction.Rollback();
                    errLog.ErrorLog("Admin", error.Message, "Add Events");
                    return 0;
                }
            }
        }

        public JsonResult ShowEvent()
        {
            db.Configuration.ProxyCreationEnabled = false;
            var query = db.tbl_events.ToList();

            var list = JsonConvert.SerializeObject(query, Formatting.Indented, new JsonSerializerSettings
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            });

            var jsonResult = Json(list, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;

            return jsonResult;
        }

        [Route("Admin/Event/Edit/{encrypt_id}")]
        public ActionResult EditEvent(string encrypt_id)
        {
            return View(db.tbl_events.Where(m=>m.encrypt_id == encrypt_id).FirstOrDefault());
        }

        [Route("Admin/Event/Edit/{encrypt_id}")]
        [HttpPost]
        public ActionResult EditEvent(tbl_events param_events)
        {
            using (var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    tbl_events events = db.tbl_events.Find(param_events.id);
                    events.title = param_events.title;
                    events.description = param_events.description;
                    events.location = param_events.location;
                    events.contact_person = param_events.contact_person;
                    events.date = param_events.date;
                    events.time = param_events.time;
                    db.SaveChanges();
                    transaction.Commit();

                    return RedirectToAction("Event");
                }
                catch (Exception error)
                {
                    transaction.Rollback();
                    errLog.ErrorLog("Admin", error.Message, "Edit Events");
                    TempData["Error"] = "Failed to edit event :'(";
                    return RedirectToAction("Error", "User");
                }
            }
        }

        //[Route("Admin/Event/Edit/{encrypt_id}")]
        [HttpPost]
        public ActionResult EditEventPicture(HttpPostedFileBase event_pic, string encrypt_id)
        {
            using (var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    if (event_pic != null)
                    {
                        string editedFileName = Guid.NewGuid().ToString() + "-" + event_pic.FileName;
                        string filePath = "~/img/event-img/" + editedFileName;
                        event_pic.SaveAs(Server.MapPath(filePath));

                        tbl_events events = db.tbl_events.Where(m => m.encrypt_id == encrypt_id).FirstOrDefault();

                        var old_pp = "~/img/event-img/" + events.photo;

                        events.photo = editedFileName;

                        db.Configuration.ValidateOnSaveEnabled = false;
                        db.SaveChanges();
                        transaction.Commit();

                        System.IO.File.Delete(Server.MapPath(old_pp));
                    }
                    else
                    {
                        TempData["error_pp"] = "There's no file attached";
                    }

                    return RedirectToAction("EditEvent", new { encrypt_id = encrypt_id });

                }
                catch (Exception error)
                {
                    transaction.Rollback();
                    errLog.ErrorLog("Admin", error.Message, "Edit Events");
                    TempData["Error"] = "Failed to edit event :'(";
                    return RedirectToAction("Error", "User");
                }
            }
        }

        [HttpPost]
        public int DeleteEvent(string encrypt_id)
        {
            using (var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    tbl_events tbl_events = db.tbl_events.Where(m => m.encrypt_id == encrypt_id).FirstOrDefault();

                    //var encrypt_id = db.tbl_course.Where(m => m.courseid == tbl_quiz.courseid).Select(m => m.encrypt_id).FirstOrDefault();
                    var old_pp = "~/img/event-img/" + tbl_events.photo;

                    db.tbl_events.Remove(tbl_events);
                    db.SaveChanges();
                    transaction.Commit();

                    System.IO.File.Delete(Server.MapPath(old_pp));

                    return 1;
                }
                catch (Exception error)
                {
                    transaction.Rollback();
                    errLog.ErrorLog("Admin", error.Message, "Edit Events");
                    return 0;
                }
            }
        }

        public ActionResult News()
        {
            return View();
        }

        [HttpPost]
        public int SaveNews(string title, string description, string source, HttpPostedFileBase photo)
        {
            using (var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    /*if(photo != null)
                    {*/
                        string editedFileName = Guid.NewGuid().ToString() + "-" + photo.FileName;
                        string filePath = "~/img/news-img/" + editedFileName;
                        photo.SaveAs(Server.MapPath(filePath));

                        tbl_news news = new tbl_news();
                        news.title = title;
                        news.description = description;
                        news.source = source;
                        news.date_posted = DateTime.Now;
                        news.photo = editedFileName;
                        news.encrypt_id = Guid.NewGuid().ToString();
                        db.tbl_news.Add(news);
                        db.SaveChanges();
                        transaction.Commit();

                        return 1;
                    /*}*/

                   /* return 0; */
                    
                }
                catch (Exception error)
                {
                    transaction.Rollback();
                    errLog.ErrorLog("Admin", error.Message, "Add News");
                    return 0;
                }
            }
        }

        public JsonResult ShowNews()
        {
            db.Configuration.ProxyCreationEnabled = false;
            var query = db.tbl_news.ToList();

            var list = JsonConvert.SerializeObject(query, Formatting.Indented, new JsonSerializerSettings
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            });

            var jsonResult = Json(list, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;

            return jsonResult;
        }

        [Route("Admin/News/Edit/{encrypt_id}")]
        public ActionResult EditNews(string encrypt_id)
        {
            return View(db.tbl_news.Where(m => m.encrypt_id == encrypt_id).FirstOrDefault());
        }

        [Route("Admin/News/Edit/{encrypt_id}")]
        [HttpPost]
        public ActionResult EditNews(tbl_news param_news)
        {
            using (var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    tbl_news news = db.tbl_news.Find(param_news.id);
                    news.title = param_news.title;
                    news.description = param_news.description;
                    news.source = param_news.source;

                    db.SaveChanges();
                    transaction.Commit();

                    return RedirectToAction("News");
                }
                catch (Exception error)
                {
                    transaction.Rollback();
                    errLog.ErrorLog("Admin", error.Message, "Edit News");
                    TempData["Error"] = "Failed to edit news :'(";
                    return RedirectToAction("Error", "User");
                }
            }
        }

        [HttpPost]
        public ActionResult EditNewsPicture(HttpPostedFileBase news_pic, string encrypt_id)
        {
            using (var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    if (news_pic != null)
                    {
                        string editedFileName = Guid.NewGuid().ToString() + "-" + news_pic.FileName;
                        string filePath = "~/img/news-img/" + editedFileName;
                        news_pic.SaveAs(Server.MapPath(filePath));

                        tbl_news news = db.tbl_news.Where(m => m.encrypt_id == encrypt_id).FirstOrDefault();

                        var old_pp = "~/img/news-img/" + news.photo;

                        news.photo = editedFileName;

                        db.Configuration.ValidateOnSaveEnabled = false;
                        db.SaveChanges();
                        transaction.Commit();
                        System.IO.File.Delete(Server.MapPath(old_pp));
                    }
                    else
                    {
                        TempData["error_pp"] = "There's no file attached";
                    }

                    return RedirectToAction("EditNews", new { encrypt_id = encrypt_id });
                }
                catch (Exception error)
                {
                    transaction.Rollback();
                    errLog.ErrorLog("Admin", error.Message, "Edit News");
                    TempData["Error"] = "Failed to edit news :'(";
                    return RedirectToAction("Error", "User");
                }
            }
        }

        [HttpPost]
        public int DeleteNews(string news_encrypt_id)
        {
            using (var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    tbl_news tbl_news = db.tbl_news.Where(m => m.encrypt_id == news_encrypt_id).FirstOrDefault();

                    //var encrypt_id = db.tbl_course.Where(m => m.courseid == tbl_quiz.courseid).Select(m => m.encrypt_id).FirstOrDefault();
                    var old_pp = "~/img/news-img/" + tbl_news.photo;

                    db.tbl_news.Remove(tbl_news);
                    db.SaveChanges();

                    transaction.Commit();

                    System.IO.File.Delete(Server.MapPath(old_pp));

                    return 1;
                }
                catch (Exception error)
                {
                    transaction.Rollback();
                    errLog.ErrorLog("Admin", error.Message, "Delete News");
                    return 0;
                }
            }
        }

        [Route("Admin/SSRS/User")]
        public ActionResult MonthlyUserRegisterReport()
        {
            string ssrsUrl = ConfigurationManager.AppSettings["SSRSReportsURL"].ToString();
            ReportViewer report = new ReportViewer();

            report.ProcessingMode = ProcessingMode.Remote;
            report.SizeToReportContent = true;
            report.Width = Unit.Percentage(100);
            report.Height = Unit.Percentage(100);
            report.ZoomMode = ZoomMode.FullPage;

            /*report.ProcessingMode = ProcessingMode.Remote;
            report.Width = Unit.Pixel(1100);*/

            report.ServerReport.ReportServerUrl = new Uri(ssrsUrl);
            //ReportParameter[] reportParameters = new ReportParameter[1];
            //reportParameters[0] = new ReportParameter("Firstname", "Ken");
            report.ServerReport.ReportPath = "/MonthlyRegisterReport";
            //report.ServerReport.SetParameters(reportParameters);
            report.ServerReport.Refresh();
            ViewBag.ReportViewer = report;
            return View();
        }

        [Route("Admin/SSRS/Payment")]
        public ActionResult PaymentReport()
        {
            string ssrsUrl = ConfigurationManager.AppSettings["SSRSReportsURL"].ToString();
            ReportViewer report = new ReportViewer();
            report.ProcessingMode = ProcessingMode.Remote;
            report.SizeToReportContent = true;
            report.Width = Unit.Percentage(100);
            report.Height = Unit.Percentage(100);
            report.ZoomMode = ZoomMode.FullPage;
            report.ServerReport.ReportServerUrl = new Uri(ssrsUrl);
            //ReportParameter[] reportParameters = new ReportParameter[1];
            //reportParameters[0] = new ReportParameter("Firstname", "Ken");
            report.ServerReport.ReportPath = "/PaymentReport";
            //report.ServerReport.SetParameters(reportParameters);
            report.ServerReport.Refresh();
            ViewBag.ReportViewer = report;
            return View();
        }

        [Route("Admin/SSRS/Course")]
        public ActionResult PassingCourseReport()
        {
            string ssrsUrl = ConfigurationManager.AppSettings["SSRSReportsURL"].ToString();
            ReportViewer report = new ReportViewer();
            report.ProcessingMode = ProcessingMode.Remote;
            report.SizeToReportContent = true;
            report.Width = Unit.Percentage(100);
            report.Height = Unit.Percentage(100);
            report.ZoomMode = ZoomMode.FullPage;
            report.ServerReport.ReportServerUrl = new Uri(ssrsUrl);
            //ReportParameter[] reportParameters = new ReportParameter[1];
            //reportParameters[0] = new ReportParameter("Firstname", "Ken");
            report.ServerReport.ReportPath = "/CoursePassingReport";
            //report.ServerReport.SetParameters(reportParameters);
            report.ServerReport.Refresh();
            ViewBag.ReportViewer = report;
            return View();
        }

        [Route("Admin/SSRS/Participant")]
        public ActionResult EventParticipantReport()
        {
            string ssrsUrl = ConfigurationManager.AppSettings["SSRSReportsURL"].ToString();
            ReportViewer report = new ReportViewer();
            report.ProcessingMode = ProcessingMode.Remote;
            report.SizeToReportContent = true;
            report.Width = Unit.Percentage(100);
            report.Height = Unit.Percentage(100);
            report.ZoomMode = ZoomMode.FullPage;
            report.ServerReport.ReportServerUrl = new Uri(ssrsUrl);
            //ReportParameter[] reportParameters = new ReportParameter[1];
            //reportParameters[0] = new ReportParameter("Firstname", "Ken");
            report.ServerReport.ReportPath = "/EventParticipantReport";
            //report.ServerReport.SetParameters(reportParameters);
            report.ServerReport.Refresh();
            ViewBag.ReportViewer = report;
            return View();
        }

        [Route("Admin/Ads")]
        public ActionResult Ads()
        {
            return View();
        }

        [HttpPost]
        public ActionResult SaveAds(string location, string link, string customer, HttpPostedFileBase picture)
        {
            using (var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    string editedFileName = Guid.NewGuid().ToString() + "-" + picture.FileName;
                    string filePath = "~/img/ads-img/" + editedFileName;
                    picture.SaveAs(Server.MapPath(filePath));

                    tbl_ads ads = new tbl_ads();
                    ads.location = location;
                    ads.link = link;
                    ads.customer = customer;
                    ads.picture = editedFileName;
                    ads.encrypt_id = Guid.NewGuid().ToString();
                    db.tbl_ads.Add(ads);
                    db.SaveChanges();
                    transaction.Commit();
                    return RedirectToAction("Ads");
                }
                catch (Exception error)
                {
                    transaction.Rollback();
                    errLog.ErrorLog("Admin", error.Message, "Add Ads");
                    TempData["Error"] = "Failed to save Ads :'(";
                    return RedirectToAction("Error", "User");
                }
            }
        }

        public JsonResult ShowAds()
        {
            db.Configuration.ProxyCreationEnabled = false;
            var query = db.tbl_ads.OrderBy(m=>m.id).ToList();

            var list = JsonConvert.SerializeObject(query, Formatting.Indented, new JsonSerializerSettings
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            });

            var jsonResult = Json(list, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;

            return jsonResult;
        }

        [Route("Admin/Ads/Edit/{encrypt_id}")]
        public ActionResult EditAds(string encrypt_id)
        {
            return View(db.tbl_ads.Where(m => m.encrypt_id == encrypt_id).FirstOrDefault());
        }

        [Route("Admin/Ads/Edit/{encrypt_id}")]
        [HttpPost]
        public ActionResult EditAds(tbl_ads param_ads)
        {
            using (var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    tbl_ads ads = db.tbl_ads.Find(param_ads.id);
                    ads.link = param_ads.link;
                    ads.customer = param_ads.customer;
                    ads.duration = param_ads.duration;

                    db.SaveChanges();
                    transaction.Commit();

                    return RedirectToAction("Ads");
                }
                catch (Exception error)
                {
                    transaction.Rollback();
                    errLog.ErrorLog("Admin", error.Message, "Edit Ads");
                    TempData["Error"] = "Failed to edit ads :'(";
                    return RedirectToAction("Error", "User");
                }
            }
        }

        [HttpPost]
        public ActionResult EditAdsPicture(HttpPostedFileBase ads_pic, string encrypt_id)
        {
            using (var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    if (ads_pic != null)
                    {
                        string editedFileName = Guid.NewGuid().ToString() + "-" + ads_pic.FileName;
                        string filePath = "~/img/ads-img/" + editedFileName;
                        ads_pic.SaveAs(Server.MapPath(filePath));

                        tbl_ads ads = db.tbl_ads.Where(m => m.encrypt_id == encrypt_id).FirstOrDefault();

                        var old_pp = "~/img/ads-img/" + ads.picture;

                        ads.picture = editedFileName;

                        db.Configuration.ValidateOnSaveEnabled = false;
                        db.SaveChanges();
                        transaction.Commit();
                        System.IO.File.Delete(Server.MapPath(old_pp));
                    }
                    else
                    {
                        TempData["error_pp"] = "There's no file attached";
                    }

                    return RedirectToAction("EditAds", new { encrypt_id = encrypt_id });
                }
                catch (Exception error)
                {
                    transaction.Rollback();
                    errLog.ErrorLog("Admin", error.Message, "Edit Ads");
                    TempData["Error"] = "Failed to edit ads :'(";
                    return RedirectToAction("Error", "User");
                }
            }
        }

        public ActionResult Reports()
        {
            ViewBag.DiscussionReport = db.tbl_discussion_report.ToList();
            ViewBag.DiscussionCommentReport = db.tbl_discussion_comment_report.ToList();
            ViewBag.NewsCommentReport = db.tbl_news_comment_report.ToList();
            ViewBag.EventCommentReport = db.tbl_event_comment_report.ToList();
            return View();
        }

        public JsonResult DiscussionReportDetail(int discussionid)
        {
            db.Configuration.ProxyCreationEnabled = false;
            var query = db.tbl_discussion.Find(discussionid);
            var list = JsonConvert.SerializeObject(query, Formatting.Indented, new JsonSerializerSettings()
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            });

            var jsonResult = Json(list, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;

            return jsonResult;
        }

        public JsonResult DiscussionCommentReportDetail(int discussioncommentid)
        {
            db.Configuration.ProxyCreationEnabled = false;
            var query = db.tbl_discussion_comment.Find(discussioncommentid);
            var list = JsonConvert.SerializeObject(query, Formatting.Indented, new JsonSerializerSettings()
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            });

            var jsonResult = Json(list, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;

            return jsonResult;
        }

        public JsonResult NewsCommentReportDetail(int newscommentid)
        {
            db.Configuration.ProxyCreationEnabled = false;
            var query = db.tbl_news_comment.Find(newscommentid);
            var list = JsonConvert.SerializeObject(query, Formatting.Indented, new JsonSerializerSettings()
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            });

            var jsonResult = Json(list, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;

            return jsonResult;
        }

        public JsonResult EventCommentReportDetail(int eventcommentid)
        {
            db.Configuration.ProxyCreationEnabled = false;
            var query = db.tbl_event_comment.Find(eventcommentid);
            var list = JsonConvert.SerializeObject(query, Formatting.Indented, new JsonSerializerSettings()
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            });

            var jsonResult = Json(list, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = int.MaxValue;

            return jsonResult;
        }

        [HttpPost]
        public void DeleteDiscussionReport(int reportid)
        {
            using (var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    var report = db.tbl_discussion_report.Find(reportid);
                    db.tbl_discussion_report.Remove(report);
                    db.SaveChanges();
                    transaction.Commit();
                }
                catch (Exception error)
                {
                    transaction.Rollback();
                    errLog.ErrorLog("Admin", error.Message, "Delete Discussion Report");
                }
            }
        }

        [HttpPost]
        public void DeleteDiscussionCommentReport(int reportid)
        {
            using (var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    var report = db.tbl_discussion_comment_report.Find(reportid);
                    db.tbl_discussion_comment_report.Remove(report);
                    db.SaveChanges();
                    transaction.Commit();
                }
                catch (Exception error)
                {
                    transaction.Rollback();
                    errLog.ErrorLog("Admin", error.Message, "Delete Discussion Comment Report");
                }
            }
        }

        [HttpPost]
        public void DeleteNewsCommentReport(int reportid)
        {
            using (var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    var report = db.tbl_news_comment_report.Find(reportid);
                    db.tbl_news_comment_report.Remove(report);
                    db.SaveChanges();
                    transaction.Commit();
                }
                catch (Exception error)
                {
                    transaction.Rollback();
                    errLog.ErrorLog("Admin", error.Message, "Delete News Comment Report");
                }
            }
        }


        [HttpPost]
        public void DeleteEventCommentReport(int reportid)
        {
            using (var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    var report = db.tbl_event_comment_report.Find(reportid);
                    db.tbl_event_comment_report.Remove(report);
                    db.SaveChanges();
                    transaction.Commit();
                }
                catch (Exception error)
                {
                    transaction.Rollback();
                    errLog.ErrorLog("Admin", error.Message, "Delete Event Comment Report");
                }
            }
        }
    }
}