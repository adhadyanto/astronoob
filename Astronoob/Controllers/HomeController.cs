﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using TestAstro.Models;
using PagedList;

namespace TestAstro.Controllers
{
    public class HomeController : Controller
    {
        db_project db = new db_project();

        public ActionResult Index()
        {
            try
            {
                if (!User.Identity.IsAuthenticated || !User.IsInRole("admin"))
                {
                    ViewBag.news = db.tbl_news.OrderBy(m => m.date_posted).ToList().Take(3);
                    ViewBag.events = db.tbl_events.OrderBy(m => m.date_posted).ToList().Take(3);
                    ViewBag.homeleftad = db.tbl_ads.Where(m => m.location == "homepageleft").FirstOrDefault();
                    ViewBag.homerightad = db.tbl_ads.Where(m => m.location == "homepageright").FirstOrDefault();

                    ViewBag.hot_topics = (from d in db.tbl_discussion
                                          join dc in db.tbl_discussion_comment on d.id equals dc.discussionid into a
                                          //group d by d.id, d.title, d.description into grp
                                          orderby a.Count() descending
                                          select new vw_hot_topics { id = d.id, title = d.title, description = d.description, date = d.date, encrypt_id = d.encrypt_id, count = a.Count() }).Take(5);

                    ViewBag.latest_topics = (from d in db.tbl_discussion
                                             join dc in db.tbl_discussion_comment on d.id equals dc.discussionid into a
                                             //group d by d.id, d.title, d.description into grp
                                             orderby d.date descending
                                             select new vw_hot_topics { id = d.id, title = d.title, description = d.description, date = d.date, encrypt_id = d.encrypt_id, count = a.Count() }).Take(5);

                    return View();
                }
                else
                {
                    return RedirectToAction("User", "Admin");
                }
            }
            catch (Exception error)
            {
                errLog.ErrorLog("User", error.Message, "Home");
                TempData["Error"] = "Page Not Found";
                return RedirectToAction("Error", "User");
            }
        }

        [Route("News")]
        public ActionResult News(int? page)
        {
            try
            {
                ViewBag.newspagead = db.tbl_ads.Where(m => m.location == "newspage").FirstOrDefault();

                int pageSize = 3;
                int pageNumber = (page ?? 1);

                var news = db.tbl_news.OrderByDescending(m => m.date_posted).ToList().ToPagedList(pageNumber, pageSize);

                return View(news);
            }
            catch (Exception error)
            {
                errLog.ErrorLog("User", error.Message, "News");
                TempData["Error"] = "Page Not Found";
                return RedirectToAction("Error", "User");
            }
        }

        [Route("News/{id}")]
        public ActionResult News(string id)
        {
            try
            {
                ViewBag.newsdetailad = db.tbl_ads.Where(m => m.location == "newsdetail").FirstOrDefault();

                var news = db.tbl_news.Where(m => m.encrypt_id == id).FirstOrDefault();

                return View("NewsDetail", news);
            }
            catch (Exception error)
            {
                errLog.ErrorLog("User", error.Message, "News Detail");
                TempData["Error"] = "Page Not Found";
                return RedirectToAction("Error", "User");
            }
        }

        [Route("Events")]
        public ActionResult Events(int? page)
        {
            try
            {
                ViewBag.eventspagead = db.tbl_ads.Where(m => m.location == "eventspage").FirstOrDefault();

                int pageSize = 3;
                int pageNumber = (page ?? 1);

                var events = db.tbl_events.OrderByDescending(m => m.date_posted).ToList().ToPagedList(pageNumber, pageSize);

                return View(events);
            }
            catch (Exception error)
            {
                errLog.ErrorLog("User", error.Message, "Events");
                TempData["Error"] = "Page Not Found";
                return RedirectToAction("Error", "User");
            }
        }

        [Route("Events/{id}")]
        public ActionResult Events(string id)
        {
            try
            {
                ViewBag.eventsdetailad = db.tbl_ads.Where(m => m.location == "eventsdetail").FirstOrDefault();

                var events = db.tbl_events.Where(m => m.encrypt_id == id).FirstOrDefault();
                var userid = db.tbl_user.Where(m => m.username == User.Identity.Name).Select(m => m.id).FirstOrDefault();

                ViewBag.event_time = events.time;
                ViewBag.event_date = events.date;

                var participant = db.tbl_event_participant.Where(m => m.userid == userid && m.eventid == events.id).FirstOrDefault();

                if (participant == null)
                {
                    ViewBag.participate = true;
                }

                return View("EventDetail", events);
            }
            catch (Exception error)
            {
                errLog.ErrorLog("User", error.Message, "Events Detail");
                TempData["Error"] = "Page Not Found";
                return RedirectToAction("Error", "User");
            }
        }

        [Route("Discussions")]
        public ActionResult Discussions()
        {
            try
            {
                ViewBag.discussionspagead = db.tbl_ads.Where(m => m.location == "discussionspage").FirstOrDefault();

                return View();
            }
            catch (Exception error)
            {
                errLog.ErrorLog("User", error.Message, "Discussions");
                TempData["Error"] = "Page Not Found";
                return RedirectToAction("Error", "User");
            }
        }

        [Route("Discussions/{id}")]
        public ActionResult Discussions(string id)
        {
            try
            {
                ViewBag.discussionsdetailad = db.tbl_ads.Where(m => m.location == "discussionsdetail").FirstOrDefault();

                var discus = db.tbl_discussion.Where(u => u.encrypt_id == id).FirstOrDefault();
                if (discus != null)
                {
                    ViewBag.photo = db.tbl_user.Where(m => m.id == discus.userid).Select(m => m.photo).FirstOrDefault();
                    ViewBag.author = db.tbl_user.Where(m => m.id == discus.userid).Select(m => m.username).FirstOrDefault();
                    return View("DiscussionsDetail", discus);
                }
                else
                {
                    TempData["Error"] = "Discussion Not Found";
                    return RedirectToAction("Error", "User");
                }
            }
            catch (Exception error)
            {
                errLog.ErrorLog("User", error.Message, "Discussions Detail");
                TempData["Error"] = "Page Not Found";
                return RedirectToAction("Error", "User");
                throw;
            }
        }


        [Route("Courses")]
        public ActionResult Courses()
        {
            try
            {
                ViewBag.coursepagead = db.tbl_ads.Where(m => m.location == "coursepage").FirstOrDefault();

                return View();
            }
            catch (Exception error)
            {
                errLog.ErrorLog("User", error.Message, "Courses");
                TempData["Error"] = "Page Not Found";
                return RedirectToAction("Error", "User");
            }
        }

        [Authorize]
        [Route("Courses/{course_encrypt_id}/{materi_encrypt_id}")]
        public ActionResult Courses(string course_encrypt_id, string materi_encrypt_id)
        {
            try
            {
                ViewBag.coursedetailad = db.tbl_ads.Where(m => m.location == "coursedetail").FirstOrDefault();

                var userid = db.tbl_user.Where(m => m.username == User.Identity.Name).Select(m => m.id).FirstOrDefault();
                var courseid = db.tbl_course.Where(m => m.encrypt_id == course_encrypt_id).Select(m => m.courseid).FirstOrDefault();
                var materiid = db.tbl_materi.Where(m => m.encrypt_id == materi_encrypt_id).Select(m => m.materiid).FirstOrDefault();
                var tbl_answer = db.tbl_answer.Where(m => m.userid == userid && m.courseid == courseid).FirstOrDefault();

                var userbadge = db.tbl_user.Where(m => m.username == User.Identity.Name).Select(m => m.badgeid).FirstOrDefault();
                var coursebadge = db.tbl_course.Where(m => m.encrypt_id == course_encrypt_id).Select(m => m.badgeid).FirstOrDefault();
                var coursequiz = db.tbl_quiz.Where(m => m.courseid == courseid).ToList();

                var course = db.tbl_course.Where(m => m.courseid == courseid).FirstOrDefault();

                ViewBag.userbadge = userbadge;
                ViewBag.coursebadge = coursebadge;
                ViewBag.quiz_status = course.quiz_status;
                ViewBag.quiz = coursequiz.Count;

                var query = db.tbl_materi.Join(db.tbl_course, d => d.courseid, u => u.courseid, (d, u) => new
                {
                    materi_encrypt = d.encrypt_id,
                    course_encrypt = u.encrypt_id,
                    d.courseid,
                    d.number,
                    d.title,
                    d.description
                }).Where(u => u.courseid == courseid).OrderBy(d => d.number).ToList();

                ViewBag.Materi = JsonConvert.SerializeObject(query, Formatting.Indented, new JsonSerializerSettings()
                {
                    ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                });

                ViewBag.count = query.Count;
                ViewBag.encrypt = course_encrypt_id;

                tbl_materi subject = db.tbl_materi.Where(m => m.courseid == courseid && m.materiid == materiid).FirstOrDefault();
                    
                if(subject == null)
                {
                    TempData["Error"] = "Bad Request";
                    return RedirectToAction("Error", "User");
                }

                return View("CoursesDetail", subject);
                
            }
            catch (Exception error)
            {
                errLog.ErrorLog("User", error.Message, "Course Detail");
                TempData["Error"] = "Bad Request";
                return RedirectToAction("Error", "User");
            }
            
        }

        [Authorize]
        [Route("Quiz/{course_encrypt_id}/{number}")]
        public ActionResult Quiz(string course_encrypt_id, int number)
        {
            try
            {
                ViewBag.quizpagead = db.tbl_ads.Where(m => m.location == "quizpage").FirstOrDefault();

                if (Session[number.ToString()] != null)
                {
                    var courseid = db.tbl_course.Where(m => m.encrypt_id == course_encrypt_id).Select(m => m.courseid).FirstOrDefault();

                    ViewBag.encrypt = course_encrypt_id;
                    ViewBag.Quiz = db.tbl_quiz.Where(m => m.courseid == courseid).Count();
                    tbl_quiz quiz = db.tbl_quiz.Where(m => m.courseid == courseid && m.number == number).FirstOrDefault();
                    return View(quiz);

                }
                else
                {
                    TempData["Error"] = "Invalid URL";
                    return RedirectToAction("Error", "User");
                }
            }
            catch (Exception error)
            {
                errLog.ErrorLog("User", error.Message, "Quiz");
                TempData["Error"] = "Bad Request";
                return RedirectToAction("Error", "User");
            }
        }


        [HttpPost]
        public int TotalPoints(string course_encrypt_id, int number, string answer)
        {
            using (var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    var userid = db.tbl_user.Where(m => m.username == User.Identity.Name).Select(m => m.id).FirstOrDefault();
                    var courseid = db.tbl_course.Where(m => m.encrypt_id == course_encrypt_id).Select(m => m.courseid).FirstOrDefault();
                    var tbl_answer = db.tbl_answer.Where(m => m.userid == userid && m.courseid == courseid).FirstOrDefault();

                    Session[number.ToString()] = null;
                    Session[(number + 1).ToString()] = 1;

                    if (number == 1)
                    {
                        tbl_answer.point = 0;
                        db.SaveChanges();
                    }

                    var correct_answer = db.tbl_quiz.Where(m => m.courseid == courseid && m.number == number).Select(m => m.answer).FirstOrDefault();

                    if (correct_answer == answer)
                    {
                        tbl_answer.point += 1;
                        db.SaveChanges();
                    }

                    else if (correct_answer != answer)
                    {
                        tbl_answer.point += 0;
                        db.SaveChanges();
                        
                    }

                    if (number == db.tbl_quiz.Where(m => m.courseid == courseid).Count())
                    {
                        var total = tbl_answer.point * 100 / db.tbl_quiz.Where(m => m.courseid == courseid).Count();

                        if (total > 70)
                        {
                            var tbl_user = db.tbl_user.Find(userid);
                            tbl_user.badgeid += 1;
                            db.Configuration.ValidateOnSaveEnabled = false;
                            db.SaveChanges();
                            transaction.Commit();

                        }
                        transaction.Commit();

                        return 0;
                    }

                    transaction.Commit();

                    return 1;
                }
                catch (Exception error)
                {
                    transaction.Rollback();
                    errLog.ErrorLog("User", error.Message, "Quiz Calculation");
                    TempData["Error"] = "Bad Request";
                    return 2;
                }
            }   
        }

        [Route("Result/{encrypt_id}")]
        public ActionResult Result(string encrypt_id)
        {
            using (var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    var courseid = db.tbl_course.Where(m => m.encrypt_id == encrypt_id).Select(m => m.courseid).FirstOrDefault();
                    var userid = db.tbl_user.Where(m => m.username == User.Identity.Name).Select(m => m.id).FirstOrDefault();
                    var point = db.tbl_answer.Where(m => m.userid == userid && m.courseid == courseid).Select(m => m.point).FirstOrDefault();

                    var title = db.tbl_course.Where(m => m.courseid == courseid).Select(m => m.name).FirstOrDefault();

                    var question = db.tbl_quiz.Where(m => m.courseid == courseid).Count();

                    var tbl_answer = db.tbl_answer.Where(m => m.userid == userid && m.courseid == courseid).FirstOrDefault();
                    var tbl_user = db.tbl_user.Find(userid);

                    if (tbl_answer.attempt == 1)
                    {
                        var total = point * 100 / question;

                        tbl_answer.grade = total;

                        tbl_course_history history = new tbl_course_history();
                        history.userid = tbl_answer.userid;
                        history.courseid = tbl_answer.courseid;
                        history.grade = tbl_answer.grade;
                        history.date = DateTime.Now;
                        db.tbl_course_history.Add(history);
                        db.SaveChanges();
                        transaction.Commit();
                        Session.Clear();


                        if (total < 70)
                        {
                            ViewBag.result = "Sorry, You did not pass the " + title + " course. You got " + tbl_answer.grade + " out of 100 score, You can try again in 7 days after this initial attempt.";
                        }

                        else
                        {
                            ViewBag.result = "Congrats! You passed the " + title + " course with score " + tbl_answer.grade + " out of 100";
                        }
                    }

                    else
                    {
                        if (tbl_answer.grade < 70)
                        {
                            ViewBag.result = "Sorry, You did not pass the " + title + " course. You got " + tbl_answer.grade + " out of 100 score, You can try again in 7 days after this initial attempt.";
                        }

                        else
                        {
                            ViewBag.result = "Congrats! You passed the " + title + " course with score " + tbl_answer.grade + " out of 100";
                        }
                    }

                    return View();
                }
                catch (Exception error)
                {
                    transaction.Rollback();
                    errLog.ErrorLog("User", error.Message, "Result");
                    TempData["Error"] = "Bad Request";
                    return RedirectToAction("Error", "User");
                }
            }
                
        }

        [HttpPost]
        public int QuizSession(int courseid)
        {
            using (var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    var course_badge = db.tbl_course.Where(m => m.courseid == courseid).Select(m => m.badgeid).FirstOrDefault();
                    var user_badge = db.tbl_user.Where(m => m.username == User.Identity.Name).Select(m => m.badgeid).FirstOrDefault();

                    if (user_badge >= course_badge)
                    {
                        var quiz = db.tbl_quiz.Where(m => m.courseid == courseid).Select(m => m.number).ToList();

                        foreach (var item in quiz)
                        {
                            Session[item.Value.ToString()] = null;
                        }

                        Session["1"] = 1;

                        var userid = db.tbl_user.Where(m => m.username == User.Identity.Name).Select(m => m.id).FirstOrDefault();
                        var tbl_answer = db.tbl_answer.Where(m => m.userid == userid && m.courseid == courseid).FirstOrDefault();


                        if (tbl_answer == null)
                        {
                            tbl_answer obj_answer = new tbl_answer();
                            obj_answer.userid = userid;
                            obj_answer.courseid = courseid;
                            obj_answer.point = 0;
                            obj_answer.grade = 0;
                            obj_answer.attempt = 1;
                            obj_answer.date = DateTime.Now;
                            db.tbl_answer.Add(obj_answer);

                            tbl_answer = obj_answer;
                        }
                        else
                        {
                            tbl_answer.attempt += 1;
                            tbl_answer.date = DateTime.Now;
                        }
                        db.SaveChanges();
                        transaction.Commit();

                        if (tbl_answer.attempt == 1)
                        {
                            return 0;
                        }
                        else
                        {
                            return 1;
                        }
                    }

                    else
                    {
                        return 2;
                    }
                }
                catch (Exception error)
                {
                    transaction.Rollback();
                    errLog.ErrorLog("User", error.Message, "Quiz Session");
                    TempData["Error"] = "Bad Request";
                    return 3;
                }
            }
        }
    }
}
