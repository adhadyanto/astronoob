﻿using Microsoft.Reporting.WebForms;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Text;
using System.Transactions;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using TestAstro.Models;

namespace TestAstro.Controllers
{
    public class UserController : Controller
    {
        private db_project db_Project = new db_project();

        [HttpPost]
        [Authorize]
        public int SaveNewsComment(int newsid, string comment_text)
        {
            using (var transaction = db_Project.Database.BeginTransaction())
            {
                try
                {
                    tbl_news_comment comment = new tbl_news_comment();
                    comment.userid = db_Project.tbl_user.Where(x => x.username == User.Identity.Name).Select(x => x.id).SingleOrDefault();
                    comment.newsid = newsid;
                    comment.comment = comment_text;
                    comment.date = DateTime.Now;
                    comment.encrypt_id = Guid.NewGuid().ToString();
                    db_Project.tbl_news_comment.Add(comment);
                    db_Project.SaveChanges();
                    transaction.Commit();
                    return 1;
                }
                catch (Exception err)
                {
                    transaction.Rollback();
                    errLog.ErrorLog(User.Identity.Name, err.Message, "Post Comment");
                    return 0;
                }
            }
        }

        public JsonResult ShowNewsComment(int newsid)
        {
            try
            {
                db_Project.Configuration.ProxyCreationEnabled = false;

                var query = db_Project.tbl_news_comment.Join(db_Project.tbl_user, d => d.userid, u => u.id, (d, u) => new
                {
                    d.id,
                    d.comment,
                    d.date,
                    d.newsid,
                    u.username,
                    u.photo
                }).Where(d => d.newsid == newsid).OrderByDescending(d => d.date).ToList();

                var list = JsonConvert.SerializeObject(query, Formatting.Indented, new JsonSerializerSettings
                {
                    ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                });

                var jsonResult = Json(list, JsonRequestBehavior.AllowGet);
                jsonResult.MaxJsonLength = int.MaxValue;

                return jsonResult;
            }
            catch (Exception err)
            {
                errLog.ErrorLog("User", err.Message, "Show Comment News");
                return null;
            }
        }

        [Authorize]
        [HttpPost]
        public void ReportCommentNews(int reportcommentid, string option)
        {
            using (var transaction = db_Project.Database.BeginTransaction())
            {
                try
                {
                    tbl_news_comment_report report = new tbl_news_comment_report();
                    report.reporter = db_Project.tbl_user.Where(m => m.username == User.Identity.Name).Select(m => m.id).FirstOrDefault();
                    report.commentid = reportcommentid;
                    report.detail = option;
                    report.date = DateTime.Now;
                    db_Project.tbl_news_comment_report.Add(report);
                    db_Project.SaveChanges();
                    transaction.Commit();
                }
                catch (Exception err)
                {
                    transaction.Rollback();
                    errLog.ErrorLog(User.Identity.Name, err.Message, "Report Comment News");
                }
            }

        }

        [Authorize]
        [HttpPost]
        public void DeleteCommentNews(int commentnewsid)
        {
            using (var transaction = db_Project.Database.BeginTransaction())
            {
                try
                {
                    tbl_news_comment comment = db_Project.tbl_news_comment.Find(commentnewsid);
                    List<tbl_news_comment_report> reports = db_Project.tbl_news_comment_report.Where(m => m.commentid == commentnewsid).ToList();
                    foreach (tbl_news_comment_report report in reports)
                    {
                        db_Project.tbl_news_comment_report.Remove(report);
                    }
                    db_Project.tbl_news_comment.Remove(comment);
                    db_Project.SaveChanges();
                    transaction.Commit();
                }
                catch (Exception err)
                {
                    transaction.Rollback();
                    errLog.ErrorLog(User.Identity.Name, err.Message, "Delete News Comment");
                }
            }

        }

        [Authorize]
        [HttpPost]
        public void EditCommentNews(int commentnewsid, string comment)
        {
            using (var transaction = db_Project.Database.BeginTransaction())
            {
                try
                {
                    tbl_news_comment edit_comment = db_Project.tbl_news_comment.Find(commentnewsid);
                    edit_comment.comment = comment;
                    db_Project.SaveChanges();
                    transaction.Commit();
                }
                catch (Exception err)
                {
                    transaction.Rollback();
                    errLog.ErrorLog(User.Identity.Name, err.Message, "Edit Comment News");
                }
            }

        }


        //[Route("DiscussionsDetail/{id}")]
        [HttpPost]
        [Authorize]
        public int SaveEventComment(int eventid, string comment_text)
        {
            using (var transaction = db_Project.Database.BeginTransaction())
            {
                try
                {
                    tbl_event_comment comment = new tbl_event_comment();
                    comment.userid = db_Project.tbl_user.Where(x => x.username == User.Identity.Name).Select(x => x.id).SingleOrDefault();
                    comment.eventid = eventid;
                    comment.comment = comment_text;
                    comment.date = DateTime.Now;
                    comment.encrypt_id = Guid.NewGuid().ToString();
                    db_Project.tbl_event_comment.Add(comment);
                    db_Project.SaveChanges();
                    transaction.Commit();
                    return 1;
                }
                catch (Exception err)
                {
                    transaction.Rollback();
                    errLog.ErrorLog(User.Identity.Name, err.Message, "Post Comment");
                    return 0;
                }
            }
        }

        public JsonResult ShowEventComment(int eventid)
        {
            try
            {
                db_Project.Configuration.ProxyCreationEnabled = false;

                var query = db_Project.tbl_event_comment.Join(db_Project.tbl_user, d => d.userid, u => u.id, (d, u) => new
                {
                    d.id,
                    d.comment,
                    d.date,
                    d.eventid,
                    u.username,
                    u.photo
                }).Where(d => d.eventid == eventid).OrderByDescending(d => d.date).ToList();

                var list = JsonConvert.SerializeObject(query, Formatting.Indented, new JsonSerializerSettings
                {
                    ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                });

                var jsonResult = Json(list, JsonRequestBehavior.AllowGet);
                jsonResult.MaxJsonLength = int.MaxValue;

                return jsonResult;

            }
            catch (Exception err)
            {
                errLog.ErrorLog("User", err.Message, "Show Comment Event");
                return null;
            }
        }

        [Authorize]
        [HttpPost]
        public void ReportCommentEvent(int reportcommentid, string option)
        {
            using (var transaction = db_Project.Database.BeginTransaction())
            {
                try
                {
                    tbl_event_comment_report report = new tbl_event_comment_report();
                    report.reporter = db_Project.tbl_user.Where(m => m.username == User.Identity.Name).Select(m => m.id).FirstOrDefault();
                    report.commentid = reportcommentid;
                    report.detail = option;
                    report.date = DateTime.Now;
                    db_Project.tbl_event_comment_report.Add(report);
                    db_Project.SaveChanges();
                    transaction.Commit();
                }
                catch (Exception err)
                {
                    transaction.Rollback();
                    errLog.ErrorLog(User.Identity.Name, err.Message, "Report Comment Event");
                }
            }

        }

        [Authorize]
        [HttpPost]
        public void DeleteCommentEvent(int commenteventid)
        {
            using (var transaction = db_Project.Database.BeginTransaction())
            {
                try
                {
                    tbl_event_comment comment = db_Project.tbl_event_comment.Find(commenteventid);
                    db_Project.tbl_event_comment.Remove(comment);
                    List<tbl_event_comment_report> reports = db_Project.tbl_event_comment_report.Where(m => m.commentid == commenteventid).ToList();
                    foreach (tbl_event_comment_report report in reports)
                    {
                        db_Project.tbl_event_comment_report.Remove(report);
                    }
                    db_Project.SaveChanges();
                    transaction.Commit();
                }
                catch (Exception err)
                {
                    transaction.Rollback();
                    errLog.ErrorLog(User.Identity.Name, err.Message, "Delete Comment Event");
                }
            }

        }

        [Authorize]
        [HttpPost]
        public void EditCommentEvent(int commenteventid, string comment)
        {
            using (var transaction = db_Project.Database.BeginTransaction())
            {
                try
                {
                    tbl_event_comment edit_comment = db_Project.tbl_event_comment.Find(commenteventid);
                    edit_comment.comment = comment;
                    db_Project.SaveChanges();
                    transaction.Commit();
                }
                catch (Exception err)
                {
                    transaction.Rollback();
                    errLog.ErrorLog(User.Identity.Name, err.Message, "Edit Comment Event");
                }
            }

        }

        [HttpPost]
        [Authorize]
        public int JoinEvent(int eventid)
        {
            using (var transaction = db_Project.Database.BeginTransaction())
            {
                try
                {
                    var userid = db_Project.tbl_user.Where(m => m.username == User.Identity.Name).Select(m => m.id).FirstOrDefault();
                    tbl_event_participant participant = new tbl_event_participant();
                    participant.eventid = eventid;
                    participant.userid = userid;
                    db_Project.tbl_event_participant.Add(participant);
                    db_Project.SaveChanges();
                    transaction.Commit();
                    return 1;
                }
                catch (Exception err)
                {
                    transaction.Rollback();
                    errLog.ErrorLog(User.Identity.Name, err.Message, "Join Event");
                    return 0;
                }
            }
        }

        [HttpPost]
        [Authorize]
        public int CancelJoinEvent(int eventid)
        {
            using (var transaction = db_Project.Database.BeginTransaction())
            {
                try
                {
                    var userid = db_Project.tbl_user.Where(m => m.username == User.Identity.Name).Select(m => m.id).FirstOrDefault();
                    tbl_event_participant participant = db_Project.tbl_event_participant.Where(m => m.eventid == eventid && m.userid == userid).FirstOrDefault();
                    db_Project.tbl_event_participant.Remove(participant);
                    db_Project.SaveChanges();
                    transaction.Commit();
                    return 1;
                }
                catch (Exception err)
                {
                    transaction.Rollback();
                    errLog.ErrorLog(User.Identity.Name, err.Message, "Join Event");
                    return 0;
                }
            }
        }


        [HttpPost]
        [Authorize]
        public int SaveComment(int postid, string comment_text)
        {
            using (var transaction = db_Project.Database.BeginTransaction())
            {
                try
                {
                    tbl_discussion_comment comment = new tbl_discussion_comment();
                    comment.userid = db_Project.tbl_user.Where(x => x.username == User.Identity.Name).Select(x => x.id).SingleOrDefault();
                    comment.discussionid = postid;
                    comment.comment = comment_text;
                    comment.date = DateTime.Now;
                    comment.encrypt_id = Guid.NewGuid().ToString();
                    db_Project.tbl_discussion_comment.Add(comment);
                    db_Project.SaveChanges();
                    transaction.Commit();
                    return 1;
                }
                catch (Exception err)
                {
                    transaction.Rollback();
                    errLog.ErrorLog(User.Identity.Name, err.Message, "Post Comment");
                    return 0;
                }
            }
        }

        public JsonResult ShowComment(int postid)
        {
            try
            {
                db_Project.Configuration.ProxyCreationEnabled = false;

                var query = db_Project.tbl_discussion_comment.Join(db_Project.tbl_user, d => d.userid, u => u.id, (d, u) => new
                {
                    d.id,
                    d.comment,
                    d.date,
                    d.discussionid,
                    u.username,
                    u.photo
                }).Where(d => d.discussionid == postid).OrderBy(d => d.date).ToList();

                var list = JsonConvert.SerializeObject(query, Formatting.Indented, new JsonSerializerSettings
                {
                    ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                });

                var jsonResult = Json(list, JsonRequestBehavior.AllowGet);
                jsonResult.MaxJsonLength = int.MaxValue;

                return jsonResult;

            }
            catch (Exception err)
            {
                errLog.ErrorLog("User", err.Message, "Show Comment Discussion");
                return null;
            }
        }

        [HttpPost]
        [Authorize]
        public int SavePost(string title, string post)
        { 
            using (var transaction = db_Project.Database.BeginTransaction())
            {
                try
                {
                    Guid encrypt_id = Guid.NewGuid();

                    tbl_discussion discussion = new tbl_discussion();
                    discussion.userid = db_Project.tbl_user.Where(x => x.username == User.Identity.Name).Select(x => x.id).SingleOrDefault();
                    discussion.encrypt_id = encrypt_id.ToString();
                    discussion.title = title;
                    discussion.description = post;
                    discussion.date = DateTime.Now;
                    db_Project.tbl_discussion.Add(discussion);
                    db_Project.SaveChanges();
                    transaction.Commit();
                    return 1;
                }
                catch (Exception err)
                {
                    transaction.Rollback();
                    errLog.ErrorLog(User.Identity.Name, err.Message, "Post Discussion");
                    return 0;
                }
            }
        }

        public JsonResult ShowPost()
        {
            try
            {
                db_Project.Configuration.ProxyCreationEnabled = false;

                var query = db_Project.tbl_discussion.Join(db_Project.tbl_user, d => d.userid, u => u.id, (d, u) => new
                {
                    d.encrypt_id,
                    d.id,
                    u.username,
                    u.photo,
                    d.title,
                    d.description,
                    d.date
                }).OrderByDescending(d => d.date).ToList();

                var list = JsonConvert.SerializeObject(query, Formatting.Indented, new JsonSerializerSettings
                {
                    ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                });

                var jsonResult = Json(list, JsonRequestBehavior.AllowGet);
                jsonResult.MaxJsonLength = int.MaxValue;

                return jsonResult;

            }
            catch (Exception err)
            {
                errLog.ErrorLog("User", err.Message, "Show Post Discussion");
                return null;
            }
        }

        [Authorize]
        [HttpPost]
        public void ReportDiscussion(int reportdiscusid, string option)
        {
            using (var transaction = db_Project.Database.BeginTransaction())
            {
                try
                {
                    tbl_discussion_report report = new tbl_discussion_report();
                    report.reporter = db_Project.tbl_user.Where(m => m.username == User.Identity.Name).Select(m => m.id).FirstOrDefault();
                    report.discussid = reportdiscusid;
                    report.detail = option;
                    report.date = DateTime.Now;
                    db_Project.tbl_discussion_report.Add(report);
                    db_Project.SaveChanges();
                    transaction.Commit();
                }
                catch (Exception err)
                {
                    transaction.Rollback();
                    errLog.ErrorLog(User.Identity.Name, err.Message, "Report Discussion");
                }
            }

        }

        [Authorize]
        [HttpPost]
        public void ReportCommentDiscussion(int reportcommentid, string option)
        {
            using (var transaction = db_Project.Database.BeginTransaction())
            {
                try
                {
                    tbl_discussion_comment_report report = new tbl_discussion_comment_report();
                    report.reporter = db_Project.tbl_user.Where(m => m.username == User.Identity.Name).Select(m => m.id).FirstOrDefault();
                    report.commentid = reportcommentid;
                    report.detail = option;
                    report.date = DateTime.Now;
                    db_Project.tbl_discussion_comment_report.Add(report);
                    db_Project.SaveChanges();
                    transaction.Commit();
                }
                catch (Exception err)
                {
                    transaction.Rollback();
                    errLog.ErrorLog(User.Identity.Name, err.Message, "Report Comment Discussion");
                }
            }

        }

        [Authorize]
        [HttpPost]
        public void DeleteDiscussion(int discussid)
        {
            using (var transaction = db_Project.Database.BeginTransaction())
            {
                try
                {

                    tbl_discussion discuss = db_Project.tbl_discussion.Find(discussid);
                    List<tbl_discussion_comment> comments = db_Project.tbl_discussion_comment.Where(m => m.discussionid == discussid).ToList();

                    foreach (tbl_discussion_comment comment in comments)
                    {
                        db_Project.tbl_discussion_comment.Remove(comment);
                    }
                    db_Project.tbl_discussion.Remove(discuss);
                    db_Project.SaveChanges();
                    transaction.Commit();
                }
                catch (Exception err)
                {
                    transaction.Rollback();
                    errLog.ErrorLog(User.Identity.Name, err.Message, "Delete Discussion");
                }
            }

        }

        [Authorize]
        [HttpPost]
        public void DeleteCommentDiscussion(int commentdiscussid)
        {
            using (var transaction = db_Project.Database.BeginTransaction())
            {
                try
                {
                    tbl_discussion_comment comment = db_Project.tbl_discussion_comment.Find(commentdiscussid);
                    db_Project.tbl_discussion_comment.Remove(comment);
                    db_Project.SaveChanges();
                    transaction.Commit();
                }
                catch (Exception err)
                {
                    transaction.Rollback();
                    errLog.ErrorLog(User.Identity.Name, err.Message, "Delete Comment Discussion");
                }
            }

        }


        [Authorize]
        [HttpPost]
        public void EditDiscussion(int editdiscusid, string title, string description)
        {
            using (var transaction = db_Project.Database.BeginTransaction())
            {
                try
                {
                    tbl_discussion discuss = db_Project.tbl_discussion.Find(editdiscusid);
                    discuss.title = title;
                    discuss.description = description;
                    db_Project.SaveChanges();
                    transaction.Commit();
                }
                catch (Exception err)
                {
                    transaction.Rollback();
                    errLog.ErrorLog(User.Identity.Name, err.Message, "Edit Discussion");
                }
            }

        }

        [Authorize]
        [HttpPost]
        public void EditCommentDiscussion(int commentdiscussid, string comment)
        {
            using (var transaction = db_Project.Database.BeginTransaction())
            {
                try
                {
                    tbl_discussion_comment edit_comment = db_Project.tbl_discussion_comment.Find(commentdiscussid);
                    edit_comment.comment = comment;
                    db_Project.SaveChanges();
                    transaction.Commit();
                }
                catch (Exception err)
                {
                    transaction.Rollback();
                    errLog.ErrorLog(User.Identity.Name, err.Message, "Edit Comment Discussion");
                }
            }

        }

        [Authorize]
        public JsonResult ShowCourses() 
        {
            try
            {
                db_Project.Configuration.ProxyCreationEnabled = false;

                var query = db_Project.tbl_course.Join(db_Project.tbl_materi, d => d.courseid, u => u.courseid, (d, u) => new
                {
                    course_encrypt = d.encrypt_id,
                    materi_encrypt = u.encrypt_id,
                    d.name,
                    d.description,
                    d.badgeid,
                    u.number
                }).Where(u => u.number == 1).OrderBy(d => d.badgeid).ToList();

                var list = JsonConvert.SerializeObject(query, Formatting.Indented, new JsonSerializerSettings
                {
                    ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                });

                var jsonResult = Json(list, JsonRequestBehavior.AllowGet);
                jsonResult.MaxJsonLength = int.MaxValue;

                return jsonResult;

            }
            catch (Exception err)
            {
                errLog.ErrorLog("User", err.Message, "Show Course");
                return null;
            }
        }

        [Authorize]
        public JsonResult ShowMateri(int id)
        {
            try
            {
                db_Project.Configuration.ProxyCreationEnabled = false;

                var query = db_Project.tbl_materi.Find(id);

                var list = JsonConvert.SerializeObject(query, Formatting.Indented, new JsonSerializerSettings
                {
                    ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                });

                var jsonResult = Json(list, JsonRequestBehavior.AllowGet);
                jsonResult.MaxJsonLength = int.MaxValue;

                return jsonResult;

            }
            catch (Exception err)
            {
                errLog.ErrorLog("User", err.Message, "Show Materi");
                return null;
            }
        }

        [Route("Setting")]
        [Authorize]
        public ActionResult AccountSetting()
        {
            return View(db_Project.vw_setting.Where(m => m.username == User.Identity.Name).FirstOrDefault());
        }

        [HttpPost]
        [Authorize]
        public ActionResult SaveProfilePicture(HttpPostedFileBase uploadPP)
        {
            if (uploadPP != null)
            {
                using (var transaction = db_Project.Database.BeginTransaction())
                {
                    try
                    {
                        string editedFileName = Guid.NewGuid().ToString() + "-" + uploadPP.FileName;
                        string filePath = "~/img/pp-img/" + editedFileName;
                        uploadPP.SaveAs(Server.MapPath(filePath));

                        tbl_user user = db_Project.tbl_user.Where(m => m.username == User.Identity.Name).FirstOrDefault();

                        var old_pp = "~/img/pp-img/" + user.photo;

                        user.photo = editedFileName;

                        db_Project.Configuration.ValidateOnSaveEnabled = false;
                        db_Project.SaveChanges();

                        if (old_pp != "~/img/pp-img/default.jpg")
                        {
                            System.IO.File.Delete(Server.MapPath(old_pp));
                        }
                        transaction.Commit();

                        TempData["success_pp"] = "success";

                    }
                    catch (Exception err)
                    {
                        transaction.Rollback();
                        errLog.ErrorLog(User.Identity.Name, err.Message, "Save Profile Picture");
                        TempData["error_pp"] = "Failed to change profile picture";
                        return RedirectToAction("AccountSetting");
                    }
                }
            }
            else
            {
                TempData["error_pp"] = "There's no file attached";
            }

            return RedirectToAction("AccountSetting");
        }

        [Authorize]
        public ActionResult SaveAccountProfile(vw_setting param_user)
        {
            using (var transaction = db_Project.Database.BeginTransaction())
            {
                try
                {
                    tbl_user user = db_Project.tbl_user.Where(m => m.username == User.Identity.Name).FirstOrDefault();
                    user.name = param_user.name;

                    db_Project.Configuration.ValidateOnSaveEnabled = false;
                    db_Project.SaveChanges();
                    transaction.Commit();

                    TempData["success_ap"] = "success";

                    return RedirectToAction("AccountSetting");


                }
                catch (Exception err)
                {
                    transaction.Rollback();
                    errLog.ErrorLog(User.Identity.Name, err.Message, "Save Account Profile");
                    TempData["Error"] = "Failed to change account profile";
                    return RedirectToAction("Error");
                }
            }
        }

        [Authorize]
        public ActionResult SaveAccountPassword(vw_setting param_user)
        {
            using (var transaction = db_Project.Database.BeginTransaction())
            {
                try
                {
                    var old_password = db_Project.tbl_user.Where(m => m.username == User.Identity.Name).Select(m => m.password).FirstOrDefault();

                    if (Encrypt(param_user.password) == old_password)
                    {
                        tbl_user user = db_Project.tbl_user.Where(m => m.username == User.Identity.Name).FirstOrDefault();
                        user.password = Encrypt(param_user.new_password);

                        db_Project.Configuration.ValidateOnSaveEnabled = false;
                        db_Project.SaveChanges();
                        transaction.Commit();

                        TempData["success_pw"] = "success";
                    }
                    else
                    {
                        TempData["invalid_password"] = "Invalid Password";
                    }

                    return RedirectToAction("AccountSetting");
                }
                catch (Exception err)
                {
                    transaction.Rollback();
                    errLog.ErrorLog(User.Identity.Name, err.Message, "Save Account Password");
                    TempData["Error"] = "Failed to change password";
                    return RedirectToAction("Error");
                }
            }
        }

        private string Encrypt(string clearText)
        {
            string EncryptionKey = "ABCDEFGHIJKLMNOPQRSTUVWXYZ123456789";
            byte[] clearBytes = Encoding.Unicode.GetBytes(clearText);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(clearBytes, 0, clearBytes.Length);
                        cs.Close();
                    }
                    clearText = Convert.ToBase64String(ms.ToArray());
                }
            }
            return clearText;
        }

        [Authorize]
        [Route("UpgradePremium")]
        public ActionResult UpgradePremium()
        {
            var userid = db_Project.tbl_user.Where(m => m.username == User.Identity.Name).FirstOrDefault();
            var payment = db_Project.tbl_premium_payment.Where(m => m.userid == userid.id && m.status == false).FirstOrDefault();



            if (payment != null)
            {
                var encrypt_id = db_Project.tbl_premium_pricing.Where(m => m.id == payment.premiumid).Select(m => m.encrypt_id).FirstOrDefault();
                var number = payment.number;

                return RedirectToAction("Checkout", new { encrypt_id = encrypt_id });
            }
            else
            {
                return View(db_Project.tbl_premium_pricing.ToList());
            }
            
        }

        [Authorize]
        [Route("UpgradePremium/Checkout/{encrypt_id}")]
        public ActionResult Checkout(string encrypt_id)
        {
            using (var transaction = db_Project.Database.BeginTransaction())
            {
                try
                {
                    var userid = db_Project.tbl_user.Where(m => m.username == User.Identity.Name).Select(m => m.id).FirstOrDefault();
                    tbl_premium_payment payment = db_Project.tbl_premium_payment.Where(m => m.userid == userid && m.status == false).FirstOrDefault();

                    if (payment != null)
                    {
                        return View(db_Project.tbl_premium_payment.Where(m => m.number == payment.number).FirstOrDefault());
                    }

                    else
                    {
                        bool IsValidEncryptId = db_Project.tbl_premium_pricing
                           .Any(u => u.encrypt_id == encrypt_id);

                        if (IsValidEncryptId)
                        {

                            var transaction_encrypt = Guid.NewGuid().ToString();
                            var premiumid = db_Project.tbl_premium_pricing.Where(m => m.encrypt_id == encrypt_id).Select(m => m.id).FirstOrDefault();
                            var days = db_Project.tbl_premium_pricing.Where(m => m.id == premiumid).Select(m => m.duration).FirstOrDefault();

                            tbl_premium_payment payments = new tbl_premium_payment();
                            payments.number = transaction_encrypt;
                            payments.userid = userid;
                            payments.status = false;
                            payments.datecreated = DateTime.Now;
                            payments.premiumid = premiumid;

                            ViewBag.days = days;

                            db_Project.tbl_premium_payment.Add(payments);
                            db_Project.SaveChanges();

                            transaction.Commit();

                            return View(db_Project.tbl_premium_payment.Where(m => m.number == transaction_encrypt).FirstOrDefault());

                        }
                        else
                        {
                            TempData["Error"] = "Failed to finish transaction";
                            return RedirectToAction("Error");
                        }
                    }
                }
                catch (Exception e)
                {
                    transaction.Rollback();
                    errLog.ErrorLog("User", e.Message, "Checkout");
                    TempData["Error"] = "Failed to finish transaction";
                    return View("Error");
                }
            }
        }

        [Authorize]
        [HttpPost]
        public int ConfirmPayment(string encrypt_id)
        {
            using (var transaction = db_Project.Database.BeginTransaction())
            {
                try
                {
                    tbl_premium_payment payment = db_Project.tbl_premium_payment.Where(m => m.number == encrypt_id).FirstOrDefault();

                    payment.datepaid = DateTime.Now;
                    payment.status = true;

                    tbl_role_mapping role = db_Project.tbl_role_mapping.Where(m => m.userid == payment.userid).FirstOrDefault();

                    role.roleid = 3;

                    var days = db_Project.tbl_premium_pricing.Where(m => m.id == payment.premiumid).Select(m => m.duration).FirstOrDefault();
                    tbl_premium_user premiumuser = db_Project.tbl_premium_user.Where(m => m.userid == payment.userid).FirstOrDefault();

                    if (premiumuser == null)
                    {

                        premiumuser = new tbl_premium_user();
                        premiumuser.userid = payment.userid;
                        premiumuser.days = days;

                        db_Project.tbl_premium_user.Add(premiumuser);

                        db_Project.SaveChanges();

                        transaction.Commit();
                    }
                    else
                    {
                        premiumuser.days += days;
                        db_Project.SaveChanges();

                        transaction.Commit();

                    }

                    var data = db_Project.tbl_user.Where(m => m.username == User.Identity.Name).FirstOrDefault();

                    var body = "Hello, " + data.name + "!";
                    body += "<p>Your premium purchase has been successful, you will receive additional " + days + " days of premium. Thank you for using our service, Enjoy your time!</p>";
                    this.sendEmail("Premium Payment", body, data.email);

                    return 1;
                }
                catch (Exception e)
                {
                    transaction.Rollback();
                    errLog.ErrorLog("User", e.Message, "Confirm Payment");
                    return 0;
                }
            }
        }

        [Authorize]
        [HttpPost]
        public int CancelPayment(string encrypt_id)
        {
            using (var transaction = db_Project.Database.BeginTransaction())
            {
                try
                {
                    tbl_premium_payment payment = db_Project.tbl_premium_payment.Where(m => m.number == encrypt_id).FirstOrDefault();

                    db_Project.tbl_premium_payment.Remove(payment);
                    db_Project.SaveChanges();

                    transaction.Commit();

                    return 1;
                }
                catch (Exception e)
                {
                    transaction.Rollback();
                    errLog.ErrorLog("User", e.Message, "Confirm Payment");
                    return 0;
                }
            }

        }

        [Route("Error")]
        public ActionResult Error()
        {
            return View();
        }

        [Authorize]
        [Route("User/Profile")]
        public ActionResult Profile()
        {
            try
            {
                var tbl_user = db_Project.tbl_user.Where(m => m.username == User.Identity.Name).FirstOrDefault();
                var badge_name = db_Project.tbl_badge.Where(m => m.badgeid == tbl_user.badgeid).Select(m => m.name).FirstOrDefault();
                var prem_days = db_Project.tbl_premium_user.Where(m => m.userid == tbl_user.id).Select(m => m.days).FirstOrDefault();

                ViewBag.badge_name = badge_name;
                ViewBag.prem_days = prem_days;
                return View(tbl_user);

            }
            catch (Exception err)
            {
                errLog.ErrorLog(User.Identity.Name, err.Message, "User Profile");
                TempData["Error"] = "Failed to show profile page :'(";
                return RedirectToAction("Error");
            }
        }

        [Authorize]
        public JsonResult UserCourseResult()
        {
            try
            {
                db_Project.Configuration.ProxyCreationEnabled = false;

                var userid = db_Project.tbl_user.Where(m => m.username == User.Identity.Name).Select(m => m.id).FirstOrDefault();

                var query = db_Project.tbl_course_history.Join(db_Project.tbl_course, d => d.courseid, u => u.courseid, (d, u) => new
                {
                    d.userid,
                    d.grade,
                    d.date,
                    u.name
                }).Where(d => d.userid == userid).OrderBy(d => d.date).ToList();

                var list = JsonConvert.SerializeObject(query, Formatting.Indented, new JsonSerializerSettings
                {
                    ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                });

                var jsonResult = Json(list, JsonRequestBehavior.AllowGet);
                jsonResult.MaxJsonLength = int.MaxValue;

                return jsonResult;

            }
            catch (Exception err)
            {
                errLog.ErrorLog(User.Identity.Name, err.Message, "User Course Result");
                return null;
            }
        }

        [Authorize]
        public JsonResult UserDiscPost()
        {
            try
            {
                db_Project.Configuration.ProxyCreationEnabled = false;

                db_Project.Configuration.ProxyCreationEnabled = false;

                var userid = db_Project.tbl_user.Where(m => m.username == User.Identity.Name).Select(m => m.id).FirstOrDefault();

                var query = db_Project.tbl_discussion.Where(d => d.userid == userid).OrderBy(d => d.date).ToList();

                var list = JsonConvert.SerializeObject(query, Formatting.Indented, new JsonSerializerSettings
                {
                    ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                });

                var jsonResult = Json(list, JsonRequestBehavior.AllowGet);
                jsonResult.MaxJsonLength = int.MaxValue;

                return jsonResult;

            }
            catch (Exception err)
            {
                errLog.ErrorLog(User.Identity.Name, err.Message, "User Discussion Post");
                return null;
            }
        }

        [Authorize]
        public JsonResult UserDiscComment()
        {
            try
            {
                db_Project.Configuration.ProxyCreationEnabled = false;

                var userid = db_Project.tbl_user.Where(m => m.username == User.Identity.Name).Select(m => m.id).FirstOrDefault();

                var query = db_Project.tbl_discussion_comment.Join(db_Project.tbl_discussion, d => d.discussionid, u => u.id, (d, u) => new
                {
                    d.comment,
                    d.userid,
                    d.date,
                    u.encrypt_id
                }).Where(d => d.userid == userid).OrderBy(d => d.date).ToList();

                var list = JsonConvert.SerializeObject(query, Formatting.Indented, new JsonSerializerSettings
                {
                    ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                });

                var jsonResult = Json(list, JsonRequestBehavior.AllowGet);
                jsonResult.MaxJsonLength = int.MaxValue;

                return jsonResult;

            }
            catch (Exception err)
            {
                errLog.ErrorLog(User.Identity.Name, err.Message, "User Discussion Comment");
                return null;
            }
        }

        [Authorize]
        [Route("User/Card")]
        public ActionResult UserCard()
        {
            string ssrsUrl = ConfigurationManager.AppSettings["SSRSReportsURL"].ToString();
            ReportViewer report = new ReportViewer();

            report.ProcessingMode = ProcessingMode.Remote;
            report.SizeToReportContent = true;
            report.Width = Unit.Percentage(100);
            report.Height = Unit.Percentage(100);
            report.ZoomMode = ZoomMode.FullPage;

            report.ServerReport.ReportServerUrl = new Uri(ssrsUrl);
            ReportParameter[] reportParameters = new ReportParameter[1];
            reportParameters[0] = new ReportParameter("username", User.Identity.Name);
            report.ServerReport.ReportPath = "/UserReport";
            report.ServerReport.SetParameters(reportParameters);
            report.ServerReport.Refresh();
            ViewBag.ReportViewer = report;
            return View();
        }

        [Authorize]
        [Route("User/CourseHistory")]
        public ActionResult CourseHistorySSRS()
        {
            string ssrsUrl = ConfigurationManager.AppSettings["SSRSReportsURL"].ToString();
            ReportViewer report = new ReportViewer();
            report.ProcessingMode = ProcessingMode.Remote;
            report.SizeToReportContent = true;
            report.Width = Unit.Percentage(100);
            report.Height = Unit.Percentage(100);
            report.ZoomMode = ZoomMode.FullPage;
            report.ServerReport.ReportServerUrl = new Uri(ssrsUrl);
            ReportParameter[] reportParameters = new ReportParameter[1];
            reportParameters[0] = new ReportParameter("username", User.Identity.Name);
            report.ServerReport.ReportPath = "/CourseHistory";
            report.ServerReport.SetParameters(reportParameters);
            report.ServerReport.Refresh();
            ViewBag.ReportViewer = report;
            return View();
        }

        public ActionResult Success()
        {
            return View();
        }

        private void sendEmail(string subject, string body, string receiver)
        {
            using (MailMessage mm = new MailMessage("hello.astronoob@gmail.com", receiver))
            {
                mm.Subject = subject;
                mm.Body = body;
                mm.IsBodyHtml = true;
                SmtpClient smtp = new SmtpClient();
                smtp.Host = "smtp.gmail.com";
                smtp.EnableSsl = true;
                NetworkCredential NetworkCred = new NetworkCredential("hello.astronoob@gmail.com", "astronoob03_");
                smtp.UseDefaultCredentials = true;
                smtp.Credentials = NetworkCred;
                smtp.Port = 587;
                smtp.Send(mm);
            }
        }

    }
}