﻿using System;
using System.Linq;
using System.Web.Mvc;
using System.Web.Security;
using TestAstro.Models;
using System.Security.Cryptography;
using System.IO;
using System.Text;
using System.Net.Mail;
using System.Net;

namespace TestAstro.Controllers
{
    public class LoginController : Controller
    {
        private db_project db_project = new db_project();

        [Route("Register")]
        public ActionResult Register()
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "Home");
            }
            else
            {
                return View();
            }
        }

        [Route("Register")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Register(tbl_user registerUser)
        {
            using (var transactionReg = db_project.Database.BeginTransaction())
            {
                try
                {
                    if (ModelState.IsValid)
                    {
                        db_project db = new db_project();

                        registerUser.active = false;
                        registerUser.badgeid = 1;
                        registerUser.photo = "default.jpg";
                        registerUser.password = Encrypt(registerUser.password.Trim());
                        registerUser.join_date = DateTime.Now;
                        tbl_role_mapping u_role = new tbl_role_mapping();
                        u_role.roleid = 2;
                        u_role.userid = registerUser.id;
                        db.tbl_role_mapping.Add(u_role);
                        db.tbl_user.Add(registerUser);
                        db.Configuration.ValidateOnSaveEnabled = false;
                        db.SaveChanges();
                        SendActivationEmail(registerUser);
                        //FormsAuthentication.SetAuthCookie(registerUser.username, false);
                        transactionReg.Commit();
                        return RedirectToAction("Confirmation");
                    }
                    return View();
                }

                catch (Exception error)
                {
                    transactionReg.Rollback();
                    errLog.ErrorLog("Visitor", error.Message, "Register");
                    ViewBag.error = error.Message;
                    return View();
                }
            }
            
        }

        [Route("Confirmation")]
        public ActionResult Confirmation()
        {
            return View();
        }


        [Route("Login")]
        public ActionResult Login()
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "Home");
            }
            else
            {
                return View();
            }
        }

        [Route("Login")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(vw_login user)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var password = Encrypt(user.password.Trim());

                    bool IsValidUser = db_project.tbl_user
                   .Any(u => u.username.ToLower() == user.username.ToLower() && password == u.password);

                    bool IsConfirmed = db_project.tbl_user
                    .Any(u => u.username.ToLower() == user.username.ToLower() && u.active == true);

                    bool IsRegistered = db_project.tbl_user
                    .Any(u => u.username.ToLower() == user.username.ToLower());

                    if (!IsRegistered)
                    {
                        ViewBag.error = "Username doesn't exist";
                        return View();
                    }

                    if (IsValidUser && IsConfirmed)
                    {
                        FormsAuthentication.SetAuthCookie(user.username, false);
                        return RedirectToAction("Index","Home");
                    }

                    else if (IsValidUser && !IsConfirmed)
                    {
                        ViewBag.confirm = "Please confirm your email first";
                    }

                    else
                    {
                        errLog.ErrorLog("Visitor", "Invalid Username or Password", "Login");
                        ViewBag.error = "Invalid Username or Password";
                        ModelState.AddModelError("", "invalid Username or Password");
                    }
                }

                errLog.ErrorLog("Visitor", "Invalid Model", "Login");
                //ViewBag.error = "Invalid Username or Password";
                return View();

            }

            catch (Exception error)
            {
                errLog.ErrorLog("Visitor", error.Message, "Login");
                ViewBag.error = error.Message;
                return View();
            }

        }

        [Route("Logout")]
        public ActionResult Logout()
        {
            if (User.Identity.IsAuthenticated)
            {
                FormsAuthentication.SignOut();
            }
            
            return RedirectToAction("Index", "Home");
        }

        private string Encrypt(string clearText)
        {
            try
            {
                string EncryptionKey = "ABCDEFGHIJKLMNOPQRSTUVWXYZ123456789";
                byte[] clearBytes = Encoding.Unicode.GetBytes(clearText);
                using (Aes encryptor = Aes.Create())
                {
                    Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                    encryptor.Key = pdb.GetBytes(32);
                    encryptor.IV = pdb.GetBytes(16);
                    using (MemoryStream ms = new MemoryStream())
                    {
                        using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                        {
                            cs.Write(clearBytes, 0, clearBytes.Length);
                            cs.Close();
                        }
                        clearText = Convert.ToBase64String(ms.ToArray());
                    }
                }
                return clearText;
            }
            catch (Exception e)
            {
                errLog.ErrorLog("Visitor", e.Message, "Encrypt");
                return "";
            }
        }

        [Route("Activation/{id}")]
        public ActionResult Activation()
        {
            using (var transaction = db_project.Database.BeginTransaction())
            {
                try
                {
                    ViewBag.Message = "Invalid Activation code.";
                    if (RouteData.Values["id"] != null)
                    {
                        Guid activationCode = new Guid(RouteData.Values["id"].ToString());
                        tbl_activation userActivation = db_project.tbl_activation.Where(p => p.activationcode == activationCode.ToString()).FirstOrDefault();
                        if (userActivation != null)
                        {
                            //var userid =
                            //tbl_user user = db_project.tbl_user.Where(i => i.id == userActivation.userid);
                            var code = userActivation.userid;

                            tbl_user user = db_project.tbl_user.Find(code);
                            user.active = true;
                            db_project.tbl_activation.Remove(userActivation);
                            db_project.Configuration.ValidateOnSaveEnabled = false;
                            db_project.SaveChanges();
                            transaction.Commit();
                            ViewBag.Message = "Your Account Has Been Successfully Activated";

                        }
                    }

                    return View();
                }

                catch (Exception error)
                {
                    transaction.Rollback();
                    errLog.ErrorLog("User", error.Message, "Activation");
                    ViewBag.error = error.Message;
                    return View();
                }
            }
        }

        private void SendActivationEmail(tbl_user user)
        {
            Guid activationCode = Guid.NewGuid();

                try
                {
                    db_project.tbl_activation.Add(new tbl_activation
                    {
                        userid = user.id,
                        activationcode = activationCode.ToString()
                    });
                    db_project.SaveChanges();

                    //transaction.Commit();
                }
                catch (Exception error)
                {
                    //transaction.Rollback();
                    errLog.ErrorLog("Visitor", error.Message, "Send Activation Email");
                    ViewBag.error = error.Message;
                }

            using (MailMessage mm = new MailMessage("hello.astronoob@gmail.com", user.email))
            {
                mm.Subject = "Account Activation";
                string body = "Hello " + user.username + ",";
                body += "<br /><br />Please click the following link to activate your account";
                body += "<br /><a href = '" + string.Format("{0}://{1}/Activation/{2}", Request.Url.Scheme, Request.Url.Authority, activationCode) + "'>Click here to activate your account.</a>";
                body += "<br /><br />Thanks";
                mm.Body = body;
                mm.IsBodyHtml = true;
                SmtpClient smtp = new SmtpClient();
                smtp.Host = "smtp.gmail.com";
                smtp.EnableSsl = true;
                NetworkCredential NetworkCred = new NetworkCredential("hello.astronoob@gmail.com", "astronoob03_");
                smtp.UseDefaultCredentials = true;
                smtp.Credentials = NetworkCred;
                smtp.Port = 587;
                smtp.Send(mm);
            }
        }

        [Route("ForgetPassword")]
        public ActionResult ForgetPassword()
        {
            return View();
        }

        [Route("ForgetPassword")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ForgetPassword(vw_forget_password user)
        {
            bool IsEmailExist = db_project.tbl_user.Any(u => u.email.ToLower() == user.email.ToLower());

            if (IsEmailExist)
            {
                tbl_user userid = (tbl_user)db_project.tbl_user.Where(x => x.email.ToLower() == user.email.ToLower()).FirstOrDefault();
                SendRecoveryEmail(userid);
            }
            else
            {
                ViewBag.EmailNotFound = true;
            }

            return View("ForgetPasswordSent");
        }

        private void SendRecoveryEmail(tbl_user user)
        {
            Guid recoveryCode = Guid.NewGuid();

            using (var transaction = db_project.Database.BeginTransaction())
            {
                try
                { 
                    db_project.tbl_recovery.Add(new tbl_recovery
                    {
                        userid = user.id,
                        recoverycode = recoveryCode.ToString()
                    });
                    db_project.SaveChanges();

                    transaction.Commit();
                }
                catch (Exception error)
                {

                    transaction.Rollback();
                    errLog.ErrorLog("Visitor", error.Message, "Send Recovery Email");
                    ViewBag.error = error.Message;
                }
            }
                
            using (MailMessage mm = new MailMessage("hello.astronoob@gmail.com", user.email))
            {
                mm.Subject = "Account Recovery";
                string body = "Hello " + user.username + ",";
                body += "<br /><br />Please click the following link to reset your account";
                body += "<br /><a href = '" + string.Format("{0}://{1}/Login/ResetPassword/{2}", Request.Url.Scheme, Request.Url.Authority, recoveryCode) + "'>Click here to reset your account.</a>";
                body += "<br /><br />Thanks";
                mm.Body = body;
                mm.IsBodyHtml = true;
                SmtpClient smtp = new SmtpClient();
                smtp.Host = "smtp.gmail.com";
                smtp.EnableSsl = true;
                NetworkCredential NetworkCred = new NetworkCredential("hello.astronoob@gmail.com", "astronoob03_");
                smtp.UseDefaultCredentials = true;
                smtp.Credentials = NetworkCred;
                smtp.Port = 587;
                smtp.Send(mm);
            }
        }

        //[Route("ResetPassword/{id}")]
        public ActionResult ResetPassword()
        {
            using (var transaction = db_project.Database.BeginTransaction())
            {
                try
                {
                    ViewBag.Message = false;
                    if (RouteData.Values["id"] != null)
                    {
                        Guid recoveryCode = new Guid(RouteData.Values["id"].ToString());
                        tbl_recovery userRecovery = db_project.tbl_recovery.Where(p => p.recoverycode == recoveryCode.ToString()).FirstOrDefault();
                        if (userRecovery != null)
                        {
                            //var userid =
                            tbl_user user = (tbl_user)db_project.tbl_user.Where(i => i.id == userRecovery.userid).FirstOrDefault();
                            var code = userRecovery.userid;
                            //ViewBag.userid = userRecovery.userid;

                            db_project.tbl_recovery.Remove(userRecovery);
                            db_project.SaveChanges();
                            transaction.Commit();
                            ViewBag.Message = true;
                            return View(user);
                        }
                    }
                    return View();
                }

                catch (Exception error)
                {
                    transaction.Rollback();
                    errLog.ErrorLog("User", error.Message, "Recovery");
                    ViewBag.error = error.Message;
                    return View();
                }
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ResetPassword(tbl_user param_user)
        {
            using (var transaction = db_project.Database.BeginTransaction())
            {

                try
                {
                    tbl_user user = db_project.tbl_user.Find(param_user.id);
                    user.password = Encrypt(param_user.password);
                    db_project.Configuration.ValidateOnSaveEnabled = false;
                    db_project.SaveChanges();

                    transaction.Commit();

                    TempData["Success"] = "Your password has been successfuly changed, you can now login using your new password.";

                    return RedirectToAction("Success", "User");
                }
                catch (Exception error)
                {

                    transaction.Rollback();
                    errLog.ErrorLog("Visitor", error.Message, "Reset Password");
                    TempData["Error"] = "Failed to reset password";
                    return RedirectToAction("Error", "User");
                }    
            }
        }


        public JsonResult IsUsernameExist(string Username, int? Id)
        {
            var validateName = db_project.tbl_user.FirstOrDefault
                                (x => x.username == Username && x.id != Id);
            if (validateName != null)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(true, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult IsEmailExist(string Email, int? Id)
        {
            var validateName = db_project.tbl_user.FirstOrDefault
                                (x => x.email == Email && x.id != Id);
            if (validateName != null)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(true, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult EmailDoesntExist(string Email, int? Id)
        {
            var validateName = db_project.tbl_user.FirstOrDefault
                                (x => x.email == Email && x.id != Id);
            if (validateName == null)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(true, JsonRequestBehavior.AllowGet);
            }
        }


    }


}
