
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 11/03/2020 19:29:14
-- Generated from EDMX file: E:\Level 2\! FINAL\astronoob-master\Astronoob\Models\db_project.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [db_project];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK__tbl_cours__cours__17036CC0]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[tbl_course_history] DROP CONSTRAINT [FK__tbl_cours__cours__17036CC0];
GO
IF OBJECT_ID(N'[dbo].[FK__tbl_cours__useri__160F4887]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[tbl_course_history] DROP CONSTRAINT [FK__tbl_cours__useri__160F4887];
GO
IF OBJECT_ID(N'[dbo].[FK__tbl_discu__comme__07C12930]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[tbl_discussion_comment_report] DROP CONSTRAINT [FK__tbl_discu__comme__07C12930];
GO
IF OBJECT_ID(N'[dbo].[FK__tbl_discu__discu__03F0984C]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[tbl_discussion_report] DROP CONSTRAINT [FK__tbl_discu__discu__03F0984C];
GO
IF OBJECT_ID(N'[dbo].[FK__tbl_discu__discu__38996AB5]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[tbl_discussion_comment] DROP CONSTRAINT [FK__tbl_discu__discu__38996AB5];
GO
IF OBJECT_ID(N'[dbo].[FK__tbl_discu__repor__02FC7413]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[tbl_discussion_report] DROP CONSTRAINT [FK__tbl_discu__repor__02FC7413];
GO
IF OBJECT_ID(N'[dbo].[FK__tbl_discu__repor__06CD04F7]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[tbl_discussion_comment_report] DROP CONSTRAINT [FK__tbl_discu__repor__06CD04F7];
GO
IF OBJECT_ID(N'[dbo].[FK__tbl_discu__useri__37A5467C]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[tbl_discussion] DROP CONSTRAINT [FK__tbl_discu__useri__37A5467C];
GO
IF OBJECT_ID(N'[dbo].[FK__tbl_discu__useri__398D8EEE]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[tbl_discussion_comment] DROP CONSTRAINT [FK__tbl_discu__useri__398D8EEE];
GO
IF OBJECT_ID(N'[dbo].[FK__tbl_event__comme__0F624AF8]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[tbl_event_comment_report] DROP CONSTRAINT [FK__tbl_event__comme__0F624AF8];
GO
IF OBJECT_ID(N'[dbo].[FK__tbl_event__event__778AC167]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[tbl_event_comment] DROP CONSTRAINT [FK__tbl_event__event__778AC167];
GO
IF OBJECT_ID(N'[dbo].[FK__tbl_event__event__7A672E12]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[tbl_event_participant] DROP CONSTRAINT [FK__tbl_event__event__7A672E12];
GO
IF OBJECT_ID(N'[dbo].[FK__tbl_event__repor__0E6E26BF]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[tbl_event_comment_report] DROP CONSTRAINT [FK__tbl_event__repor__0E6E26BF];
GO
IF OBJECT_ID(N'[dbo].[FK__tbl_event__useri__76969D2E]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[tbl_event_comment] DROP CONSTRAINT [FK__tbl_event__useri__76969D2E];
GO
IF OBJECT_ID(N'[dbo].[FK__tbl_event__useri__7B5B524B]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[tbl_event_participant] DROP CONSTRAINT [FK__tbl_event__useri__7B5B524B];
GO
IF OBJECT_ID(N'[dbo].[FK__tbl_news___comme__0B91BA14]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[tbl_news_comment_report] DROP CONSTRAINT [FK__tbl_news___comme__0B91BA14];
GO
IF OBJECT_ID(N'[dbo].[FK__tbl_news___newsi__7F2BE32F]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[tbl_news_comment] DROP CONSTRAINT [FK__tbl_news___newsi__7F2BE32F];
GO
IF OBJECT_ID(N'[dbo].[FK__tbl_news___repor__0A9D95DB]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[tbl_news_comment_report] DROP CONSTRAINT [FK__tbl_news___repor__0A9D95DB];
GO
IF OBJECT_ID(N'[dbo].[FK__tbl_news___useri__7E37BEF6]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[tbl_news_comment] DROP CONSTRAINT [FK__tbl_news___useri__7E37BEF6];
GO
IF OBJECT_ID(N'[dbo].[fk_activation]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[tbl_activation] DROP CONSTRAINT [fk_activation];
GO
IF OBJECT_ID(N'[dbo].[fk_course_badge]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[tbl_course] DROP CONSTRAINT [fk_course_badge];
GO
IF OBJECT_ID(N'[dbo].[fk_courseid]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[tbl_materi] DROP CONSTRAINT [fk_courseid];
GO
IF OBJECT_ID(N'[dbo].[fk_courseid_answer]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[tbl_answer] DROP CONSTRAINT [fk_courseid_answer];
GO
IF OBJECT_ID(N'[dbo].[fk_courseid_on_quiz]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[tbl_quiz] DROP CONSTRAINT [fk_courseid_on_quiz];
GO
IF OBJECT_ID(N'[dbo].[fk_premium_user]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[tbl_premium_user] DROP CONSTRAINT [fk_premium_user];
GO
IF OBJECT_ID(N'[dbo].[fk_premiumid_payment]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[tbl_premium_payment] DROP CONSTRAINT [fk_premiumid_payment];
GO
IF OBJECT_ID(N'[dbo].[fk_recov_userid]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[tbl_recovery] DROP CONSTRAINT [fk_recov_userid];
GO
IF OBJECT_ID(N'[dbo].[fk_roleid]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[tbl_role_mapping] DROP CONSTRAINT [fk_roleid];
GO
IF OBJECT_ID(N'[dbo].[fk_user_badge]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[tbl_user] DROP CONSTRAINT [fk_user_badge];
GO
IF OBJECT_ID(N'[dbo].[fk_userid]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[tbl_role_mapping] DROP CONSTRAINT [fk_userid];
GO
IF OBJECT_ID(N'[dbo].[fk_userid_answer]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[tbl_answer] DROP CONSTRAINT [fk_userid_answer];
GO
IF OBJECT_ID(N'[dbo].[fk_userid_payment]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[tbl_premium_payment] DROP CONSTRAINT [fk_userid_payment];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[tbl_activation]', 'U') IS NOT NULL
    DROP TABLE [dbo].[tbl_activation];
GO
IF OBJECT_ID(N'[dbo].[tbl_ads]', 'U') IS NOT NULL
    DROP TABLE [dbo].[tbl_ads];
GO
IF OBJECT_ID(N'[dbo].[tbl_answer]', 'U') IS NOT NULL
    DROP TABLE [dbo].[tbl_answer];
GO
IF OBJECT_ID(N'[dbo].[tbl_badge]', 'U') IS NOT NULL
    DROP TABLE [dbo].[tbl_badge];
GO
IF OBJECT_ID(N'[dbo].[tbl_course]', 'U') IS NOT NULL
    DROP TABLE [dbo].[tbl_course];
GO
IF OBJECT_ID(N'[dbo].[tbl_course_history]', 'U') IS NOT NULL
    DROP TABLE [dbo].[tbl_course_history];
GO
IF OBJECT_ID(N'[dbo].[tbl_discussion]', 'U') IS NOT NULL
    DROP TABLE [dbo].[tbl_discussion];
GO
IF OBJECT_ID(N'[dbo].[tbl_discussion_comment]', 'U') IS NOT NULL
    DROP TABLE [dbo].[tbl_discussion_comment];
GO
IF OBJECT_ID(N'[dbo].[tbl_discussion_comment_report]', 'U') IS NOT NULL
    DROP TABLE [dbo].[tbl_discussion_comment_report];
GO
IF OBJECT_ID(N'[dbo].[tbl_discussion_report]', 'U') IS NOT NULL
    DROP TABLE [dbo].[tbl_discussion_report];
GO
IF OBJECT_ID(N'[dbo].[tbl_error_log_history]', 'U') IS NOT NULL
    DROP TABLE [dbo].[tbl_error_log_history];
GO
IF OBJECT_ID(N'[dbo].[tbl_event_comment]', 'U') IS NOT NULL
    DROP TABLE [dbo].[tbl_event_comment];
GO
IF OBJECT_ID(N'[dbo].[tbl_event_comment_report]', 'U') IS NOT NULL
    DROP TABLE [dbo].[tbl_event_comment_report];
GO
IF OBJECT_ID(N'[dbo].[tbl_event_participant]', 'U') IS NOT NULL
    DROP TABLE [dbo].[tbl_event_participant];
GO
IF OBJECT_ID(N'[dbo].[tbl_events]', 'U') IS NOT NULL
    DROP TABLE [dbo].[tbl_events];
GO
IF OBJECT_ID(N'[dbo].[tbl_materi]', 'U') IS NOT NULL
    DROP TABLE [dbo].[tbl_materi];
GO
IF OBJECT_ID(N'[dbo].[tbl_news]', 'U') IS NOT NULL
    DROP TABLE [dbo].[tbl_news];
GO
IF OBJECT_ID(N'[dbo].[tbl_news_comment]', 'U') IS NOT NULL
    DROP TABLE [dbo].[tbl_news_comment];
GO
IF OBJECT_ID(N'[dbo].[tbl_news_comment_report]', 'U') IS NOT NULL
    DROP TABLE [dbo].[tbl_news_comment_report];
GO
IF OBJECT_ID(N'[dbo].[tbl_premium_payment]', 'U') IS NOT NULL
    DROP TABLE [dbo].[tbl_premium_payment];
GO
IF OBJECT_ID(N'[dbo].[tbl_premium_pricing]', 'U') IS NOT NULL
    DROP TABLE [dbo].[tbl_premium_pricing];
GO
IF OBJECT_ID(N'[dbo].[tbl_premium_user]', 'U') IS NOT NULL
    DROP TABLE [dbo].[tbl_premium_user];
GO
IF OBJECT_ID(N'[dbo].[tbl_quiz]', 'U') IS NOT NULL
    DROP TABLE [dbo].[tbl_quiz];
GO
IF OBJECT_ID(N'[dbo].[tbl_recovery]', 'U') IS NOT NULL
    DROP TABLE [dbo].[tbl_recovery];
GO
IF OBJECT_ID(N'[dbo].[tbl_role_mapping]', 'U') IS NOT NULL
    DROP TABLE [dbo].[tbl_role_mapping];
GO
IF OBJECT_ID(N'[dbo].[tbl_roles]', 'U') IS NOT NULL
    DROP TABLE [dbo].[tbl_roles];
GO
IF OBJECT_ID(N'[dbo].[tbl_user]', 'U') IS NOT NULL
    DROP TABLE [dbo].[tbl_user];
GO
IF OBJECT_ID(N'[db_projectModelStoreContainer].[vw_forget_password]', 'U') IS NOT NULL
    DROP TABLE [db_projectModelStoreContainer].[vw_forget_password];
GO
IF OBJECT_ID(N'[db_projectModelStoreContainer].[vw_login]', 'U') IS NOT NULL
    DROP TABLE [db_projectModelStoreContainer].[vw_login];
GO
IF OBJECT_ID(N'[db_projectModelStoreContainer].[vw_setting]', 'U') IS NOT NULL
    DROP TABLE [db_projectModelStoreContainer].[vw_setting];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'tbl_activation'
CREATE TABLE [dbo].[tbl_activation] (
    [id] int IDENTITY(1,1) NOT NULL,
    [userid] int  NULL,
    [activationcode] varchar(255)  NULL
);
GO

-- Creating table 'tbl_discussion'
CREATE TABLE [dbo].[tbl_discussion] (
    [id] int IDENTITY(1,1) NOT NULL,
    [userid] int  NULL,
    [title] varchar(255)  NULL,
    [description] varchar(max)  NULL,
    [date] datetime  NULL,
    [encrypt_id] varchar(200)  NULL
);
GO

-- Creating table 'tbl_discussion_comment'
CREATE TABLE [dbo].[tbl_discussion_comment] (
    [id] int IDENTITY(1,1) NOT NULL,
    [userid] int  NULL,
    [comment] varchar(max)  NULL,
    [date] datetime  NULL,
    [discussionid] int  NULL,
    [encrypt_id] varchar(200)  NULL
);
GO

-- Creating table 'tbl_error_log_history'
CREATE TABLE [dbo].[tbl_error_log_history] (
    [id] int IDENTITY(1,1) NOT NULL,
    [username] varchar(50)  NULL,
    [err_msg] varchar(max)  NULL,
    [err_loc] varchar(50)  NULL,
    [err_date] datetime  NULL
);
GO

-- Creating table 'tbl_recovery'
CREATE TABLE [dbo].[tbl_recovery] (
    [id] int IDENTITY(1,1) NOT NULL,
    [userid] int  NULL,
    [recoverycode] varchar(255)  NULL
);
GO

-- Creating table 'tbl_role_mapping'
CREATE TABLE [dbo].[tbl_role_mapping] (
    [id] int IDENTITY(1,1) NOT NULL,
    [userid] int  NULL,
    [roleid] int  NULL
);
GO

-- Creating table 'tbl_roles'
CREATE TABLE [dbo].[tbl_roles] (
    [id] int IDENTITY(1,1) NOT NULL,
    [rolename] varchar(50)  NULL
);
GO

-- Creating table 'tbl_badge'
CREATE TABLE [dbo].[tbl_badge] (
    [badgeid] int IDENTITY(1,1) NOT NULL,
    [name] varchar(100)  NULL
);
GO

-- Creating table 'tbl_course'
CREATE TABLE [dbo].[tbl_course] (
    [courseid] int IDENTITY(1,1) NOT NULL,
    [name] varchar(100)  NULL,
    [description] varchar(max)  NULL,
    [badgeid] int  NULL,
    [encrypt_id] varchar(200)  NULL,
    [quiz_status] bit  NULL
);
GO

-- Creating table 'tbl_materi'
CREATE TABLE [dbo].[tbl_materi] (
    [materiid] int IDENTITY(1,1) NOT NULL,
    [courseid] int  NULL,
    [title] varchar(100)  NULL,
    [date] datetime  NULL,
    [description] varchar(max)  NULL,
    [number] int  NULL,
    [encrypt_id] varchar(200)  NULL,
    [video] varchar(max)  NULL,
    [video_source] varchar(max)  NULL,
    [subject_source] varchar(max)  NULL
);
GO

-- Creating table 'tbl_quiz'
CREATE TABLE [dbo].[tbl_quiz] (
    [quizid] int IDENTITY(1,1) NOT NULL,
    [courseid] int  NULL,
    [question] varchar(max)  NULL,
    [opt_a] varchar(max)  NULL,
    [opt_b] varchar(max)  NULL,
    [opt_c] varchar(max)  NULL,
    [opt_d] varchar(max)  NULL,
    [answer] char(1)  NULL,
    [number] int  NULL,
    [encrypt_id] varchar(200)  NULL
);
GO

-- Creating table 'tbl_answer'
CREATE TABLE [dbo].[tbl_answer] (
    [id] int IDENTITY(1,1) NOT NULL,
    [userid] int  NULL,
    [courseid] int  NULL,
    [point] int  NULL,
    [grade] int  NULL,
    [attempt] int  NULL,
    [date] datetime  NULL
);
GO

-- Creating table 'tbl_premium_pricing'
CREATE TABLE [dbo].[tbl_premium_pricing] (
    [id] int IDENTITY(1,1) NOT NULL,
    [packagetype] int  NULL,
    [name] varchar(255)  NULL,
    [price] int  NULL,
    [encrypt_id] varchar(255)  NULL,
    [duration] int  NULL
);
GO

-- Creating table 'tbl_premium_payment'
CREATE TABLE [dbo].[tbl_premium_payment] (
    [id] int IDENTITY(1,1) NOT NULL,
    [number] varchar(255)  NULL,
    [userid] int  NULL,
    [status] bit  NULL,
    [datecreated] datetime  NULL,
    [datepaid] datetime  NULL,
    [premiumid] int  NULL
);
GO

-- Creating table 'tbl_premium_user'
CREATE TABLE [dbo].[tbl_premium_user] (
    [id] int IDENTITY(1,1) NOT NULL,
    [userid] int  NULL,
    [days] int  NULL
);
GO

-- Creating table 'tbl_events'
CREATE TABLE [dbo].[tbl_events] (
    [id] int IDENTITY(1,1) NOT NULL,
    [title] varchar(255)  NULL,
    [photo] varchar(max)  NULL,
    [description] varchar(max)  NULL,
    [date_posted] datetime  NULL,
    [date] datetime  NULL,
    [time] time  NULL,
    [location] varchar(255)  NULL,
    [encrypt_id] varchar(200)  NULL,
    [contact_person] varchar(255)  NULL
);
GO

-- Creating table 'tbl_news'
CREATE TABLE [dbo].[tbl_news] (
    [id] int IDENTITY(1,1) NOT NULL,
    [title] varchar(255)  NULL,
    [photo] varchar(max)  NULL,
    [description] varchar(max)  NULL,
    [date_posted] datetime  NULL,
    [encrypt_id] varchar(200)  NULL,
    [source] varchar(max)  NULL
);
GO

-- Creating table 'tbl_event_comment'
CREATE TABLE [dbo].[tbl_event_comment] (
    [id] int IDENTITY(1,1) NOT NULL,
    [userid] int  NULL,
    [comment] varchar(max)  NULL,
    [date] datetime  NULL,
    [eventid] int  NULL,
    [encrypt_id] varchar(200)  NULL
);
GO

-- Creating table 'tbl_event_participant'
CREATE TABLE [dbo].[tbl_event_participant] (
    [id] int IDENTITY(1,1) NOT NULL,
    [eventid] int  NULL,
    [userid] int  NULL
);
GO

-- Creating table 'tbl_news_comment'
CREATE TABLE [dbo].[tbl_news_comment] (
    [id] int IDENTITY(1,1) NOT NULL,
    [userid] int  NULL,
    [comment] varchar(max)  NULL,
    [date] datetime  NULL,
    [newsid] int  NULL,
    [encrypt_id] varchar(200)  NULL
);
GO

-- Creating table 'tbl_discussion_report'
CREATE TABLE [dbo].[tbl_discussion_report] (
    [id] int IDENTITY(1,1) NOT NULL,
    [reporter] int  NULL,
    [discussid] int  NULL,
    [detail] varchar(max)  NULL,
    [date] datetime  NULL
);
GO

-- Creating table 'tbl_discussion_comment_report'
CREATE TABLE [dbo].[tbl_discussion_comment_report] (
    [id] int IDENTITY(1,1) NOT NULL,
    [reporter] int  NULL,
    [commentid] int  NULL,
    [detail] varchar(max)  NULL,
    [date] datetime  NULL
);
GO

-- Creating table 'tbl_news_comment_report'
CREATE TABLE [dbo].[tbl_news_comment_report] (
    [id] int IDENTITY(1,1) NOT NULL,
    [reporter] int  NULL,
    [commentid] int  NULL,
    [detail] varchar(max)  NULL,
    [date] datetime  NULL
);
GO

-- Creating table 'tbl_event_comment_report'
CREATE TABLE [dbo].[tbl_event_comment_report] (
    [id] int IDENTITY(1,1) NOT NULL,
    [reporter] int  NULL,
    [commentid] int  NULL,
    [detail] varchar(max)  NULL,
    [date] datetime  NULL
);
GO

-- Creating table 'vw_forget_password'
CREATE TABLE [dbo].[vw_forget_password] (
    [id] int IDENTITY(1,1) NOT NULL,
    [email] varchar(100)  NULL
);
GO

-- Creating table 'vw_login'
CREATE TABLE [dbo].[vw_login] (
    [username] varchar(50)  NOT NULL,
    [password] varchar(200)  NOT NULL
);
GO

-- Creating table 'vw_setting'
CREATE TABLE [dbo].[vw_setting] (
    [username] varchar(50)  NOT NULL,
    [name] varchar(50)  NULL,
    [password] varchar(200)  NOT NULL,
    [email] varchar(100)  NULL,
    [photo] varchar(max)  NULL
);
GO

-- Creating table 'tbl_ads'
CREATE TABLE [dbo].[tbl_ads] (
    [id] int IDENTITY(1,1) NOT NULL,
    [location] varchar(255)  NULL,
    [picture] varchar(max)  NULL,
    [link] varchar(max)  NULL,
    [customer] varchar(255)  NULL,
    [encrypt_id] varchar(max)  NULL,
    [duration] int  NULL
);
GO

-- Creating table 'tbl_course_history'
CREATE TABLE [dbo].[tbl_course_history] (
    [id] int IDENTITY(1,1) NOT NULL,
    [userid] int  NULL,
    [courseid] int  NULL,
    [grade] int  NULL,
    [date] datetime  NULL
);
GO

-- Creating table 'tbl_user'
CREATE TABLE [dbo].[tbl_user] (
    [id] int IDENTITY(1,1) NOT NULL,
    [username] varchar(50)  NOT NULL,
    [password] varchar(200)  NOT NULL,
    [badgeid] int  NULL,
    [name] varchar(50)  NULL,
    [email] varchar(100)  NULL,
    [photo] varchar(max)  NULL,
    [active] bit  NULL,
    [join_date] datetime  NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [id] in table 'tbl_activation'
ALTER TABLE [dbo].[tbl_activation]
ADD CONSTRAINT [PK_tbl_activation]
    PRIMARY KEY CLUSTERED ([id] ASC);
GO

-- Creating primary key on [id] in table 'tbl_discussion'
ALTER TABLE [dbo].[tbl_discussion]
ADD CONSTRAINT [PK_tbl_discussion]
    PRIMARY KEY CLUSTERED ([id] ASC);
GO

-- Creating primary key on [id] in table 'tbl_discussion_comment'
ALTER TABLE [dbo].[tbl_discussion_comment]
ADD CONSTRAINT [PK_tbl_discussion_comment]
    PRIMARY KEY CLUSTERED ([id] ASC);
GO

-- Creating primary key on [id] in table 'tbl_error_log_history'
ALTER TABLE [dbo].[tbl_error_log_history]
ADD CONSTRAINT [PK_tbl_error_log_history]
    PRIMARY KEY CLUSTERED ([id] ASC);
GO

-- Creating primary key on [id] in table 'tbl_recovery'
ALTER TABLE [dbo].[tbl_recovery]
ADD CONSTRAINT [PK_tbl_recovery]
    PRIMARY KEY CLUSTERED ([id] ASC);
GO

-- Creating primary key on [id] in table 'tbl_role_mapping'
ALTER TABLE [dbo].[tbl_role_mapping]
ADD CONSTRAINT [PK_tbl_role_mapping]
    PRIMARY KEY CLUSTERED ([id] ASC);
GO

-- Creating primary key on [id] in table 'tbl_roles'
ALTER TABLE [dbo].[tbl_roles]
ADD CONSTRAINT [PK_tbl_roles]
    PRIMARY KEY CLUSTERED ([id] ASC);
GO

-- Creating primary key on [badgeid] in table 'tbl_badge'
ALTER TABLE [dbo].[tbl_badge]
ADD CONSTRAINT [PK_tbl_badge]
    PRIMARY KEY CLUSTERED ([badgeid] ASC);
GO

-- Creating primary key on [courseid] in table 'tbl_course'
ALTER TABLE [dbo].[tbl_course]
ADD CONSTRAINT [PK_tbl_course]
    PRIMARY KEY CLUSTERED ([courseid] ASC);
GO

-- Creating primary key on [materiid] in table 'tbl_materi'
ALTER TABLE [dbo].[tbl_materi]
ADD CONSTRAINT [PK_tbl_materi]
    PRIMARY KEY CLUSTERED ([materiid] ASC);
GO

-- Creating primary key on [quizid] in table 'tbl_quiz'
ALTER TABLE [dbo].[tbl_quiz]
ADD CONSTRAINT [PK_tbl_quiz]
    PRIMARY KEY CLUSTERED ([quizid] ASC);
GO

-- Creating primary key on [id] in table 'tbl_answer'
ALTER TABLE [dbo].[tbl_answer]
ADD CONSTRAINT [PK_tbl_answer]
    PRIMARY KEY CLUSTERED ([id] ASC);
GO

-- Creating primary key on [id] in table 'tbl_premium_pricing'
ALTER TABLE [dbo].[tbl_premium_pricing]
ADD CONSTRAINT [PK_tbl_premium_pricing]
    PRIMARY KEY CLUSTERED ([id] ASC);
GO

-- Creating primary key on [id] in table 'tbl_premium_payment'
ALTER TABLE [dbo].[tbl_premium_payment]
ADD CONSTRAINT [PK_tbl_premium_payment]
    PRIMARY KEY CLUSTERED ([id] ASC);
GO

-- Creating primary key on [id] in table 'tbl_premium_user'
ALTER TABLE [dbo].[tbl_premium_user]
ADD CONSTRAINT [PK_tbl_premium_user]
    PRIMARY KEY CLUSTERED ([id] ASC);
GO

-- Creating primary key on [id] in table 'tbl_events'
ALTER TABLE [dbo].[tbl_events]
ADD CONSTRAINT [PK_tbl_events]
    PRIMARY KEY CLUSTERED ([id] ASC);
GO

-- Creating primary key on [id] in table 'tbl_news'
ALTER TABLE [dbo].[tbl_news]
ADD CONSTRAINT [PK_tbl_news]
    PRIMARY KEY CLUSTERED ([id] ASC);
GO

-- Creating primary key on [id] in table 'tbl_event_comment'
ALTER TABLE [dbo].[tbl_event_comment]
ADD CONSTRAINT [PK_tbl_event_comment]
    PRIMARY KEY CLUSTERED ([id] ASC);
GO

-- Creating primary key on [id] in table 'tbl_event_participant'
ALTER TABLE [dbo].[tbl_event_participant]
ADD CONSTRAINT [PK_tbl_event_participant]
    PRIMARY KEY CLUSTERED ([id] ASC);
GO

-- Creating primary key on [id] in table 'tbl_news_comment'
ALTER TABLE [dbo].[tbl_news_comment]
ADD CONSTRAINT [PK_tbl_news_comment]
    PRIMARY KEY CLUSTERED ([id] ASC);
GO

-- Creating primary key on [id] in table 'tbl_discussion_report'
ALTER TABLE [dbo].[tbl_discussion_report]
ADD CONSTRAINT [PK_tbl_discussion_report]
    PRIMARY KEY CLUSTERED ([id] ASC);
GO

-- Creating primary key on [id] in table 'tbl_discussion_comment_report'
ALTER TABLE [dbo].[tbl_discussion_comment_report]
ADD CONSTRAINT [PK_tbl_discussion_comment_report]
    PRIMARY KEY CLUSTERED ([id] ASC);
GO

-- Creating primary key on [id] in table 'tbl_news_comment_report'
ALTER TABLE [dbo].[tbl_news_comment_report]
ADD CONSTRAINT [PK_tbl_news_comment_report]
    PRIMARY KEY CLUSTERED ([id] ASC);
GO

-- Creating primary key on [id] in table 'tbl_event_comment_report'
ALTER TABLE [dbo].[tbl_event_comment_report]
ADD CONSTRAINT [PK_tbl_event_comment_report]
    PRIMARY KEY CLUSTERED ([id] ASC);
GO

-- Creating primary key on [id] in table 'vw_forget_password'
ALTER TABLE [dbo].[vw_forget_password]
ADD CONSTRAINT [PK_vw_forget_password]
    PRIMARY KEY CLUSTERED ([id] ASC);
GO

-- Creating primary key on [username], [password] in table 'vw_login'
ALTER TABLE [dbo].[vw_login]
ADD CONSTRAINT [PK_vw_login]
    PRIMARY KEY CLUSTERED ([username], [password] ASC);
GO

-- Creating primary key on [username], [password] in table 'vw_setting'
ALTER TABLE [dbo].[vw_setting]
ADD CONSTRAINT [PK_vw_setting]
    PRIMARY KEY CLUSTERED ([username], [password] ASC);
GO

-- Creating primary key on [id] in table 'tbl_ads'
ALTER TABLE [dbo].[tbl_ads]
ADD CONSTRAINT [PK_tbl_ads]
    PRIMARY KEY CLUSTERED ([id] ASC);
GO

-- Creating primary key on [id] in table 'tbl_course_history'
ALTER TABLE [dbo].[tbl_course_history]
ADD CONSTRAINT [PK_tbl_course_history]
    PRIMARY KEY CLUSTERED ([id] ASC);
GO

-- Creating primary key on [id] in table 'tbl_user'
ALTER TABLE [dbo].[tbl_user]
ADD CONSTRAINT [PK_tbl_user]
    PRIMARY KEY CLUSTERED ([id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [discussionid] in table 'tbl_discussion_comment'
ALTER TABLE [dbo].[tbl_discussion_comment]
ADD CONSTRAINT [FK__tbl_discu__discu__38996AB5]
    FOREIGN KEY ([discussionid])
    REFERENCES [dbo].[tbl_discussion]
        ([id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK__tbl_discu__discu__38996AB5'
CREATE INDEX [IX_FK__tbl_discu__discu__38996AB5]
ON [dbo].[tbl_discussion_comment]
    ([discussionid]);
GO

-- Creating foreign key on [roleid] in table 'tbl_role_mapping'
ALTER TABLE [dbo].[tbl_role_mapping]
ADD CONSTRAINT [fk_roleid]
    FOREIGN KEY ([roleid])
    REFERENCES [dbo].[tbl_roles]
        ([id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'fk_roleid'
CREATE INDEX [IX_fk_roleid]
ON [dbo].[tbl_role_mapping]
    ([roleid]);
GO

-- Creating foreign key on [badgeid] in table 'tbl_course'
ALTER TABLE [dbo].[tbl_course]
ADD CONSTRAINT [fk_badgeid_course]
    FOREIGN KEY ([badgeid])
    REFERENCES [dbo].[tbl_badge]
        ([badgeid])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'fk_badgeid_course'
CREATE INDEX [IX_fk_badgeid_course]
ON [dbo].[tbl_course]
    ([badgeid]);
GO

-- Creating foreign key on [courseid] in table 'tbl_materi'
ALTER TABLE [dbo].[tbl_materi]
ADD CONSTRAINT [fk_courseid]
    FOREIGN KEY ([courseid])
    REFERENCES [dbo].[tbl_course]
        ([courseid])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'fk_courseid'
CREATE INDEX [IX_fk_courseid]
ON [dbo].[tbl_materi]
    ([courseid]);
GO

-- Creating foreign key on [courseid] in table 'tbl_quiz'
ALTER TABLE [dbo].[tbl_quiz]
ADD CONSTRAINT [fk_courseid_on_quiz]
    FOREIGN KEY ([courseid])
    REFERENCES [dbo].[tbl_course]
        ([courseid])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'fk_courseid_on_quiz'
CREATE INDEX [IX_fk_courseid_on_quiz]
ON [dbo].[tbl_quiz]
    ([courseid]);
GO

-- Creating foreign key on [courseid] in table 'tbl_answer'
ALTER TABLE [dbo].[tbl_answer]
ADD CONSTRAINT [fk_courseid_answer]
    FOREIGN KEY ([courseid])
    REFERENCES [dbo].[tbl_course]
        ([courseid])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'fk_courseid_answer'
CREATE INDEX [IX_fk_courseid_answer]
ON [dbo].[tbl_answer]
    ([courseid]);
GO

-- Creating foreign key on [premiumid] in table 'tbl_premium_payment'
ALTER TABLE [dbo].[tbl_premium_payment]
ADD CONSTRAINT [fk_premiumid_payment]
    FOREIGN KEY ([premiumid])
    REFERENCES [dbo].[tbl_premium_pricing]
        ([id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'fk_premiumid_payment'
CREATE INDEX [IX_fk_premiumid_payment]
ON [dbo].[tbl_premium_payment]
    ([premiumid]);
GO

-- Creating foreign key on [eventid] in table 'tbl_event_comment'
ALTER TABLE [dbo].[tbl_event_comment]
ADD CONSTRAINT [FK__tbl_event__event__5BE2A6F2]
    FOREIGN KEY ([eventid])
    REFERENCES [dbo].[tbl_events]
        ([id])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK__tbl_event__event__5BE2A6F2'
CREATE INDEX [IX_FK__tbl_event__event__5BE2A6F2]
ON [dbo].[tbl_event_comment]
    ([eventid]);
GO

-- Creating foreign key on [eventid] in table 'tbl_event_participant'
ALTER TABLE [dbo].[tbl_event_participant]
ADD CONSTRAINT [FK__tbl_event__event__5EBF139D]
    FOREIGN KEY ([eventid])
    REFERENCES [dbo].[tbl_events]
        ([id])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK__tbl_event__event__5EBF139D'
CREATE INDEX [IX_FK__tbl_event__event__5EBF139D]
ON [dbo].[tbl_event_participant]
    ([eventid]);
GO

-- Creating foreign key on [newsid] in table 'tbl_news_comment'
ALTER TABLE [dbo].[tbl_news_comment]
ADD CONSTRAINT [FK__tbl_news___newsi__7F2BE32F]
    FOREIGN KEY ([newsid])
    REFERENCES [dbo].[tbl_news]
        ([id])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK__tbl_news___newsi__7F2BE32F'
CREATE INDEX [IX_FK__tbl_news___newsi__7F2BE32F]
ON [dbo].[tbl_news_comment]
    ([newsid]);
GO

-- Creating foreign key on [discussid] in table 'tbl_discussion_report'
ALTER TABLE [dbo].[tbl_discussion_report]
ADD CONSTRAINT [FK__tbl_discu__discu__70DDC3D8]
    FOREIGN KEY ([discussid])
    REFERENCES [dbo].[tbl_discussion]
        ([id])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK__tbl_discu__discu__70DDC3D8'
CREATE INDEX [IX_FK__tbl_discu__discu__70DDC3D8]
ON [dbo].[tbl_discussion_report]
    ([discussid]);
GO

-- Creating foreign key on [commentid] in table 'tbl_discussion_comment_report'
ALTER TABLE [dbo].[tbl_discussion_comment_report]
ADD CONSTRAINT [FK__tbl_discu__comme__75A278F5]
    FOREIGN KEY ([commentid])
    REFERENCES [dbo].[tbl_discussion_comment]
        ([id])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK__tbl_discu__comme__75A278F5'
CREATE INDEX [IX_FK__tbl_discu__comme__75A278F5]
ON [dbo].[tbl_discussion_comment_report]
    ([commentid]);
GO

-- Creating foreign key on [commentid] in table 'tbl_news_comment_report'
ALTER TABLE [dbo].[tbl_news_comment_report]
ADD CONSTRAINT [FK__tbl_news___comme__0C85DE4D]
    FOREIGN KEY ([commentid])
    REFERENCES [dbo].[tbl_news_comment]
        ([id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK__tbl_news___comme__0C85DE4D'
CREATE INDEX [IX_FK__tbl_news___comme__0C85DE4D]
ON [dbo].[tbl_news_comment_report]
    ([commentid]);
GO

-- Creating foreign key on [commentid] in table 'tbl_event_comment_report'
ALTER TABLE [dbo].[tbl_event_comment_report]
ADD CONSTRAINT [FK__tbl_event__comme__14270015]
    FOREIGN KEY ([commentid])
    REFERENCES [dbo].[tbl_event_comment]
        ([id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK__tbl_event__comme__14270015'
CREATE INDEX [IX_FK__tbl_event__comme__14270015]
ON [dbo].[tbl_event_comment_report]
    ([commentid]);
GO

-- Creating foreign key on [courseid] in table 'tbl_course_history'
ALTER TABLE [dbo].[tbl_course_history]
ADD CONSTRAINT [FK__tbl_cours__cours__787EE5A0]
    FOREIGN KEY ([courseid])
    REFERENCES [dbo].[tbl_course]
        ([courseid])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK__tbl_cours__cours__787EE5A0'
CREATE INDEX [IX_FK__tbl_cours__cours__787EE5A0]
ON [dbo].[tbl_course_history]
    ([courseid]);
GO

-- Creating foreign key on [userid] in table 'tbl_activation'
ALTER TABLE [dbo].[tbl_activation]
ADD CONSTRAINT [fk_activation]
    FOREIGN KEY ([userid])
    REFERENCES [dbo].[tbl_user]
        ([id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'fk_activation'
CREATE INDEX [IX_fk_activation]
ON [dbo].[tbl_activation]
    ([userid]);
GO

-- Creating foreign key on [userid] in table 'tbl_answer'
ALTER TABLE [dbo].[tbl_answer]
ADD CONSTRAINT [fk_userid_answer]
    FOREIGN KEY ([userid])
    REFERENCES [dbo].[tbl_user]
        ([id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'fk_userid_answer'
CREATE INDEX [IX_fk_userid_answer]
ON [dbo].[tbl_answer]
    ([userid]);
GO

-- Creating foreign key on [badgeid] in table 'tbl_user'
ALTER TABLE [dbo].[tbl_user]
ADD CONSTRAINT [fk_user_badge]
    FOREIGN KEY ([badgeid])
    REFERENCES [dbo].[tbl_badge]
        ([badgeid])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'fk_user_badge'
CREATE INDEX [IX_fk_user_badge]
ON [dbo].[tbl_user]
    ([badgeid]);
GO

-- Creating foreign key on [userid] in table 'tbl_course_history'
ALTER TABLE [dbo].[tbl_course_history]
ADD CONSTRAINT [FK__tbl_cours__useri__160F4887]
    FOREIGN KEY ([userid])
    REFERENCES [dbo].[tbl_user]
        ([id])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK__tbl_cours__useri__160F4887'
CREATE INDEX [IX_FK__tbl_cours__useri__160F4887]
ON [dbo].[tbl_course_history]
    ([userid]);
GO

-- Creating foreign key on [userid] in table 'tbl_discussion'
ALTER TABLE [dbo].[tbl_discussion]
ADD CONSTRAINT [FK__tbl_discu__useri__37A5467C]
    FOREIGN KEY ([userid])
    REFERENCES [dbo].[tbl_user]
        ([id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK__tbl_discu__useri__37A5467C'
CREATE INDEX [IX_FK__tbl_discu__useri__37A5467C]
ON [dbo].[tbl_discussion]
    ([userid]);
GO

-- Creating foreign key on [userid] in table 'tbl_discussion_comment'
ALTER TABLE [dbo].[tbl_discussion_comment]
ADD CONSTRAINT [FK__tbl_discu__useri__398D8EEE]
    FOREIGN KEY ([userid])
    REFERENCES [dbo].[tbl_user]
        ([id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK__tbl_discu__useri__398D8EEE'
CREATE INDEX [IX_FK__tbl_discu__useri__398D8EEE]
ON [dbo].[tbl_discussion_comment]
    ([userid]);
GO

-- Creating foreign key on [reporter] in table 'tbl_discussion_comment_report'
ALTER TABLE [dbo].[tbl_discussion_comment_report]
ADD CONSTRAINT [FK__tbl_discu__repor__06CD04F7]
    FOREIGN KEY ([reporter])
    REFERENCES [dbo].[tbl_user]
        ([id])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK__tbl_discu__repor__06CD04F7'
CREATE INDEX [IX_FK__tbl_discu__repor__06CD04F7]
ON [dbo].[tbl_discussion_comment_report]
    ([reporter]);
GO

-- Creating foreign key on [reporter] in table 'tbl_discussion_report'
ALTER TABLE [dbo].[tbl_discussion_report]
ADD CONSTRAINT [FK__tbl_discu__repor__02FC7413]
    FOREIGN KEY ([reporter])
    REFERENCES [dbo].[tbl_user]
        ([id])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK__tbl_discu__repor__02FC7413'
CREATE INDEX [IX_FK__tbl_discu__repor__02FC7413]
ON [dbo].[tbl_discussion_report]
    ([reporter]);
GO

-- Creating foreign key on [userid] in table 'tbl_event_comment'
ALTER TABLE [dbo].[tbl_event_comment]
ADD CONSTRAINT [FK__tbl_event__useri__76969D2E]
    FOREIGN KEY ([userid])
    REFERENCES [dbo].[tbl_user]
        ([id])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK__tbl_event__useri__76969D2E'
CREATE INDEX [IX_FK__tbl_event__useri__76969D2E]
ON [dbo].[tbl_event_comment]
    ([userid]);
GO

-- Creating foreign key on [reporter] in table 'tbl_event_comment_report'
ALTER TABLE [dbo].[tbl_event_comment_report]
ADD CONSTRAINT [FK__tbl_event__repor__0E6E26BF]
    FOREIGN KEY ([reporter])
    REFERENCES [dbo].[tbl_user]
        ([id])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK__tbl_event__repor__0E6E26BF'
CREATE INDEX [IX_FK__tbl_event__repor__0E6E26BF]
ON [dbo].[tbl_event_comment_report]
    ([reporter]);
GO

-- Creating foreign key on [userid] in table 'tbl_event_participant'
ALTER TABLE [dbo].[tbl_event_participant]
ADD CONSTRAINT [FK__tbl_event__useri__7B5B524B]
    FOREIGN KEY ([userid])
    REFERENCES [dbo].[tbl_user]
        ([id])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK__tbl_event__useri__7B5B524B'
CREATE INDEX [IX_FK__tbl_event__useri__7B5B524B]
ON [dbo].[tbl_event_participant]
    ([userid]);
GO

-- Creating foreign key on [userid] in table 'tbl_news_comment'
ALTER TABLE [dbo].[tbl_news_comment]
ADD CONSTRAINT [FK__tbl_news___useri__7E37BEF6]
    FOREIGN KEY ([userid])
    REFERENCES [dbo].[tbl_user]
        ([id])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK__tbl_news___useri__7E37BEF6'
CREATE INDEX [IX_FK__tbl_news___useri__7E37BEF6]
ON [dbo].[tbl_news_comment]
    ([userid]);
GO

-- Creating foreign key on [reporter] in table 'tbl_news_comment_report'
ALTER TABLE [dbo].[tbl_news_comment_report]
ADD CONSTRAINT [FK__tbl_news___repor__0A9D95DB]
    FOREIGN KEY ([reporter])
    REFERENCES [dbo].[tbl_user]
        ([id])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK__tbl_news___repor__0A9D95DB'
CREATE INDEX [IX_FK__tbl_news___repor__0A9D95DB]
ON [dbo].[tbl_news_comment_report]
    ([reporter]);
GO

-- Creating foreign key on [userid] in table 'tbl_premium_payment'
ALTER TABLE [dbo].[tbl_premium_payment]
ADD CONSTRAINT [fk_userid_payment]
    FOREIGN KEY ([userid])
    REFERENCES [dbo].[tbl_user]
        ([id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'fk_userid_payment'
CREATE INDEX [IX_fk_userid_payment]
ON [dbo].[tbl_premium_payment]
    ([userid]);
GO

-- Creating foreign key on [userid] in table 'tbl_premium_user'
ALTER TABLE [dbo].[tbl_premium_user]
ADD CONSTRAINT [fk_premium_user]
    FOREIGN KEY ([userid])
    REFERENCES [dbo].[tbl_user]
        ([id])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'fk_premium_user'
CREATE INDEX [IX_fk_premium_user]
ON [dbo].[tbl_premium_user]
    ([userid]);
GO

-- Creating foreign key on [userid] in table 'tbl_recovery'
ALTER TABLE [dbo].[tbl_recovery]
ADD CONSTRAINT [fk_recov_userid]
    FOREIGN KEY ([userid])
    REFERENCES [dbo].[tbl_user]
        ([id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'fk_recov_userid'
CREATE INDEX [IX_fk_recov_userid]
ON [dbo].[tbl_recovery]
    ([userid]);
GO

-- Creating foreign key on [userid] in table 'tbl_role_mapping'
ALTER TABLE [dbo].[tbl_role_mapping]
ADD CONSTRAINT [fk_userid]
    FOREIGN KEY ([userid])
    REFERENCES [dbo].[tbl_user]
        ([id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'fk_userid'
CREATE INDEX [IX_fk_userid]
ON [dbo].[tbl_role_mapping]
    ([userid]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------