﻿
$(document).ready(function () {
    $.noConflict();
    $("#report").DataTable();
    $("#discussion_report").DataTable({
    "responsive": true,
        "scrollX": true
    });
    $("#discussion_comment_report").DataTable({
    "responsive": true,
        "scrollX": true
    });
    $("#news_comment_report").DataTable({
    "responsive": true,
        "scrollX": true
    });
    $("#event_comment_report").DataTable({
    "responsive": true,
        "scrollX": true
    });

});

function detailDiscussion(discussion) {
    var discussionid = discussion.getAttribute("data-discussid");
    $.ajax({
    url: "/Admin/DiscussionReportDetail",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        method: 'GET',
        data: {discussionid: discussionid },
        beforeSend: function () {
    $('#modal_body').html('<p>loading data....</p>');
        },
        success: function (response) {
            var reportid = discussion.getAttribute("data-reportid");
            var data = JSON.parse(response);
            $('#modal_body').html('<h5>' + data.title + '</h5>' + '<p>' + data.description + '</p>');
            $('#modal_footer').html('<button class="btn btn-warning btn-sm" data-reportid="' + reportid + '" onclick="deleteReport(this)" >Delete Report</button> <button class="btn btn-danger btn-sm" data-discussionid="' + data.id + '" onclick="deleteDiscussion(this)">Delete Discussion</button>');
        }
    });
}

function deleteReport(report) {
    var reportid = report.getAttribute("data-reportid");
    var r = confirm("Are you sure want to delete this report?");
    if (r == true) {
    $.ajax({
        url: "/Admin/DeleteDiscussionReport",
        type: 'POST',
        data: { reportid: reportid },
        success: function () {
            window.location.reload();
        }
    });
    }
}

function deleteDiscussion(report) {
    var discussid = report.getAttribute("data-discussionid")
    var r = confirm("Are you sure want to delete this discussion post? this action will also delete comments and reported comments associated with this post");
    if (r == true) {
    $.ajax({
        url: "/User/DeleteDiscussion",
        type: 'POST',
        data: { discussid: discussid },
        success: function () {
            window.location.reload();
        }
    });
    }
}



function detailDiscussionComment(discussion) {
    var discussioncommentid = discussion.getAttribute("data-discusscommentid");
    $.ajax({
    url: "/Admin/DiscussionCommentReportDetail",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        method: 'GET',
        data: {discussioncommentid: discussioncommentid },
        beforeSend: function () {
    $('#modal_body').html('<p>loading data....</p>');
        },
        success: function (response) {
            var reportid = discussion.getAttribute("data-reportid");
            var data = JSON.parse(response);
            $('#modal_body').html('<p>' + data.comment + '</p>');
            $('#modal_footer').html('<button class="btn btn-warning btn-sm" data-reportid="' + reportid + '" onclick="deleteDiscussionCommentReport(this)" >Delete Report</button> <button class="btn btn-danger btn-sm" data-discussioncommentid="' + data.id + '" onclick="deleteDiscussionComment(this)">Delete Comment</button>');
        }
    });
}

function deleteDiscussionCommentReport(report) {
    var reportid = report.getAttribute("data-reportid");
    var r = confirm("Are you sure want to delete this report?");
    if (r == true) {
    $.ajax({
        url: "/Admin/DeleteDiscussionCommentReport",
        type: 'POST',
        data: { reportid: reportid },
        success: function () {
            window.location.reload();
        }
    });
    }
}

function deleteDiscussionComment(report) {
    var commentid = report.getAttribute("data-discussioncommentid");
    var r = confirm("Are you sure want to delete this comment?");
    if (r == true) {
    $.ajax({
        url: "/User/DeleteCommentDiscussion",
        type: 'POST',
        data: { commentdiscussid: commentid },
        success: function () {
            window.location.reload();
        }
    });
    }

}



function detailNewsComment(news) {
    var newscommentid = news.getAttribute("data-newscommentid");
    $.ajax({
    url: "/Admin/NewsCommentReportDetail",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        method: 'GET',
        data: {newscommentid: newscommentid },
        beforeSend: function () {
    $('#modal_body').html('<p>loading data....</p>');
        },
        success: function (response) {
            var reportid = news.getAttribute("data-reportid");
            var data = JSON.parse(response);
            $('#modal_body').html('<p>' + data.comment + '</p>');
            $('#modal_footer').html('<button class="btn btn-warning btn-sm" data-reportid="' + reportid + '" onclick="deleteNewsCommentReport(this)" >Delete Report</button> <button class="btn btn-danger btn-sm" data-newscommentid="' + data.id + '" onclick="deleteNewsComment(this)">Delete Comment</button>');
        }
    });
}

function deleteNewsCommentReport(report) {
    var reportid = report.getAttribute("data-reportid");
    var r = confirm("Are you sure want to delete this report?");
    if (r == true) {
    $.ajax({
        url: "/Admin/DeleteNewsCommentReport",
        type: 'POST',
        data: { reportid: reportid },
        success: function () {
            window.location.reload();
        }
    });
    }
}

function deleteNewsComment(report) {
    var commentid = report.getAttribute("data-newscommentid");
    var r = confirm("Are you sure want to delete this comment?");
    if (r == true) {
    $.ajax({
        url: "/User/DeleteCommentNews",
        type: 'POST',
        data: { commentnewsid: commentid },
        success: function () {
            window.location.reload();
        }
    });
    }
}



function detailEventComment(event) {
    var eventcommentid = event.getAttribute("data-eventcommentid");
    $.ajax({
    url: "/Admin/EventCommentReportDetail",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        method: 'GET',
        data: {eventcommentid: eventcommentid },
        beforeSend: function () {
    $('#modal_body').html('<p>loading data....</p>');
        },
        success: function (response) {
            var reportid = event.getAttribute("data-reportid");
            var data = JSON.parse(response);
            $('#modal_body').html('<p>' + data.comment + '</p>');
            $('#modal_footer').html('<button class="btn btn-warning btn-sm" data-reportid="' + reportid + '" onclick="deleteEventCommentReport(this)" >Delete Report</button> <button class="btn btn-danger btn-sm" data-eventcommentid="' + data.id + '" onclick="deleteEventComment(this)">Delete Comment</button>');
        }
    });
}

function deleteEventCommentReport(report) {
    var reportid = report.getAttribute("data-reportid");
    var r = confirm("Are you sure want to delete this report?");
    if (r == true) {
    $.ajax({
        url: "/Admin/DeleteEventCommentReport",
        type: 'POST',
        data: { reportid: reportid },
        success: function () {
            window.location.reload();
        }
    });
    }
}

function deleteEventComment(report) {
    var commentid = report.getAttribute("data-eventcommentid");
    var r = confirm("Are you sure want to delete this comment?");
    if (r == true) {
    $.ajax({
        url: "/User/DeleteCommentEvent",
        type: 'POST',
        data: { commenteventid: commentid },
        success: function () {
            window.location.reload();
        }
    });
    }
}

