﻿$(function () {
    $("#submitNews").click(function () {

        if ($("form").valid()) {

            var title = $("#news_title").val();
            var description = $("#news_desc").val();
            var source = $("#news_source").val();
            var photo = $("#news_pic")[0].files[0];

            var formData = new FormData();

            formData.append("title", title);
            formData.append("description", description);
            formData.append("source", source);
            formData.append("photo", photo);

            $.ajax({
                url: "/Admin/SaveNews",
                type: 'POST',
                data: formData,
                contentType: false,
                processData: false,
                success: function (data) {
                    if (data == 0) {
                        alert("Failed to save event");
                    }
                }
            });
        }
    });
});

$(function getAllNews() {
    $.ajax({
        url: "/Admin/ShowNews",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        method: 'GET',
        beforeSend: function () {
            $('#news_loading').html('<div class="text-center"><p>Loading Data</p></div>');
        },
        success: function (response) {
            $('#news_loading').html('');
            var data = JSON.parse(response);
            $.each(data, function (key) {
                var post =
                    '<tr>' +
                    '<td style="width:200px">' +
                    data[key].title +
                    '</td>' +
                    '<td>' +
                    '<img src="../../img/news-img/' + data[key].photo + '" style="width:75px"/>' +
                    '</td>' +
                    '<td style="width:500px;white-space:nowrap;overflow:hidden;text-overflow:ellipsis;display:-webkit-box;-webkit-line-clamp:5;-webkit-box-orient:vertical">' +
                    data[key].description +
                    '</td>' +
                    '<td>' +
                    data[key].source +
                    '</td>' +
                    '<td>' +
                    data[key].date_posted +
                    '</td>' +
                    '<td>' +
                    '<a href="/Admin/News/Edit/' + data[key].encrypt_id + '" class="btn btn-primary btn-sm" style="width:30px" title="Edit" ><i class="fa fa-edit"></i></a> ' +
                    '<button class="btn btn-sm btn-danger" style="width:30px; margin-top:5px" title="Delete" data-news="' + data[key].encrypt_id + '" onclick="deleteNews(this)"><i class="fa fa-trash"></i></button>' +
                    '</td>' +
                    '</tr>';
                $("#post_news_body").append(post);
            });
            $.noConflict();
            $("#post_news").DataTable({
                responsive: true,
                "scrollX": true
            });

        }
    });
});

function deleteNews(news) {
    var encrypt_id = news.getAttribute("data-news");
    var r = confirm("Are you sure want to delete this news?");
    if (r == true) {
        $.ajax({
            url: "/Admin/DeleteNews",
            type: 'POST',
            data: { news_encrypt_id: encrypt_id },
            success: function (response) {
                if (response == 1) {
                    window.location.reload();
                } else {
                    alert("failed to delete news");
                }
            }
        });
    }
}
