﻿$(function () {
    $("#submitEvent").click(function () {
        if ($("form").valid()) {
            var title = $("#event_title").val();
            var description = $("#event_desc").val();
            var location = $("#event_loc").val();
            var contact = $("#event_con").val();
            var date = $("#event_date").val();
            var time = $("#event_time").val();
            var photo = $("#event_pic").get(0).files[0];

            var formData = new FormData();

            formData.append("title", title);
            formData.append("description", description);
            formData.append("location", location);
            formData.append("contact", contact);
            formData.append("date", date);
            formData.append("time", time);
            formData.append("photo", photo);

            $.ajax({
                url: "/Admin/SaveEvent",
                type: 'POST',
                data: formData,
                contentType: false,
                processData: false,
                success: function (data) {
                    if (data == 0) {
                        alert("Failed to save event");
                    }
                }
            });
        }


    });
});

$(function getAllEvent() {
    $.ajax({
        url: "/Admin/ShowEvent",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        method: 'GET',
        beforeSend: function () {
            $('#events_loading').html('<div class="text-center"><p>Loading Data</p></div>');
        },
        success: function (response) {
            $('#events_loading').html('');
            var data = JSON.parse(response);
            $.each(data, function (key) {
                var post =
                    '<tr>' +
                    '<td>' +
                    '<p style="width:200px; white-space:nowrap; overflow:hidden; text-overflow:ellipsis;" >' + data[key].title + '</p>' +
                    '</td>' +
                    '<td>' +
                    '<img src="../../img/event-img/' + data[key].photo + '" style="width:75px"/>' +
                    '</td>' +
                    '<td>' +
                    '<p style="width:250px; white-space:nowrap; overflow:hidden; text-overflow:ellipsis;" >' + data[key].description + '</p> ' +
                    '</td>' +
                    '<td>' +
                    data[key].date_posted +
                    '</td>' +
                    '<td>' +
                    data[key].date +
                    '</td>' +
                    '<td>' +
                    data[key].time +
                    '</td>' +
                    '<td>' +
                    data[key].location +
                    '</td>' +
                    '<td>' +
                    data[key].contact_person +
                    '</td>' +
                    '<td>' +
                    '<a href="/Admin/Event/Edit/' + data[key].encrypt_id + '" class="btn btn-primary btn-sm" style="width:30px" title="Edit"><i class="fa fa-edit"></i></a> ' +
                    '<button class="btn btn-sm btn-danger" style="width:30px; margin-top:5px" title="Delete" data-event="' + data[key].encrypt_id + '" onclick="deleteEvent(this)"><i class="fa fa-trash"></i></button>' +
                    '</td>' +
                    '</tr>';
                $("#post_event_body").append(post);
            });
            $.noConflict();
            $("#post_event").DataTable({
                responsive: true,
                "scrollX": true
            });
        }
    });
});

function deleteEvent(event) {
    var encrypt_id = event.getAttribute("data-event");
    var r = confirm("Are you sure want to delete this event?");
    if (r == true) {
    $.ajax({
        url: "/Admin/DeleteEvent",
        type: 'POST',
        data: { encrypt_id: encrypt_id },
        success: function (response) {
            if (response == 1) {
                window.location.reload();
            } else {
                alert("failed to delete event");
            }
        }
    });
    }
}
