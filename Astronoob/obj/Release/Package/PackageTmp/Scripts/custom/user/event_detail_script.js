﻿$(function getAllEventComments() {
    $.ajax({
        url: "/User/ShowEventComment",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        method: 'GET',
        data: { eventid: model_eventid },
        beforeSend: function () {
            $('#event_comment_area_body').html('<div class="text-center"><p>Loading Data</p></div>');
        },
        success: function (response) {
            $('#event_comment_area_body').html('');
            if (response == '[]') {
                $('#event_comment_area_body').html('<div class="text-center"><p>No Replies</p></div>');
            } else {
                var data = JSON.parse(response);
                $.each(data, function (key) {
                    var post =
                        '<tr>' +
                        '<td>' +
                        '<ol>' +
                        '<li class="single_comment_area">' +
                        '<div class="comment-content d-flex">' +
                        '<div class="comment-author">' +
                        '<img style="width:70px;height:100%" src="../../img/pp-img/' + data[key].photo + '" alt="author">' +
                        '</div>' +
                        '<div class="comment-meta">' +

                        '<h6>' + data[key].username + '</h6>' +
                        '<p>' + data[key].comment + '</p>' +
                        '<p class="comment-date">' + data[key].date + '</p>';



                     if (identity) {
                        var uname = identity_nm;
                        if (uname != data[key].username) {
                            post += '<button type = "button" class="btn btn-secondary btn-sm" style="width:30px" title="Report" data-commentevent="' + data[key].id + '" id="report_comment_modal" onclick ="reportCommentEvent(this)" ><i class="fa fa-flag"></i></button>';
                        } else {
                            post += '<button type="button" class="btn btn-info btn-sm" style="width:30px" title="Edit" data-commentid="' + data[key].id + '" data-comment="' + data[key].comment + '" id="edit_comment_modal" onclick="editCommentEvent(this)"><i class="fa fa-edit"></i></button> ';
                            post += '<button type = "button" class="btn btn-danger btn-sm" style="width:30px" title="Delete" data-commentevent="' + data[key].id + '" id="delete_comment_modal" onclick="deleteCommentEvent(this)"><i class="fa fa-trash"></i></button>';
                        }
                    }
                    else {
                        post +=
                            '<button type="button" class="btn btn-info btn-sm" title="Please login first to report this comment" disabled>' +
                            '<i class="fa fa-flag"></i>' +
                            '</button>';
                    }
                    post +=
                        '</div>' +
                        '</div>' +
                        '</li>' +
                        '</ol>' +
                        '</td>' +
                        '</tr>' +
                        '<hr/>';
                    $("#event_comment_area_body").append(post);
                });

                $.noConflict();

                $("#event_comment_area").DataTable({
                    "pageLength": 5,
                    "lengthChange": false,
                    "searching": false,
                    "info": false,
                    "sorting": false,
                    "responsive": true,
                    "scrollX": true
                });
            }
        }
    });
});

$(function () {
    $("#submitEventComment").click(function () {

        if ($("#post_event_comment").valid()) {

            var comment = $("#eventcomment").val();
            $.ajax({
                url: "/User/SaveEventComment",
                type: 'POST',
                data: { eventid: model_eventid, comment_text: comment },
            });
        }
    });
});


$(document).on("click", "button.joinEvent", function () {
    $("#event_join_modal").modal("show");
});

function joinEvent() {
    $("#joinEvent").removeClass("joinEvent").addClass("unjoinEvent");
    $("#joinEvent").html("Cancel Reminder");
    $("#event_join_modal").modal("hide");
    $.ajax({
        url: "/User/JoinEvent",
        type: 'POST',
        data: { eventid: model_eventid }
    });
}

$(document).on("click", "button.unjoinEvent", function () {
    $("#event_unjoin_modal").modal("show");
});

function unjoinEvent() {
    $("#joinEvent").removeClass("unjoinEvent").addClass("joinEvent");
    $("#joinEvent").html("Remind Me");
    $("#event_unjoin_modal").modal("hide");
    $.ajax({
        url: "/User/CancelJoinEvent",
        type: 'POST',
        data: { eventid: model_eventid }
    });
}

$(function () {
    $("#submitCommentReport").click(function () {
        var commentid = $("#reportcommentid").val();
        var option = $("input[name='option_comment']:checked", "#form_event_comment_report").val();;
        $.ajax({
            url: "/User/ReportCommentEvent",
            type: 'POST',
            data: { reportcommentid: commentid, option: option },
            success: function (data) {
                $('#event_report_comment_modal').modal('hide');
                $('#event_success_modal').modal('show');
            }
        });
    });
});

function reportCommentEvent(id) {
    $('#event_report_comment_modal').modal('show');
    $("#reportcommentid").val(id.getAttribute("data-commentevent"))
}

$(function () {
    $("#deleteCommentEvent").click(function () {
        var commentid = $("#deletecommentid").val();
        $.ajax({
            url: "/User/DeleteCommentEvent",
            type: 'POST',
            data: { commenteventid: commentid },
            success: function (data) {
                $('#event_delete_comment_modal').modal('hide');
                window.location.reload(true);
            }
        });
    });
});

function deleteCommentEvent(id) {
    $('#event_delete_comment_modal').modal('show');
    $("#deletecommentid").val(id.getAttribute("data-commentevent"))
}

$(function () {
    $("#submitCommentEdit").click(function () {

        if ($("#form_event_comment_edit").valid()) {
            var commentid = $("#editeventcommentid").val();
            var comment = $("#commentdesc").val();
            $.ajax({
                url: "/User/EditCommentevent",
                type: 'POST',
                data: { commenteventid: commentid, comment: comment }
            });
        }
    });
});

function editCommentEvent(id) {
    $('#event_edit_comment_modal').modal('show');
    $("#editeventcommentid").val(id.getAttribute("data-commentid"))
    $("#commentdesc").val(id.getAttribute("data-comment"))
}
