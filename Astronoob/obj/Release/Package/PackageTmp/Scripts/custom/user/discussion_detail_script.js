﻿$(function getAllComments() {
    $.ajax({
        url: "/User/ShowComment",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        method: 'GET',
        data: { postid: model_postid },
        beforeSend: function () {
           $('#comarea').html('<div class="section-heading"><h5>REPLIES</h5></div><div class="text-center"><p>Loading Data</p></div>');
        },
        success: function (response) {
            $('#comarea').html('');
            if (response == '[]') {
                $('#comarea').html('<div class="section-heading"><h5>REPLIES</h5></div><div class="text-center"><p>No Replies</p></div>');
            } else {
                var data = JSON.parse(response);
                $('#comarea').html('<div class="section-heading"><h5>REPLIES</h5></div><table id="post_comment_area" style="width:100%"><thead><tr><th></th> </tr></thead><tbody id="post_comment_area_body"></tbody></table>');
                $.each(data, function (key) {
                    var post =
                        '<tr>' +
                        '<td>' +
                        '<li class="single_comment_area">' +
                        '<div class="comment-content d-flex">' +
                        '<div class="comment-author">' +
                        '<img style="width:70px;height:100%" src="../../img/pp-img/' + data[key].photo + '" alt="author">' +
                        '</div>' +
                        '<div class="comment-meta">' +
                        '<h6>' + data[key].username + '</h6>' +
                        '<p>' + data[key].comment + '</p>' +
                        '<p class="comment-date">' + data[key].date + '</p>';

                        if (identity) {
                            var uname = identity_nm;
                            if (uname != data[key].username) {
                            post += '<button type = "button" class="btn btn-secondary btn-sm" style="width:30px" title="Report" data-commentdiscussion="' + data[key].id + '" id="report_comment_modal" onclick ="reportCommentDiscussion(this)" ><i class="fa fa-flag"></i></button >';

                            } else {
                            post += '<button type="button" class="btn btn-info btn-sm" style="width:30px" title="Edit" data-commentid="' + data[key].id + '" data-comment="' + data[key].comment + '" id="edit_comment_modal" onclick="editCommentDiscussion(this)"><i class="fa fa-edit"></i></button> ';
                                post += '<button type="button" class="btn btn-danger btn-sm" style="width:30px" title="Delete" data-commentdiscussion="' + data[key].id + '" id="delete_comment_modal" onclick="deleteCommentDiscussion(this)"><i class="fa fa-trash"></i></button>';
                            }
                        }
                        else {
                            post +=
                            '<button type="button" class="btn btn-info btn-sm" title="Please login first to report this comment" disabled>' +
                            '<i class="fa fa-flag"></i>' +
                            '</button>';
                        }
                    post +=
                        '</div>' +
                        '</div>' +
                        '</li>' +
                        '</td>' +
                        '</tr>';
                    $("#post_comment_area_body").append(post);
                });

                $.noConflict();

                $("#post_comment_area").DataTable({
                    "pageLength": 5,
                    "lengthChange": false,
                    "searching": false,
                    "info": false,
                    "sorting": false,
                    "responsive": true,
                    "scrollX": true
                }).page('last').draw('page');
            }
        }
    });
});

$(function () {
        $("#submitComment").click(function () {
            if ($("form").valid()) {
                var comment = $("#comment").val();
                $.ajax({
                    url: "/User/SaveComment",
                    type: 'POST',
                    data: { postid: model_postid, comment_text: comment },
            });
        }
    });
});




$(function () {
        $("#submitReport").click(function () {
            var discussid = $("#reportdiscusid").val();
            var option = $("input[name='option']:checked", "#form_discuss_report").val();;
            $.ajax({
                url: "/User/ReportDiscussion",
                type: 'POST',
                data: { reportdiscusid: discussid, option: option },
                success: function (data) {
                    $('#discuss_report_modal').modal('hide');
                    $('#discuss_success_modal').modal('show');
                }
            });
        });
});

    function reportDiscussion(id) {
        $('#discuss_report_modal').modal('show');
        $("#reportdiscusid").val(id.getAttribute("data-discussion"))
}


$(function () {
        $("#submitCommentReport").click(function () {
            var commentid = $("#reportcommentid").val();
            var option = $("input[name='option_comment']:checked", "#form_discuss_comment_report").val();;
            $.ajax({
                url: "/User/ReportCommentDiscussion",
                type: 'POST',
                data: { reportcommentid: commentid, option: option },
                success: function (data) {
                    $('#discuss_report_comment_modal').modal('hide');
                    $('#discuss_success_modal').modal('show');
                }
            });
        });
});

function reportCommentDiscussion(id) {
        $('#discuss_report_comment_modal').modal('show');
    $("#reportcommentid").val(id.getAttribute("data-commentdiscussion"))
}



$(function () {
        $("#deleteDiscussion").click(function () {
            var discussid = $("#deletediscusid").val();
            $.ajax({
                url: "/User/DeleteDiscussion",
                type: 'POST',
                data: { discussid: discussid },
                success: function (data) {
                    window.location.href = '/Discussions';
                }
            });
        });
});

function deleteDiscussion(id) {
        $('#discuss_delete_modal').modal('show');
    $("#deletediscusid").val(id.getAttribute("data-discussion"))
}


$(function () {
        $("#deleteCommentDiscussion").click(function () {
            var commentid = $("#deletecommentid").val();
            $.ajax({
                url: "/User/DeleteCommentDiscussion",
                type: 'POST',
                data: { commentdiscussid: commentid },
                success: function (data) {
                    $('#discuss_delete_comment_modal').modal('hide');
                    window.location.reload(true);
                }
            });
        });
});

function deleteCommentDiscussion(id) {
        $('#discuss_delete_comment_modal').modal('show');
    $("#deletecommentid").val(id.getAttribute("data-commentdiscussion"))
}


$(function () {
        $("#submitEdit").click(function () {

            if ($("#form_discuss_edit").valid()) {
                var discussid = $("#editdiscusid").val();
                var title = $("#discusstitle").val();
                var description = $("#discussdesc").val();

                $.ajax({
                    url: "/User/EditDiscussion",
                    type: 'POST',
                    data: { editdiscusid: discussid, title: title, description: description }
                });
            }
        });
});

function editDiscussion(id) {
        $('#discuss_edit_modal').modal('show');
    $("#editdiscusid").val(id.getAttribute("data-discussion"))
    $("#discusstitle").val(id.getAttribute("data-title"))
    $("#discussdesc").val(id.getAttribute("data-description"))
}

$(function () {
        $("#submitCommentEdit").click(function () {

            if ($("#form_discuss_comment_edit").valid()) {
                var commentid = $("#editdiscuscommentid").val();
                var comment = $("#commentdesc").val();
                $.ajax({
                    url: "/User/EditCommentDiscussion",
                    type: 'POST',
                    data: { commentdiscussid: commentid, comment: comment }
                });
            }
        });
});

function editCommentDiscussion(id) {
        $('#discuss_edit_comment_modal').modal('show');
    $("#editdiscuscommentid").val(id.getAttribute("data-commentid"))
    $("#commentdesc").val(id.getAttribute("data-comment"))
}
