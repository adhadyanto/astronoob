﻿$(function getAllDiscussions() {
    $.ajax({
        url: "/User/ShowPost",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        method: 'GET',
        beforeSend: function () {
            $('discussion_loading').html('<div class="text-center"><p>Loading Data</p></div>');
        },
        success: function (response) {
            $('discussion_loading').html('');
            if (response == '[]') {
                $('#discussion_loading').html('<div class="text-center"><p>No Discussion Available</p></div>');
            } else {
                var data = JSON.parse(response);
                $.each(data, function (key) {
                    var post =
                        '<tr>' +
                        '<td>' +
                        '<ol>' +
                        '<li class="single_comment_area">' +
                        '<div class="comment-content d-flex">' +
                        '<div class="comment-author">' +
                        '<img style="width:70px;height:100%" src="../../img/pp-img/' + data[key].photo + '" alt="author">' +
                        '</div>' +
                        '<div class="comment-meta">' +

                        '<a href = "/Discussions/' + data[key].encrypt_id + '"><h6 style="width:400px; white-space:nowrap; overflow:hidden; text-overflow:ellipsis;">' + data[key].title + '</h6></a>' +
                        '<p>' + data[key].description + '</p>' +
                        '<p class="comment-date"> Posted by ' + data[key].username + ' on ' + data[key].date + '</p>' +

                        '</div>' +
                        '</div>' +
                        '</li>' +
                        '</ol>' +
                        '</td>' +
                        '</tr>';

                    $("#post_area_body").append(post);
                });

                $.noConflict();

                $("#post_area").DataTable({
                    "pageLength": 5,
                    "lengthChange": false,
                    "searching": false,
                    "info": false,
                    "sorting": false,
                    "responsive": true,
                    "scrollX": true
                });
            }
        }
    });
});

$(function () {
    $("#submitPost").click(function () {

        if ($("form").valid()) {
            var title = $("#dis_title").val();
            var post = $("#dis_post").val();
            $.ajax({
                url: "/User/SavePost",
                type: 'POST',
                data: { title: title, post: post },
            });
        }
    });
});
