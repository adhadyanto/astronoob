﻿using System.Web;
using System.Web.Optimization;

namespace TestAstro
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js"));

            bundles.Add(new ScriptBundle("~/bundles/js/jquery-3").Include(
                      "~/Scripts/js/jquery/jquery-3.2.1.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/js").Include(
                      "~/Scripts/js/jquery/jquery-2.2.4.min.js",
                      "~/Scripts/jquery.validate.min.js",
                      "~/Scripts/jquery.validate.unobtrusive.min.js",
                      "~/Scripts/js/bootstrap/popper.min.js",
                      "~/Scripts/js/bootstrap/bootstrap.min.js",
                      "~/Scripts/js/plugins/plugins.js",
                      "~/Scripts/js/active.js"));

            bundles.Add(new StyleBundle("~/bundles/css").Include(
                      "~/Content/bootstrap.min.css",
                      "~/Content/classy-nav.css",
                      "~/Content/font-awesome.min.css",
                      "~/Content/themify-icons.css",
                      "~/Content/owl.carousel.min.css",
                      "~/Content/animate.css",
                      "~/Content/magnific-popup.css",
                      "~/Content/style.css",
                      "~/Content/style.css.map"));

            bundles.Add(new ScriptBundle("~/bundles/js/dataTables").Include(
                      "~/Scripts/js/jquery/jquery-3.2.1.min.js",
                      "~/Scripts/js/bootstrap/bootstrap.min.js",
                      "~/Scripts/js/jquery/jquery.dataTables-1.10.22.min.js"
            ));

            bundles.Add(new StyleBundle("~/bundles/css/dataTables").Include(
                      "~/Content/jquery.dataTables-1.10.22.min.css"));

            bundles.Add(new ScriptBundle("~/bundles/js/validator").Include(
                      "~/Scripts/jquery.validate.min.js",
                      "~/Scripts/jquery.validate.unobtrusive.min.js"));

            bundles.Add(new StyleBundle("~/bundles/css/custom").Include(
                      "~/Content/custom.css"));

            bundles.Add(new ScriptBundle("~/bundles/js/admin/ads").Include(
                      "~/Scripts/custom/admin/ads_script.js"));

            bundles.Add(new ScriptBundle("~/bundles/js/admin/event").Include(
                      "~/Scripts/custom/admin/event_script.js"));

            bundles.Add(new ScriptBundle("~/bundles/js/admin/course").Include(
                      "~/Scripts/custom/admin/course_script.js"));

            bundles.Add(new ScriptBundle("~/bundles/js/admin/news").Include(
                      "~/Scripts/custom/admin/news_script.js"));

            bundles.Add(new ScriptBundle("~/bundles/js/admin/report").Include(
                      "~/Scripts/custom/admin/report_script.js"));

            bundles.Add(new ScriptBundle("~/bundles/js/admin/subject").Include(
                      "~/Scripts/custom/admin/subject_script.js"));

            bundles.Add(new ScriptBundle("~/bundles/js/admin/user").Include(
                      "~/Scripts/custom/admin/user_script.js"));

            bundles.Add(new ScriptBundle("~/bundles/js/user/course").Include(
                      "~/Scripts/custom/user/course_script.js"));

            bundles.Add(new ScriptBundle("~/bundles/js/user/course_detail").Include(
                      "~/Scripts/custom/user/course_detail_script.js"));

            bundles.Add(new ScriptBundle("~/bundles/js/user/discussion").Include(
                      "~/Scripts/custom/user/discussion_script.js"));

            bundles.Add(new ScriptBundle("~/bundles/js/user/discussion_detail").Include(
                      "~/Scripts/custom/user/discussion_detail_script.js"));

            bundles.Add(new ScriptBundle("~/bundles/js/user/event_detail").Include(
                      "~/Scripts/custom/user/event_detail_script.js"));

            bundles.Add(new ScriptBundle("~/bundles/js/user/news_detail").Include(
                      "~/Scripts/custom/user/news_detail_script.js"));

            bundles.Add(new ScriptBundle("~/bundles/js/user/quiz").Include(
                      "~/Scripts/custom/user/quiz_script.js"));
        }
    }
}
