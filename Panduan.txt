==============================================
		   Installasi
==============================================
- Pull semua file dari repository astronoob di
  GitLab dan tempatkan pada satu file.
- Jika sudah, extract file 
  "SqlServerTypes.zip" didalam folder "file
  Pendukung". Maka akan muncul 
  folder dengan nama "SqlServerTypes".
- Copy folder "SqlServerTypes" tadi ke dalam 
  folder "Astronoob", lalu replace folder yang
  sudah ada dengan folder yang baru di copy.
- Restore database pada model project 
  (nama database: db_project), khusus untuk 
  bagian view, ketentuan lebih lanjut dapat 
  dilihat pada file catatan.txt.
- Lalu input semua data pada file 
  (db_project.sql) ke model yang tadi telah
  direstore dari visual studio project. 
- Ubah ConnectionString pada project
  (Web.config) dan pada service (App.config) 
  sesuai dengan datasource yang ada dengan id
  dan password milik datasource.
- Selanjutnya buka Command Prompt sebagai
  administrator.
- lalu install AstronoobService dengan cara
  akses C:\Windows\Microsoft.NET\Framework64\
  v4.0.30319>InstallUtil.exe "i" 
  "lokasi file service\AstronoobService.exe"
- Jika sudah, buka Windows Service lalu cari
  "AstronoobService".
- Pilih "AstronoobService", Klik kanan lalu
  pilih tombol start. Maka otomatis file 
  service akan langsung berjalan.
- Selanjutnya, buka Ms. Visual Studio lalu 
  buka file ekstensi ".sln" yang berada 
  didalam folder "Astronoob" dan tunggu 
  beberapa saat untuk membuka project.
- Jika project sudah terbuka, klik tombol 
  panah berwarna hijau di bagian atas Visual
  Studio untuk menjalankan project sesuai
  dengan browser yang telah di pilih. 

==============================================
		   Home Menu
==============================================
- Ditampilkan slider berupa pengenalan
  aplikasi astronoob.
- Ditampilkan menu news dan event terakhir
  yang kalau di klik akan diarahkan ke halaman
  news atau event yang di pilih .
- Footer yang berisi about aplikasi astronoob
  disertai dengan informasi kontak dibagian
  contact us. Footer juga berisi copyright dan
  Menu home yang jika di klik akan diarahkan 
  ke halaman homepage.
- Terdapat iklan untuk visitor dan user
  generic di sebelah kiri bawah dan kanan
  bawah.

==============================================
		   News Menu
==============================================
- Berisi daftar kabar berita tentang astronomi
  yang berurut dari terbaru hingga yang telah
  lalu dengan gambar, judul dan juga isi
  deskripsi.
- Jika di klik judul dari daftar yang sudah di
  sebut sebelumnya, maka akan diarahkan ke 
  halaman news detail.
- Terdapat iklan untuk visitor dan user
  generic di sebelah kanan.

==============================================
		   News Detail
==============================================
- Berisi isi pokok dari kabar berita yang di
  pilih di bagian news menu.
- halaman berisi judul, gambar, isi deskripsi,
  tanggal dan waktu di post ke news detail.
- halaman juga berisi seksi komentar untuk 
  memberi tanggapan terhadap kabar berita yang
  disajikan.
- Tersedia kolom post komentar / post reply 
  yang digunakan untuk menginput komentar
  tanggapan tentang kabar berita (harus login
  terlebih dahulu).
- Sisanya dibawah post reply merupakan kolom 
  daftar komentar yang telah di post.
- Jika komentar adalah milik user, maka user
  dapat mengedit ataupun menghapus komentar
  tersebut. Jika bukan milik user, maka user
  dapat mereport komentar tersebut.
- Terdapat iklan untuk visitor dan user
  generic di sebelah kanan.

==============================================
		  Events Menu
==============================================
- Berisi daftar event tentang astronomi
  yang berurut dari yang akan datang hingga 
  yang telah lalu dengan gambar, judul dan 
  juga isi deskripsi.
- Jika di klik judul dari daftar yang sudah di
  sebut sebelumnya, maka akan diarahkan ke 
  halaman events detail.
- Terdapat iklan untuk visitor dan user
  generic di sebelah kanan.

==============================================
		 Events Detail
==============================================
- Berisi isi pokok dari event yang di pilih di
  bagian news menu.
- halaman berisi judul, gambar, isi deskripsi,
  tanggal, lokasi, waktu acara dan waktu di 
  post ke events detail.
- halaman juga berisi seksi komentar untuk 
  memberi tanggapan terhadap kabar berita yang
  disajikan.
- Tersedia kolom post komentar / post reply 
  yang digunakan untuk menginput komentar
  tanggapan tentang events (harus login
  terlebih dahulu).
- Sisanya dibawah post reply merupakan kolom 
  daftar komentar yang telah di post.
- Jika komentar adalah milik user, maka user
  dapat mengedit ataupun menghapus komentar
  tersebut. Jika bukan milik user, maka user
  dapat mereport komentar tersebut.
- Terdapat iklan untuk visitor dan user
  generic di sebelah kanan.

==============================================
		  Discussions
==============================================
- Berisi daftar postingan user yang telah di
  buat di halaman discussions.
- Daftar postingan terdiri dari judul, isi 
  nama pemposting, tanggal dan waktu postingan
  dibuat.
- Bila judul dari postingan tersebut di klik,
  maka akan diarahkan ke halaman discussions 
  detail untuk melihat detail jelas dari 
  postingan tersebut.
- terdapat form untuk membuat postingan dengan
  kolom judul dan isi deskripsi postingan 
  (harus login terlebih dahulu).
- Terdapat iklan untuk visitor dan user
  generic di sebelah kanan.

==============================================
	       Discussions Detail
==============================================
- Berisi isi detail dari post yang di pilih di
  bagian discussions menu.
- halaman berisi judul, isi deskripsi,
  tanggal, dan waktu discussions di post ke 
  discussions detail.
- halaman juga berisi seksi komentar untuk 
  memberi tanggapan terhadap postingan yang
  disajikan.
- Tersedia kolom post komentar / post reply 
  yang digunakan untuk menginput komentar
  tanggapan tentang postingan original poster
  (harus login terlebih dahulu).
- Sisanya dibawah post reply merupakan kolom 
  daftar komentar yang telah di post.
- Jika komentar adalah milik user, maka user
  dapat mengedit ataupun menghapus komentar
  tersebut. Jika bukan milik user, maka user
  dapat mereport komentar tersebut.
- Terdapat iklan untuk visitor dan user
  generic di sebelah kanan.

==============================================
		  Course Menu
==============================================
- Berisi daftar kursus yang tersedia oleh 
  astronoob, daftar diurutkan sesuai dengan 
  tingkat kesulitan kursus tersebut.
- Jika di klik salah satu judul dari daftar 
  kursus tersebut maka akan diarahkan ke 
  halaman course detail.
- Terdapat iklan untuk visitor dan user
  generic di sebelah kanan.

==============================================
		 Course Detail
                    (User)
==============================================
- Berisi detail dari kursus yang telah dipilih
  pada bagian course menu.
- Dibagian atas terdapat video pembelajaran 
  terkait dengan subject materi yang sedang di
  baca.
- Dibawahnya terdapat tulisan isi pokok dari
  materi yang sedang ditampilkan ataupun 
  dipilih dari daftar materi.
- Disebelah kanan terdapat tombol "Start Quiz"
  yang dapat digunakan untuk memulai quiz jika
  dirasa user sudah cukup memahami tentang 
  materi yang dipelajari.
- Lalu dibagian bawahnya terdapat daftar
  subject materi yang dapat user baca yang 
  merupakan bagian dari kursus yang telah user
  pilih sebelumnya. 
- Terdapat iklan untuk user generic di 
  sebelah kanan.

==============================================
		  Quiz Page
		    (User)
==============================================
- Berisi soal-soal quiz yang telah dipilih 
  user dan harus user jawab untuk mendapat 
  point menuju ke tingkat kuis selanjutnya.
- Satu soal kuis disediakan waktu selama 1 
  menit untuk menjawab, jika waktu habis maka
  soal kuis akan di lewati dan jawaban akan 
  dianggap kosong (tidak mendapat poin soal).
- Halaman kuis tidak bisa di back, atau link
  nya di copy ke halaman browser lain karena
  menggunakan session.
- Untuk menjawab kuis cukup untuk memilih 
  salah satu radio button dari pilihan ganda 
  yang telah disediakan sesuai dengan 
  pertanyaan yang diberikan lalu klik tombol
  "Next" untuk mengsubmit jawaban lalu lanjut
  ke soal selanjutnya.
- Jika pertanyaan kuis sudah habis dan
  mencapai nomor terakhir, maka window kuis
  akan ditutup dan user akan diarahkan ke
  halaman result untuk mengetahui bagaimana
  hasil dari kuis yang telah dikerjakan.
- Percobaan kuis hanya tersedia seminggu 
  sekali setelah dikerjakan oleh user apabila
  user gagal.
- Jika user berhasil dan telah lulus, maka user
  akan langsung diarahkan ke halaman result
  yang akan menampilkan nilai kuis user saat
  user lulus mengerjakan kuis tersebut.
- Terdapat iklan untuk user generic di sebelah 
  kanan.

==============================================
	         Result Page
                   (User)
==============================================  
- Berisi tampilan info tentang hasil dari kuis
  yang telah user kerjaan.
- Isi berupa informasi apakah user lulus atau
  tidak lulus kuis untuk menuju ke level kuis
  selanjutnya, total nilai yang didapat
  oleh user serta kapan user bisa mengulang 
  kuis tersebut (jika gagal).
- Jika nilai kurang dari 70 maka user di
  anggap tidak lulus.

==============================================
	     Account Profile Page
		    (User)
==============================================
- Berisi informasi tentang user yang sedang 
  login.
- Informasi berupa nama, username, email, 
  foto, tingkatan badge, status aktif dsb.
- Dibawahnya terdapat tombol print untuk
  mencetak informasi user dalam bentuk kartu
  dengan ssrs.
- disebelah kiri terdapat tabel history 
  tentang apa yang telah dikerjakan user
  pada bagian kursus, diskusi dan komentar
  diskusi serta tombol print history course
  yang pernah user lakukan.
- Pada table diskusi dan komentar diskusi,
  user dapat mengklik link yang akan diarahkan
  ke halaman diskusi atau komentar diskusi 
  yang telah dipilih.

==============================================
	        User Card SSRS
		    (User)
==============================================
- Berisi informasi tentang user yang sedang 
  login dalam bentuk kartu yang dapat dicetak.

==============================================
	    Account Settings Page
               (User & Admin)
==============================================
- Berisi form account details, change password
  dan upload profile picture.
- Di form account details, user dapat 
  mengganti nama user dengan mengisi kolom 
  "Name" lalu disimpan dengan menekan tombol
  "Save Profile".
- Di form change password, user dapat
  mengganti password lama user dengan cara
  menginput password lama, password baru,
  konfirmasi password lalu menekan tombol
  "Save Password".
- Di form Profile Picture, user dapan 
  mengganti foto profile user dengan cara
  menekan tombol upload dan memilih foto
  yang ingin digunakan lalu memilih tombol
  "Save Picture"  

==============================================
	      Get Premium Page
		   (User)
==============================================
- Berisi table pilihan paket premium astronoob
  dengan ketentuannya masing-masing.
- Terdapat tombol buy untuk masing-masing
  paket yang jika di klik akan mengarahkan ke
  checkout page dengan pilihan sesuai paket
  yang dipilih.
- Apabila user sudah punya transaksi yang
  belum dibayar, akan diarahkan langsung ke
  checkout page.

==============================================
	        Checkout Page
                   (User)
==============================================
- Berisi informasi tentang apa saja yang akan
  didapatkan user dari transaksi yang akan
  dilakukan.
- disebelah kanan terdapat barcode yang bisa
  di scan oleh user untuk melanjutkan proses
  transaksi.
- Dibawah terdapat instruksi bagaimana
  proses pembayaran dan cara memindai barcode.
- Dibawah barcode terdapat tombol confirm 
  payment untuk simulasi jika transaksi 
  pembayaran sukses (user akan dikirim email 
  transaksi).
- Dibawah tombol confirm terdapat tombol 
  cancel untuk membatalkan transaksi yang
  telah user buat.

==============================================
	      	  Login Page
==============================================
- Berisi form login untuk user astronoob yang
  telah terdaftar.
- Form berisi kolom username dan password.
- Jika kolom username dan password benar, user
  bisa mengklik tombol login dan akan di
  arahkan ke halaman home dengan status sudah
  login.
- Dibawah form login terdapat pilihan forget
  password dan juga create new account.
- Forget password akan diarahkan ke halaman
  forget password dan create new account akan
  diarahkan ke hamalan register.

==============================================
	      	Register Page
==============================================
- Berisi form registrasi untuk visitor yang
  ingin mendaftar/membuat akun baru di 
  astronoob.
- Form berisi kolom name, username, email,
  password dan confirm password.
- Jika kolom username dan email tersedia dan
  dalam format yang benar, user bisa mengklik 
  tombol create new account dan akan di
  arahkan ke halaman konfirmasi email status
  akun yang harus diaktifkan terlebih dahulu
  untuk login.

==============================================
	      	Forget Password
==============================================
- Berisi form untuk data yang dibutuhkan dalam
  proses password recovery.
- Form berisi kolom email yang email itu nanti
  akan digunakan untuk dikirimkan link
  recovery yang valid dari system astronoob.
- Jika email valid dan terdaftar, user bisa
  langsung tekan tombol send email.

==============================================
	      	Reset Password
==============================================
- Berisi Form untuk membuat password baru
  setelah berhasil proses forget password
- Form berisi kolom new password dan confirm
  password.
- Jika sudah sesuai dengan validasi, user
  bisa langsung menyimpan password barunya
  dengan menekan tombol save password.

==============================================
	      	   Logout
==============================================
- Tombol yang digunakan untuk keluar dari 
  user authentication.

==============================================
	      	  User Menu
		   (Admin)
==============================================
- Berisi table tentang informasi user yang
  terdiri dari kolom username, name, picture,
  email dan badge dengan pagination, sort, dan
  search bar.
- terdapat tombol details untuk melihat
  detail informasi dari user yang dipilih.

==============================================
	      	 User Detail
		   (Admin)
==============================================
- Berisi informasi tentang user yang sedang 
  admin pilih.
- Informasi berupa nama, username, email, 
  foto, tingkatan badge, status aktif dsb.
- Dibawahnya terdapat tombol print untuk
  mencetak informasi user yang admin pilih 
  dalam bentuk kartu dengan ssrs.
- disebelah kiri terdapat tabel history 
  tentang apa yang telah dikerjakan user
  pada bagian kursus, diskusi dan komentar
  diskusi.
- Pada table diskusi dan komentar diskusi,
  user dapat mengklik link yang akan diarahkan
  ke halaman diskusi atau komentar diskusi 
  yang telah dipilih.

==============================================
	        User Card SSRS
		    (Admin)
==============================================
- Berisi informasi tentang user yang dipilih
  admin dalam bentuk kartu yang dapat dicetak.

==============================================
	      	  News Menu
		   (Admin)
==============================================
- Berisi table tentang daftar news yang
  terdiri dari kolom title, photo, description
  , date dan action dengan pagination, sort, 
  dan search bar.
- Terdapat tombol create news untuk membuat
  sebuah berita baru.
- Untuk membuat berita baru, harus mengisi
  seluruh kolom form create news yang tersedia
- Jika sudah, klik submit news untuk publish
  berita tersebut.
- Di bagian action table terdapat tombol edit
  yang akan mengarah ke halaman edit news dan
  tombol delete yang akan menampilkan modal
  apakah yakin news yang dipilih akan dihapus.

==============================================
	      	  Edit News
		   (Admin)
==============================================
- Untuk mengedit news, harus mengisi
  seluruh kolom form edit news yang tersedia
- Jika sudah, klik save changes untuk
  menyimpan perubahan.

==============================================
	      	 Events Menu
		   (Admin)
==============================================
- Berisi table tentang daftar events yang
  terdiri dari kolom title, photo, description
  , email, date posted, date, time, location
  dan action dengan pagination, sort, dan
  search bar.
- Terdapat tombol create event untuk membuat
  sebuah event baru.
- Untuk membuat event baru, harus mengisi
  seluruh kolom form create event yang 
  tersedia
- Jika sudah, klik submit event untuk publish
  berita tersebut.
- Di bagian action table terdapat tombol edit
  yang akan mengarah ke halaman edit event dan
  tombol delete yang akan menampilkan modal
  apakah yakin event yang dipilih akan dihapus.

==============================================
	      	 Edit Events
		   (Admin)
==============================================
- Untuk mengedit events, harus mengisi
  seluruh kolom form edit events yang tersedia
- Jika sudah, klik save changes untuk
  menyimpan perubahan.

==============================================
	      	 Course Menu
		   (Admin)
==============================================
- Berisi table tentang daftar course yang
  terdiri dari kolom name, description,
  badgeid dan action dengan pagination, sort, 
  dan serach bar.
- Terdapat tombol create course untuk membuat
  sebuah course baru.
- Untuk membuat course baru, harus mengisi
  seluruh kolom form create course yang 
  tersedia
- Jika sudah, klik submit course untuk publish
  course tersebut.
- Di bagian action table terdapat tombol edit
  yang akan mengarah ke halaman edit course dan
  tombol details yang akan mengarahkan ke
  halaman subjects yang berisi materi dan quiz.

==============================================
	      	 Edit Course
		   (Admin)
==============================================
- Untuk mengedit course, harus mengisi
  seluruh kolom form edit course yang tersedia
- Jika sudah, klik save changes untuk
  menyimpan perubahan.

==============================================
	      	 Subject Page
		   (Admin)
==============================================
- Berisi table tentang daftar subject yang
  terdiri dari kolom no, title, video,
  description, date dan action dengan 
  pagination, sort, dan search bar.
- Berisi table tentang daftar quiz yang
  terdiri dari kolom no, question, opt a,
  opt b, opt c, opt d, answer dan action 
  dengan pagination, sort, dan search bar.
- Terdapat tombol create subject untuk membuat
  sebuah subject baru.
- Terdapat tombol create quiz untuk membuat
  soal quiz baru.
- Untuk membuat subject baru, harus mengisi
  seluruh kolom form create subject yang 
  tersedia
- Untuk membuat soal quiz baru, harus mengisi
  seluruh kolom form create quiz yang 
  tersedia
- Jika sudah, klik submit subject untuk publish
  subject tersebut.
- Jika sudah, klik submit quiz untuk publish
  soal quiz tersebut.
- Di bagian action table subject terdapat 
  tombol edit yang akan mengarah ke halaman 
  edit subject dan tombol delete untuk
  menghapus subject yang dipilih.
- Di bagian action table quiz terdapat 
  tombol edit yang akan mengarah ke halaman 
  edit quiz dan tombol delete untuk
  menghapus soal quiz yang dipilih.

==============================================
	      	 Edit Subject
		   (Admin)
==============================================
- Untuk mengedit subject, harus mengisi
  seluruh kolom form edit subject yang 
  tersedia.
- Jika sudah, klik save changes untuk
  menyimpan perubahan.

==============================================
	      	  Edit Quiz
		   (Admin)
==============================================
- Untuk mengedit soal quiz, harus mengisi
  seluruh kolom form edit quiz yang 
  tersedia.
- Jika sudah, klik save changes untuk
  menyimpan perubahan.

==============================================
	      	   Ads Menu
		   (Admin)
==============================================
- Berisi table tentang daftar ads yang
  terdiri dari kolom location, picture, link,
  customer dan action dengan pagination, sort, 
  dan search bar.
- Di bagian action table terdapat tombol edit
  yang akan mengarah ke halaman edit ads.

==============================================
	      	   Edit Ads
		   (Admin)
==============================================
- Untuk mengedit ads, harus mengisi
  seluruh kolom form edit ads yang tersedia.
- Jika sudah, klik save changes untuk
  menyimpan perubahan.

==============================================
	      	 Report Menu
		   (Admin)
==============================================
- Berisi 4 tab yaitu discussion, discussion
  comment, news comment dan event comment.
- masing-masing tab jika diklik akan 
  menampilkan table yang berisi laporan dari
  tiap bagian lokasi sesuai nama tab tersebut.
- Di bagian kolom action terdapat tombol 
  details untuk melihat detail laporan yang
  isinya berupa isi komentar ataupun deskripsi
  sebuah post untuk di tinjau.
- Jika laporan sesuai, maka terdapat pilihan 
  untuk menghapus komentar atau post tersebut,
  jika tidak sesuai, maka terdapat pilihan
  untuk menghapus laporan yang tidak relevant
  tersebut.

==============================================
	      	  Error Log
		   (Admin)
==============================================
- Berisi table tentang error log yang
  terdiri dari kolom username, message,
  location dan date dengan pagination, sort, 
  dan search bar.

==============================================
	       Monthly User SSRS
		   (Admin)
==============================================
- Berisi table laporan tentang berapa total 
  user yang mendaftar pada satu bulan. 

==============================================
	         Payment SSRS
		   (Admin)
==============================================
- Berisi table laporan tentang berapa total 
  transaksi pembayaran premium mulai dari yang
  pending dan sukses dengan total jumlah uang
  yang didapat dari transaksi.

==============================================
	      Passing Course SSRS
		   (Admin)
==============================================
- Berisi table laporan tentang berapa total 
  user lulus ataupun tidak lulus pada suatu
  course. Dapat di sortir sesuai dengan level
  course. 

==============================================
	     Event Participant SSRS
		   (Admin)
==============================================
- Berisi table laporan tentang berapa total 
  user yang akan mengikuti sebuah event yang
  berlangsung selama satu bulan terakhir. 