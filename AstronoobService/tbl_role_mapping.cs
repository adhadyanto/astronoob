//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AstronoobService
{
    using System;
    using System.Collections.Generic;
    
    public partial class tbl_role_mapping
    {
        public int id { get; set; }
        public Nullable<int> userid { get; set; }
        public Nullable<int> roleid { get; set; }
    
        public virtual tbl_user tbl_user { get; set; }
    }
}
