﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;

namespace AstronoobService
{
    [RunInstaller(true)]
    public partial class Service1 : ServiceBase
    {
        System.Timers.Timer tmrExecutor = new System.Timers.Timer();
        db_projectEntities db = new db_projectEntities();
        public Thread Worker = null;

        public Service1()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            //System.Diagnostics.Debugger.Launch();
            try
            {
                tmrExecutor.Elapsed += new ElapsedEventHandler(tmrExecutor_Elapsed);
/*              tmrExecutor.Elapsed += new ElapsedEventHandler(tmrExecutor_Elapsed_demo);*/
/*              tmrExecutor.Interval = 10000;*/
                tmrExecutor.Interval = 60000;
                tmrExecutor.Enabled = true;
                tmrExecutor.Start();
            }
            catch (Exception)
            {
                throw;
            }
        }

        private void tmrExecutor_Elapsed_demo(object sender, ElapsedEventArgs e)
        {
           
            var emails = db.tbl_user.Join(db.tbl_role_mapping, u => u.id, p => p.userid, (u, p) => new {
                u.id,
                u.name,
                u.email,
                p.roleid
            }).Where(p => p.roleid == 3).ToList();

            var news = db.tbl_news.OrderByDescending(n => n.date_posted).FirstOrDefault();
            var template = db.tbl_email_template.Where(m => m.name == "newsletter").FirstOrDefault();

            string subject = template.subject;
            string body = "";

            foreach (var data in emails)
            {
                body = String.Format(template.body, data.name, news.encrypt_id, news.title, news.description);
                this.sendEmail(subject, body, data.email);
            }

            using (var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    var premium_user = db.tbl_premium_user.ToList();

                    template = db.tbl_email_template.Where(m => m.name == "expiration").FirstOrDefault();
                    subject = template.subject;
                    body = template.body;

                    foreach (var data in premium_user)
                    {
                        var user_role = db.tbl_role_mapping.Where(m => m.userid == data.userid).FirstOrDefault();
                        var email = db.tbl_user.Find(data.userid);
                        user_role.roleid = 2;
                        db.tbl_premium_user.Remove(data);
                        sendEmail(subject, body, email.email);
                    }

                    template = db.tbl_email_template.Where(m => m.name == "attempt").FirstOrDefault();
                    subject = template.subject;
                    body = template.body;

                    var attempt = db.tbl_answer.Where(a => a.grade < 70).ToList();
                    foreach (var data in attempt)
                    {

                        data.attempt = 0;
                        var user = db.tbl_user.Find(data.userid);
                        sendEmail(subject, body, user.email);
                    }

                    var ads = db.tbl_ads.Where(m => m.duration > 0).ToList();
                    foreach (var data in ads)
                    {
                        data.duration -= 1;
                    }

                    db.SaveChanges();
                    transaction.Commit();
                }
                catch (Exception error)
                {
                    transaction.Rollback();
                    db_projectEntities err_db = new db_projectEntities();
                    tbl_error_log_history err_log = new tbl_error_log_history();
                    err_log.username = "System";
                    err_log.err_msg = error.Message;
                    err_log.err_loc = "Service";
                    err_log.err_date = DateTime.Now;
                    err_db.tbl_error_log_history.Add(err_log);
                    err_db.SaveChanges();
                }                
            }
        }
        

        private void tmrExecutor_Elapsed(object sender, ElapsedEventArgs e)
        {
            if (DateTime.Now.Hour == 08 && DateTime.Now.Minute == 00)
            {
                var emails = db.tbl_user.Join(db.tbl_role_mapping, u => u.id, p => p.userid, (u, p) => new {
                    u.id,
                    u.name,
                    u.email,
                    p.roleid
                }).Where(p => p.roleid == 3).ToList();

                var news = db.tbl_news.OrderByDescending(n => n.date_posted).FirstOrDefault();
                var template = db.tbl_email_template.Where(m => m.name == "newsletter").FirstOrDefault();

                string subject = template.subject;
                string body = "";

                foreach (var data in emails)
                {
                    body = String.Format(template.body, data.name, news.encrypt_id, news.title, news.description);
                    this.sendEmail(subject, body, data.email);
                }

                var date = DateTime.Now.AddDays(3).Date;
                var events = db.tbl_events.Where(m => m.date == date).ToList();

                template = db.tbl_email_template.Where(m => m.name == "reminder").FirstOrDefault();
                subject = template.subject;

                foreach (var item in events)
                {
                    var participants = db.tbl_event_participant.Join(db.tbl_user, d => d.userid, u => u.id, (d, u) => new
                    {
                        d.eventid,
                        u.email,
                        u.name
                    }).Where(d => d.eventid == item.id).ToList();

                    foreach (var participant in participants)
                    {
                        body = String.Format(template.body, participant.name, item.title, item.time);
                        this.sendEmail(subject, body, participant.email);
                    }
                }

            }

            else if (DateTime.Now.Hour == 23 && DateTime.Now.Minute == 59)
            {
                var premium_user = db.tbl_premium_user.ToList();
                var attempt = db.tbl_answer.Where(a => a.grade < 70).ToList();
                var ads = db.tbl_ads.Where(m => m.duration > 0).ToList();

                var template = db.tbl_email_template.Where(m => m.name == "expiration").FirstOrDefault();
                var subject = template.subject;

                string body = template.body;

                using (var transaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        foreach (var data in premium_user)
                        {
                            data.days -= 1;
                            if (data.days == 0)
                            {
                                var user_role = db.tbl_role_mapping.Where(m => m.userid == data.userid).FirstOrDefault();
                                var email = db.tbl_user.Find(data.userid);
                                user_role.roleid = 2;
                                db.tbl_premium_user.Remove(data);
                                sendEmail(subject, body, email.email);
                            }
                        }

                        template = db.tbl_email_template.Where(m => m.name == "attempt").FirstOrDefault();
                        subject = template.subject;

                        body = template.body;

                        foreach (var data in attempt)
                        {
                            DateTime date = (DateTime)data.date;
                            if (date.Date.AddDays(7) == DateTime.Now.Date)
                            {
                                data.attempt = 0;
                                var user = db.tbl_user.Find(data.userid);
                                sendEmail(subject, body , user.email);
                            }
                        }
                        foreach (var data in ads)
                        {
                            data.duration -= 1;
                        }
                        db.SaveChanges();

                        transaction.Commit();
                    }
                    catch (Exception error)
                    {
                        transaction.Rollback();
                        db_projectEntities err_db = new db_projectEntities();
                        tbl_error_log_history err_log = new tbl_error_log_history();
                        err_log.username = "System";
                        err_log.err_msg = error.Message;
                        err_log.err_loc = "Service";
                        err_log.err_date = DateTime.Now;
                        err_db.tbl_error_log_history.Add(err_log);
                        err_db.SaveChanges();
                    }
                }
            }
        }

        private void sendEmail(string subject, string body, string receiver)
        {
            using (MailMessage mm = new MailMessage("hello.astronoob@gmail.com", receiver))
            {
                mm.Subject = subject;
                mm.Body = body;
                mm.IsBodyHtml = true;
                SmtpClient smtp = new SmtpClient();
                smtp.Host = "smtp.gmail.com";
                smtp.EnableSsl = true;
                NetworkCredential NetworkCred = new NetworkCredential("hello.astronoob@gmail.com", "astronoob03_");
                smtp.UseDefaultCredentials = true;
                smtp.Credentials = NetworkCred;
                smtp.Port = 587;
                smtp.Send(mm);
            }
        }

        protected override void OnStop()
        {
            try
            {
                tmrExecutor.Enabled = false;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
